<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Callback\Exception;

/**
 * Abstract class AbstractException
 *
 * @package CrefoPay\Library\Callback\Exception
 */
abstract class AbstractException extends \CrefoPay\Library\AbstractException
{
    /**
     * Constructor
     *
     * @param string $message
     */
    public function __construct($message = 'Callback Exception')
    {
        parent::__construct($message);
    }
}
