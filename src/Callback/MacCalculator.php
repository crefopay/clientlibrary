<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Callback;

use CrefoPay\Library\Callback\Exception\MacValidation;
use CrefoPay\Library\Config;
use CrefoPay\Library\Mac\AbstractCalculator;
use CrefoPay\Library\Mac\Exception\MacInvalid;

/**
 * Class MacCalculator
 *
 * Mac calculator for call backs and MNS notifications
 *
 * @package CrefoPay\Library\Callback
 */
class MacCalculator extends AbstractCalculator
{
    /**
     * Given HMAC
     *
     * @var string
     */
    private $mac;

    /**
     * Raw request
     *
     * @var array
     */
    private $rawRequest;

    /**
     * Constructor
     *
     * @param Config $config
     * @param array  $request
     */
    public function __construct(Config $config, array $request)
    {
        $this->setConfig($config);
        $this->setRequest($request);
    }

    /**
     * Sets the request
     *
     * @param array $request
     *
     * @return $this
     */
    public function setRequest(array $request)
    {
        $this->setCalculationArray($request);

        $this->rawRequest = $request;

        if (array_key_exists('mac', $request)) {
            $this->mac = $request['mac'];
        }
        return $this;
    }

    /**
     * Validation function
     *
     * @return bool
     *
     * @throws MacValidation
     * @throws MacInvalid
     */
    public function validateResponse()
    {
        if (!parent::validate($this->mac, false)) {
            throw new MacValidation($this->calculateMac(), $this->mac, $this->rawRequest);
        }

        return true;
    }
}
