<?php
/**
 * SolvencyCheck Class file
 *
 * @author Vincent Mrose
 */

namespace CrefoPay\Library\Request;

use CrefoPay\Library\Request\Objects\Address;
use CrefoPay\Library\Request\Objects\Company;
use CrefoPay\Library\Request\Objects\Person;

/**
 * Class SolvencyCheck
 *
 * This is the request class for any solvencycheck request object
 *
 * @link    https://docs.crefopay.de/api/#solvencycheck
 * @package CrefoPay\Library\Request
 */
class SolvencyCheck extends AbstractRequest
{
    /**
     * The unique user id of the customer.
     *
     * @var string
     */
    private $userID;

    /**
     * This parameter is used to differentiate b2b and b2c customers
     *
     * @see \CrefoPay\Library\User\Type::USER_TYPE_PRIVATE
     * @see \CrefoPay\Library\User\Type::USER_TYPE_BUSINESS
     * @var string
     */
    private $userType;

    /**
     * Contact data of the users company
     *
     * @see Company
     * @var Company
     */
    private $companyData;

    /**
     * contact data of the user. The field “date of birth” is not mandatory.
     *
     * It’s needed for solvency checks and the payment method “bill secure”.
     * An absent “date of birth” could cause less payment methods to be offered to the user.
     *
     * @var Person
     */
    private $userData;

    /**
     * the customers billing address
     *
     * Only required if user was not registered before with this userID
     *
     * @var Address
     */
    private $billingRecipient;

    /**
     * the customers billing address
     *
     * Only required if user was not registered before with this userID
     *
     * @var Address
     */
    private $billingAddress;

    /**
     * Set the user ID
     *
     * @see SolvencyCheck::userID
     *
     * @param string $userID
     *
     * @return $this
     */
    public function setUserID($userID)
    {
        $this->userID = $userID;
        return $this;
    }

    /**
     * Get the user id
     *
     * @see SolvencyCheck::userID
     * @return string
     */
    public function getUserId()
    {
        return $this->userID;
    }

    /**
     * Set the userType field
     *
     * @see SolvencyCheck::userType
     *
     * @param $userType
     *
     * @return $this
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
        return $this;
    }

    /**
     * Get the userType field
     *
     * @see SolvencyCheck::userType
     * @return string
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * Set the companyData field
     *
     * @see SolvencyCheck::companyData
     *
     * @param Company $company
     *
     * @return $this
     */
    public function setCompanyData(Company $company)
    {
        $this->companyData = $company;
        return $this;
    }

    /**
     * Get the companyData field
     *
     * @see SolvencyCheck::companyData
     * @return Company
     */
    public function getCompanyData()
    {
        return $this->companyData;
    }

    /**
     * Set the userData field
     *
     * @see SolvencyCheck::userData
     *
     * @param Person $userData
     *
     * @return $this
     */
    public function setUserData(Person $userData)
    {
        $this->userData = $userData;
        return $this;
    }

    /**
     * Get the userData field
     *
     * @see SolvencyCheck::userData
     * @return Person
     */
    public function getUserData()
    {
        return $this->userData;
    }

    /**
     * Set the billingRecipient field
     *
     * @see SolvencyCheck::billingRecipient
     *
     * @param $billingRecipient
     *
     * @return $this
     */
    public function setBillingRecipient($billingRecipient)
    {
        $this->billingRecipient = $billingRecipient;
        return $this;
    }

    /**
     * Get the billingRecipient field
     *
     * @see SolvencyCheck::billingRecipient
     * @return string
     */
    public function getBillingRecipient()
    {
        return $this->billingRecipient;
    }

    /**
     * Set billingAddress field
     *
     * @see SolvencyCheck::billingAddress
     *
     * @param Address $billingAddress
     *
     * @return $this
     */
    public function setBillingAddress(Address $billingAddress)
    {
        $this->billingAddress = $billingAddress;
        return $this;
    }

    /**
     * Get billingAddress field
     *
     * @see SolvencyCheck::billingAddress
     * @return Address
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * Must be implemented in classes
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getClassValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['userID'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "userID is required"
        );

        $validationData['userID'][] = array(
            'name' => 'MaxLength',
            'value' => '50',
            'message' => "userID must be between 1 and 50 characters"
        );

        $validationData['userType'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "userType is required"
        );

        $validationData['userType'][] = array(
            'name' => 'Callback',
            'value' => 'CrefoPay\Library\User\Type::validate',
            'message' => "userType must be certain values"
        );

        $validationData['billingRecipient'][] = array(
            'name' => 'MaxLength',
            'value' => '80',
            'message' => "billingRecipient must be between 1 and 80 characters"
        );

        return $validationData;
    }

    /**
     * Classes must return the data
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        $data = array(
            'userID' => $this->getUserId(),
            'userType' => $this->getUserType(),
        );

        if (!empty($this->companyData)) {
            $data['companyData'] = $this->getCompanyData();
        }

        if (!empty($this->userData)) {
            $data['userData'] = $this->getUserData();
        }

        if (!empty($this->billingRecipient)) {
            $data['billingRecipient'] = $this->getBillingRecipient();
        }

        if (!empty($this->billingAddress)) {
            $data['billingAddress'] = $this->getBillingAddress();
        }

        return $data;
    }
}