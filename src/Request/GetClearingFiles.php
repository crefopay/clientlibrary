<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Request;

/**
 * Class GetClearingFiles
 *
 * @package CrefoPay\Library\Request
 * @link    https://docs.crefopay.de/api/#getclearingfiles
 */
class GetClearingFiles extends AbstractRequest
{
    /**
     * The file ID
     *
     * @var string
     */
    private $clearingFileID;

    /**
     * The path
     *
     * @var string
     */
    private $path = "";


    /**
     * Set the clearingFileID field in the request
     *
     * @see GetClearingFiles::$clearingFileID
     *
     * @param string $clearingFileID
     *
     * @return $this
     */
    public function setClearingFileID($clearingFileID)
    {
        $this->clearingFileID = $clearingFileID;
        return $this;
    }

    /**
     * Get the value of the clearingFileID field
     *
     * @see GetClearingFiles::$clearingFileID
     * @return string
     */
    public function getClearingFileID()
    {
        return $this->clearingFileID;
    }

    /**
     * Set the path field in the request
     *
     * @see GetClearingFiles::$path
     *
     * @param bool $path
     *
     * @return $this
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * Get the value of the path field
     *
     * @see GetClearingFiles::$path
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Get the serializer data
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        $return = array();

        $return['clearingFileID'] = $this->getClearingFileID();

        return $return;
    }

    /**
     * Get the validation data
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getClassValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['clearingFileID'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Clearing file ID is required"
        );
        return $validationData;
    }
}
