<?php
/**
 * Class file
 *
 * @author Vincent Mrose
 */

namespace CrefoPay\Library\Request;

/**
 * Class UpdateTransactionData
 *
 * This is the request class for any updateTransactionData request object
 *
 * @package CrefoPay\Library\Request
 */
class UpdateTransactionData extends AbstractRequest
{
    /**
     * This is the order number of the shop.
     *
     * This id is created by the shop and is used as identifier for this transaction
     *
     * @var string
     */
    private $orderID;

    /**
     * The new merchant reference for the transaction
     *
     * @var string
     */
    private $merchantReference;

    /**
     * Set the Order ID
     *
     * @see UpdateTransactionData::orderID
     *
     * @param string $orderID
     *
     * @return $this
     */
    public function setOrderID($orderID)
    {
        $this->orderID = $orderID;
        return $this;
    }

    /**
     * Get the set order ID
     *
     * @see UpdateTransactionData::orderID
     * @return string
     */
    public function getOrderID()
    {
        return $this->orderID;
    }

    /**
     * Set the merchant reference
     *
     * @see UpdateTransactionData::merchantReference
     *
     * @param string $merchantReference
     *
     * @return $this
     */
    public function setMerchantReference($merchantReference)
    {
        $this->merchantReference = $merchantReference;
        return $this;
    }

    /**
     * Get the merchant reference
     *
     * @see UpdateTransactionData::merchantReference
     * @return string
     */
    public function getMerchantReference()
    {
        return $this->merchantReference;
    }

    /**
     * Get the serializer data
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        $data = array(
            'orderID' => $this->getOrderID(),
        );
        if (!empty($this->merchantReference)) {
            $data['merchantReference'] = $this->getMerchantReference();
        }
        return $data;
    }

    /**
     * Get the validation
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getClassValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['orderID'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "orderID is required"
        );

        $validationData['orderID'][] = array(
            'name' => 'MaxLength',
            'value' => '50',
            'message' => "orderID must be between 1 and 50 characters"
        );

        $validationData['merchantReference'][] = array(
            'name' => 'MaxLength',
            'value' => '30',
            'message' => "merchantReference must be between 1 and 30 characters"
        );

        return $validationData;
    }
}
