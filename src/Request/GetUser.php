<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Request;

/**
 * Class GetUser
 *
 * The getUser call adds the functionality to get the data and the status of a user.
 *
 * @link    https://docs.crefopay.de/api/#getuser
 * @package CrefoPay\Library\Request
 */
class GetUser extends AbstractRequest
{
    /**
     * The unique user id of the customer.
     *
     * @var string
     */
    private $userID;

    /**
     * Defines if the risk checks for this transaction should be returned.
     *
     * @var boolean
     */
    private $returnRiskData;

    /**
     * Set the userID field
     *
     * @see GetUser::userID
     *
     * @param $userID
     *
     * @return $this
     */
    public function setUserID($userID)
    {
        $this->userID = $userID;
        return $this;
    }

    /**
     * Gets the set userID
     *
     * @see GetUser::userID
     * @return string
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * Set the flag to return risk check data
     *
     * @see GetUser::returnRiskData
     *
     * @param boolean $returnRiskData
     *
     * @return $this
     */
    public function setReturnRiskData($returnRiskData)
    {
        $this->returnRiskData = $returnRiskData;
        return $this;
    }

    /**
     * Get the flag to return risk check data
     *
     * @see GetUser::returnRiskData
     * @return boolean
     */
    public function getReturnRiskData()
    {
        return $this->returnRiskData;
    }

    /**
     * Getting data for serialization
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        $data = array(
            'userID' => $this->getUserId(),
        );
        if (!empty($this->returnRiskData)) {
            $data['returnRiskData'] = $this->getReturnRiskData();
        }
        return $data;
    }

    /**
     * Validation meta data
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getClassValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['userID'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "userID is required"
        );

        $validationData['userID'][] = array(
            'name' => 'MaxLength',
            'value' => '50',
            'message' => "userID must be between 1 and 50 characters"
        );

        return $validationData;
    }
}
