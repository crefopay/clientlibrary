<?php
/**
 * Class file
 *
 * @author Vincent Mrose
 */

namespace CrefoPay\Library\Request\Helper;

/**
 * Class PhoneNumber
 *
 * Validation helper for PhoneNumbers
 *
 * @package CrefoPay\Library\Request\Helper
 */
class PhoneNumber
{
    const PHONE_REGEX = '/^(((((((00|\+)[0-9]{2}[ \\\\\-\/]?)|0)[1-9][0-9]{1,4})[ \\\\\-\/]?)|((((00|\+)[0-9]{2}\()|\(0)[1-9][0-9]{1,4}\)[ \\\\\-\/]?))[0-9]{1,7}([ \\\\\-\/]?[0-9]{1,5})?)$/';

    /**
     * Validates a phone number
     *
     * @param string $number
     *
     * @return bool
     */
    public static function validatePhoneNumber($number)
    {
        $matchFound = preg_match(self::PHONE_REGEX, $number);
        return $matchFound === 1;
    }

    /**
     * Validates a fax number
     *
     * @param string $number
     *
     * @return bool
     */
    public static function validateFaxNumber($number)
    {
        $matchFound = preg_match(self::PHONE_REGEX, $number);
        return $matchFound === 1;
    }
}