<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Request;

/**
 * Class Cancel
 *
 * This is the request class for any Cancel request
 *
 * @link    https://docs.crefopay.de/api/#cancel
 * @package CrefoPay\Library\Request
 */
class Cancel extends AbstractRequest
{
    /**
     * This is the order number of the shop.
     *
     * This id is created by the shop and is used as identifier for this transaction
     *
     * @var string
     */
    private $orderID;

    /**
     * Set the Order ID
     *
     * @see Cancel::orderID
     *
     * @param string $orderID
     *
     * @return $this
     */
    public function setOrderID($orderID)
    {
        $this->orderID = $orderID;
        return $this;
    }

    /**
     * Get the set order ID
     *
     * @see Cancel::orderID
     * @return string
     */
    public function getOrderID()
    {
        return $this->orderID;
    }

    /**
     * Get the serializer data
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        return array(
            'orderID' => $this->getOrderID(),
        );
    }

    /**
     * Get the validation
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getClassValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['orderID'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "orderID is required"
        );

        $validationData['orderID'][] = array(
            'name' => 'MaxLength',
            'value' => '50',
            'message' => "orderID must be between 1 and 50 characters"
        );

        return $validationData;
    }
}
