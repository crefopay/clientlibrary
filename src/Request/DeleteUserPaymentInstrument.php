<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Request;

/**
 * Class DeleteUserPaymentInstrument
 *
 * The deleteUserPaymentInstrument call adds the functionality to delete a payment instrument from a user.
 * Input data consist of the following:
 * User information (existing user-id or complete user data)
 * Payment information (existing payment instrument-id or payment instrument data)
 *
 * @see     https://docs.crefopay.de/api/#deleteuserpaymentinstrument
 * @package CrefoPay\Library\Request
 */
class DeleteUserPaymentInstrument extends AbstractRequest
{
    /**
     * unique identifier of the payment instrument
     *
     * @var string
     */
    private $paymentInstrumentID;

    /**
     * Set the paymentInstrumentID field
     *
     * @see DeleteUserPaymentInstrument::paymentInstrumentID
     *
     * @param string $paymentInstrumentID
     */
    public function setPaymentInstrumentID($paymentInstrumentID)
    {
        $this->paymentInstrumentID = $paymentInstrumentID;
    }

    /**
     * Get the paymentInstrumentID field
     *
     * @see DeleteUserPaymentInstrument::paymentInstrumentID
     * @return string
     */
    public function getPaymentInstrumentID()
    {
        return $this->paymentInstrumentID;
    }

    /**
     * Getting data for serialization
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        return array(
            'paymentInstrumentID' => $this->getPaymentInstrumentID(),
        );
    }

    /**
     * Validation meta data
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getClassValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['paymentInstrumentID'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "paymentInstrumentID is required"
        );

        return $validationData;
    }
}
