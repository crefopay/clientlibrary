<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Request;

/**
 * Class GetClearingFileList
 *
 * @package CrefoPay\Library\Request
 * @link    https://docs.crefopay.de/api/#getclearingfilelist
 */
class GetClearingFileList extends AbstractRequest
{
    /**
     * The from date is including
     *
     * @var \DateTimeInterface
     */
    private $from;

    /**
     * The to date is including
     *
     * @var \DateTimeInterface
     */
    private $to;

    /**
     * Set the from field in the request
     *
     * @see GetClearingFileList::$from
     *
     * @param \DateTimeInterface $from
     *
     * @return $this
     */
    public function setFrom(\DateTimeInterface $from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * Get the value of the from field
     *
     * @see GetClearingFileList::$from
     * @return \DateTimeInterface
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set the to field
     *
     * @see GetClearingFileList::$to
     *
     * @param \DateTimeInterface $to
     *
     * @return $this
     */
    public function setTo(\DateTimeInterface $to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * Get value of the to field
     *
     * @see GetClearingFileList::$to
     * @return \DateTimeInterface
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Get the serializer data
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        $return = array();

        if ($this->from instanceof \DateTimeInterface) {
            $return['from'] = $this->getFrom()->format('Y-m-d');
        }

        if ($this->to instanceof \DateTimeInterface) {
            $return['to'] = $this->getTo()->format('Y-m-d');
        }

        return $return;
    }

    /**
     * Get the validation data
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getClassValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['from'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Start date is required"
        );

        return $validationData;
    }
}
