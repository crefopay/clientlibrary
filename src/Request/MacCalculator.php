<?php
/**
 * MacCalculator for requests
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Request;

use CrefoPay\Library\Mac\AbstractCalculator;
use CrefoPay\Library\Serializer\SerializerFactory;

/**
 * Class MacCalculator
 *
 * Mac calculator for the request objects
 *
 * @package CrefoPay\Library\Request
 */
class MacCalculator extends AbstractCalculator
{
    /**
     * Instance of the serializer for calculation
     *
     * @var \CrefoPay\Library\Serializer\Serializer
     */
    private $serializer;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->serializer = SerializerFactory::getSerializer();
    }

    /**
     * Set request that MAC needs to be calculated on
     *
     * @param AbstractRequest $request
     *
     * @return $this
     *
     * @throws \CrefoPay\Library\Serializer\Exception\VisitorCouldNotBeFound
     */
    public function setRequest(AbstractRequest $request)
    {
        $data = array();
        $fields = $request->getSerializerData();
        $excluded = $request->getExcludedMacFields();
        foreach ($excluded as $value) {
            if (isset($fields[$value])) {
                unset($fields[$value]);
            }
        }
        /**Serialize the any sub objects**/
        foreach ($fields as $key => $value) {
            if ($this->needsToBeSerialized($value)) {
                $data[$key] = $this->serializer->serialize($value);
            } else {
                $data[$key] = $value;
            }
        }

        $this->setCalculationArray($data);

        return $this;
    }

    /**
     * Check if value needs to be serialized
     *
     * @param $value
     *
     * @return bool
     */
    private function needsToBeSerialized($value)
    {
        return $value instanceof RequestInterface;
    }
}
