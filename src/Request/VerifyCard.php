<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Request;

use CrefoPay\Library\Validation\Helper\Constants;

/**
 * Class VerifyCard
 *
 * This is the request class for any verifyCard request object
 *
 * @package    CrefoPay\Library\Request
 * @deprecated 1.1.6 verifyCard API was removed by CrefoPay
 */
class VerifyCard extends AbstractRequest
{

    /**
     * Verification context: ONLINE
     * Context in which the transaction should take place
     *
     * @var string
     */
    const CONTEXT_ONLINE = "ONLINE";

    /**
     * Verification context: MAIL_ORDER
     * Context in which the transaction should take place
     *
     * @var string
     */
    const CONTEXT_MAIL_ORDER = "MAIL_ORDER";

    /**
     * Verification context: TELEPHONE_ORDER
     * Context in which the transaction should take place
     *
     * @var string
     */
    const CONTEXT_TELEPHONE_ORDER = "TELEPHONE_ORDER";

    /**
     * This is the card number of the credit card.
     *
     * @var string
     */
    private $cardNumber;

    /**
     * This is the validity date
     *
     * @var \DateTimeInterface
     */
    private $validity;

    /**
     * Context in which the transaction should take place
     *
     * @var string
     */
    private $context;

    /**
     * CVV of the credit card
     *
     * @var string
     */
    private $cvv;

    /**
     * Set the card number
     *
     * @param string $number
     *
     * @return $this
     * @see VerifyCard::cardNumber
     *
     */
    public function setCardNumber($number)
    {
        $this->cardNumber = $number;
        return $this;
    }

    /**
     * Get the set card number
     *
     * @return string
     * @see VerifyCard::cardNumber
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * Set the validity
     *
     * @param \DateTimeInterface $validity
     *
     * @return $this
     * @see VerifyCard::validity
     *
     */
    public function setValidity(\DateTimeInterface $validity)
    {
        $this->validity = $validity;
        return $this;
    }

    /**
     * Get the set validity
     *
     * @return \DateTimeInterface
     * @see VerifyCard::validity
     */
    public function getValidity()
    {
        return $this->validity;
    }

    /**
     * Set the context
     *
     * @param string $context
     *
     * @return $this
     * @see VerifyCard::context
     *
     */
    public function setContext($context)
    {
        $this->context = $context;
        return $this;
    }

    /**
     * Get the set context
     *
     * @return string
     * @see VerifyCard::context
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Set the cvv
     *
     * @param string $cvv
     *
     * @return \CrefoPay\Library\Request\VerifyCard
     */
    public function setCvv($cvv)
    {
        $this->cvv = $cvv;
        return $this;
    }

    /**
     * Get the cvv
     *
     * @return string
     */
    public function getCvv()
    {
        return $this->cvv;
    }

    /**
     * Get the serializer data
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        $result = array(
            'cardNumber' => $this->getCardNumber(),
            'validity' => (empty($this->validity) ? '' : $this->getValidity()->format("Y-m")),
            'context' => $this->getContext()
        );

        if ($this->cvv) {
            $result['cvv'] = $this->getCvv();
        }

        return $result;
    }

    /**
     * Get the validation
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getClassValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['cardNumber'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "cardNumber is required"
        );

        $validationData['cardNumber'][] = array(
            'name' => 'MaxLength',
            'value' => '20',
            'message' => "cardNumber must be between 1 and 20 characters"
        );

        $validationData['validity'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "validity is required"
        );

        $validationData['context'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "context is required"
        );

        $validationData['context'][] = array(
            'name' => 'Callback',
            'value' => get_class($this) . '::validateContextType',
            'message' => "context must one of 3 values"
        );

        if ($this->cvv) {
            $validationData['cvv'][] = array(
                'name' => 'Regex',
                'value' => '/^[0-9]{1,4}$/',
                'message' => "cvv has an invalid format"
            );
        }

        return $validationData;
    }

    /**
     * Validation function
     *
     * @param string $value
     *
     * @return bool
     */
    public static function validateContextType($value)
    {
        return Constants::validateConstant(__CLASS__, $value, 'CONTEXT');
    }
}
