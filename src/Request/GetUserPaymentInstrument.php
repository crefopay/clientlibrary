<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Request;

/**
 * Class GetUserPaymentInstrument
 *
 * The getUserPaymentInstrument call adds the functionality to get a user‘s payment instruments.
 * Input data consist of the following:
 * User information (existing user-id or complete user data)
 * Payment information (existing payment instrument-id or payment instrument data)
 *
 * @link    https://docs.crefopay.de/api/#getuserpaymentinstrument
 * @package CrefoPay\Library\Request
 */
class GetUserPaymentInstrument extends AbstractRequest
{
    /**
     * The unique user id of the customer.
     *
     * @var string
     */
    private $userID;

    /**
     * Set the userID field
     *
     * @see GetUser::userID
     *
     * @param $userID
     *
     * @return $this
     */
    public function setUserID($userID)
    {
        $this->userID = $userID;
        return $this;
    }

    /**
     * Getting the set userID
     *
     * @see GetUser::userID
     * @return string
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * Get the data for serializing
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        return array(
            'userID' => $this->getUserId(),
        );
    }

    /**
     * Validation meta data
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getClassValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['userID'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "userID is required"
        );

        $validationData['userID'][] = array(
            'name' => 'MaxLength',
            'value' => '50',
            'message' => "userID must be between 1 and 50 characters"
        );

        return $validationData;
    }
}
