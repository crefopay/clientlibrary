<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Request\Objects;

use CrefoPay\Library\Request\RequestInterface;

/**
 * Class AbstractRange
 *
 * For the AbstractRange json objects
 *
 * @link    https://docs.crefopay.de/api/#getsubscriptionplans
 * @package CrefoPay\Library\Request\Objects
 */
abstract class AbstractRange extends AbstractObject
{
    /**
     * Minimum value
     *
     * @var mixed
     */
    private $minimum;

    /**
     * Maximum value
     *
     * @var mixed
     */
    private $maximum;

    /**
     * Constructor
     */
    abstract public function __construct();

    /**
     * Set the minimum
     *
     * @param mixed $minimum
     *
     * @return $this
     */
    public function setMinimum($minimum)
    {
        $this->minimum = $minimum;
        return $this;
    }

    /**
     * Get the minimum
     *
     * @return mixed
     */
    public function getMinimum()
    {
        return $this->minimum;
    }

    /**
     * Set the maximum
     *
     * @param mixed $maximum
     *
     * @return $this
     */
    public function setMaximum($maximum)
    {
        $this->maximum = $maximum;
        return $this;
    }

    /**
     * Get the maximum
     *
     * @return mixed
     */
    public function getMaximum()
    {
        return $this->maximum;
    }

    /**
     * Convert to array for validator
     *
     * @return array
     */
    public function toArray()
    {
        $return = array(
            'minimum' => $this->getMinimum(),
            'maximum' => $this->getMaximum()
        );

        return $return;
    }

    /**
     * Validation meta data
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    abstract public function getValidationData(RequestInterface $parent = null);
}
