<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Request\Objects;

use CrefoPay\Library\Request\RequestInterface;
use CrefoPay\Library\Validation\Helper\Constants;

/**
 * Class HostedPagesText
 *
 * For hostedPagesText json Objects
 *
 * @link    https://docs.crefopay.de/api/#hostedpages-text
 * @package CrefoPay\Library\Request\Objects
 */
class HostedPagesText extends AbstractObject
{

    /**
     * Payment method: Direct Debit
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_DD = "DD";

    /**
     * Payment methods: Credit Card
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_CC = "CC";

    /**
     * Payment method: Credit Card 3D Secure
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_CC3D = "CC3D";

    /**
     * Payment method: Cash in advance
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_PREPAID = "PREPAID";

    /**
     * Payment method: PayPal
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_PAYPAL = "PAYPAL";

    /**
     * Payment method: Sofort
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_SU = "SU";

    /**
     * Payment method: Bill Payment
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_BILL = "BILL";

    /**
     * Payment method: Bill Payment Secure
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_BILL_SECURE = "BILL_SECURE";

    /**
     * Payment method: Cash on delivery
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_COD = "COD";

    /**
     * Payment method: Ideal
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_IDEAL = "IDEAL";

    /**
     * Payment method: Installment
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_INSTALLMENT = "INSTALLMENT";

    /**
     * Payment method: Dummy
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_DUMMY = "DUMMY";

    const TAG_PAYMENT_METHOD = "PAYMENT_METHOD_TYPE";

    /**
     * PaymentMethod code
     *
     * @var string Payment method type to change text on
     */
    private $paymentMethodType;

    /**
     * Fee to be added
     *
     * @var int Fee to be associated with a payment method. Must be given as an int
     */
    private $fee;

    /**
     * The text to be shown
     *
     * @var string Additional text to be shown with the method on the payment page
     */
    private $description;

    /**
     * Locale code
     *
     * @var string Locale determined communication language e.g. for e-mails
     */
    private $locale;

    /**
     * Set the payment method must be certain values
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_DD
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_CC
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_CC3D
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_PREPAID
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_PAYPAL
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_SU
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_BILL
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_COD
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_DUMMY
     *
     * @param $paymentMethodType string The payment method you want to set a property for
     *
     * @return $this
     */
    public function setPaymentMethodType($paymentMethodType)
    {
        $this->paymentMethodType = $paymentMethodType;
        return $this;
    }

    /**
     * Return the payment method that has been set
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_DD
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_CC
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_CC3D
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_PREPAID
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_PAYPAL
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_SU
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_BILL
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_COD
     * @see  \CrefoPay\Library\PaymentMethods\Methods::PAYMENT_METHOD_TYPE_DUMMY
     * @return string
     */
    public function getPaymentMethodType()
    {
        return $this->paymentMethodType;
    }

    /**
     * Set the fee as the lowest currency unit, ie cents, pence etc
     *
     * @param int $fee
     *
     * @return $this
     */
    public function setFee($fee)
    {
        $this->fee = $fee;
        return $this;
    }

    /**
     * Return the fee that has been set
     *
     * @return int
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * Set the description
     *
     * @param $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = utf8_encode($description);
        return $this;
    }

    /**
     * Get description field
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set the locale
     *
     * @link https://docs.crefopay.de/api/#languages
     * @see  \CrefoPay\Library\Locale\Codes::LOCALE_DE
     * @see  \CrefoPay\Library\Locale\Codes::LOCALE_EN
     * @see  \CrefoPay\Library\Locale\Codes::LOCALE_ES
     * @see  \CrefoPay\Library\Locale\Codes::LOCALE_FR
     * @see  \CrefoPay\Library\Locale\Codes::LOCALE_IT
     * @see  \CrefoPay\Library\Locale\Codes::LOCALE_NL
     *
     * @param $locale
     *
     * @return $this
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * Return the set Locale
     *
     * @link https://docs.crefopay.de/api/#languages
     * @see  \CrefoPay\Library\Locale\Codes::LOCALE_DE
     * @see  \CrefoPay\Library\Locale\Codes::LOCALE_EN
     * @see  \CrefoPay\Library\Locale\Codes::LOCALE_ES
     * @see  \CrefoPay\Library\Locale\Codes::LOCALE_FR
     * @see  \CrefoPay\Library\Locale\Codes::LOCALE_IT
     * @see  \CrefoPay\Library\Locale\Codes::LOCALE_NL
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Convert to array for validator
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'paymentMethodType' => $this->getPaymentMethodType(),
            'fee' => $this->getFee(),
            'description' => $this->getDescription(),
            'locale' => $this->getLocale()
        );
    }

    /**
     * Validation meta data
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['paymentMethodType'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "paymentMethodType is required"
        );

        $validationData['paymentMethodType'][] = array(
            'name' => 'Callback',
            'value' => 'CrefoPay\Library\PaymentMethods\Methods::validate',
            'message' => "paymentMethodType must be certain values"
        );

        $validationData['fee'][] = array(
            'name' => 'Regex',
            'value' => '/^[0-9]{1,16}$/',
            'message' => "Fee must be a numeric non decimal place value and no more than 16 characters"
        );

        $validationData['description'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Description is required"
        );

        $validationData['description'][] = array(
            'name' => 'MaxLength',
            'value' => '255',
            'message' => "Description must be no more than 255 characters long"
        );

        $validationData['locale'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Locale is required"
        );

        $validationData['locale'][] = array(
            'name' => 'Callback',
            'value' => 'CrefoPay\Library\Locale\Codes::validateLocale',
            'message' => "Locale must be certain values"
        );

        return $validationData;
    }

    /**
     * Validation the payment method type
     *
     * @param string $value
     *
     * @return bool
     */
    public static function validatePaymentMethodType($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_PAYMENT_METHOD);
    }
}
