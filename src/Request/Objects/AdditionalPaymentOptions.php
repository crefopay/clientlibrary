<?php
/**
 * Class file
 *
 * @author Vincent Mrose
 */

namespace CrefoPay\Library\Request\Objects;

use CrefoPay\Library\Request\RequestInterface;
use CrefoPay\Library\Validation\Helper\Constants;

/**
 * Class AdditionalPaymentOptions
 *
 * For additionalPaymentOptions json objects
 *
 * @link    https://docs.crefopay.de/api/#payment-options
 * @package CrefoPay\Library\Request\Objects
 * @since   1.0.14
 */
class AdditionalPaymentOptions extends AbstractObject
{
    const RECURRING_TYPE_FIRST = "FIRST";
    const RECURRING_TYPE_RECURRING = "RECURRING";
    const TAG_RECURRING_TRANSACTION_TYPE = "RECURRING_TYPE";

    /**
     * @var bool
     */
    private $recurringCustomer;

    /**
     * @var string
     */
    private $recurringTransactionType;

    /**
     * @param bool $recurringCustomer
     *
     * @return $this
     */
    public function setRecurringCustomer($recurringCustomer)
    {
        $this->recurringCustomer = $recurringCustomer;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRecurringCustomer()
    {
        return $this->recurringCustomer;
    }

    /**
     * @see AdditionalPaymentOptions::RECURRING_TYPE_FIRST
     * @see AdditionalPaymentOptions::RECURRING_TYPE_RECURRING
     *
     * @param string $recurringTransactionType
     *
     * @return $this
     */
    public function setRecurringTransactionType($recurringTransactionType)
    {
        $this->recurringTransactionType = $recurringTransactionType;
        return $this;
    }

    /**
     * @return string
     */
    public function getRecurringTransactionType()
    {
        return $this->recurringTransactionType;
    }

    public function toArray()
    {
        $objectArray = array();
        if ($this->recurringCustomer) {
            $objectArray['recurringCustomer'] = $this->isRecurringCustomer();
        }
        if ($this->recurringTransactionType) {
            $objectArray['recurringTransactionType'] = $this->getRecurringTransactionType();
        }
        return $objectArray;
    }

    /**
     * Provide validation information to the validator
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['recurringTransactionType'][] = array(
            'name' => 'Callback',
            'value' => get_class($this) . '::validateRecurringTransactionType',
            'message' => "recurringTransactionType must be one of the defined constants"
        );

        return $validationData;
    }

    /**
     * Validate the user type
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateRecurringTransactionType($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_RECURRING_TRANSACTION_TYPE);
    }
}