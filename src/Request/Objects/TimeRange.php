<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Request\Objects;

use CrefoPay\Library\Request\RequestInterface;

/**
 * Class TimeRange
 *
 * For the TimeRange json objects
 *
 * @link    https://docs.crefopay.de/api/#getsubscriptionplans
 * @package CrefoPay\Library\Request\Objects
 */
class TimeRange extends AbstractRange
{
    /**
     * Constructor
     *
     * @param int $minTime
     * @param int $maxTime
     */
    public function __construct($minTime = 0, $maxTime = 0)
    {
        if ($minTime > 0) {
            $this->setMinimum($minTime);
        }
        if ($maxTime > 0) {
            $this->setMaximum($maxTime);
        }
    }

    /**
     * Validation meta data
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['minimum'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Minimum time is required"
        );
        $validationData['maximum'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Maximum time is required"
        );
        $validationData['minimum'][] = array(
            'name' => 'Between',
            'value' => array('min' => 1, 'max' => 366),
            'message' => "Minimum time is not in the allowed range"
        );
        $validationData['maximum'][] = array(
            'name' => 'Between',
            'value' => array('min' => 1, 'max' => 366),
            'message' => "Maximum time is not in the allowed range"
        );

        return $validationData;
    }
}
