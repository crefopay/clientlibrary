<?php
/**
 * ClearingFile class file
 *
 * @author Vincent Mrose
 */

namespace CrefoPay\Library\Request\Objects;

use CrefoPay\Library\Request\RequestInterface;

/**
 * Class ClearingFile
 *
 * For the ClearingFile json objects
 *
 * @link    https://docs.crefopay.de/api/#clearingfile
 * @package CrefoPay\Library\Request\Objects
 */
class ClearingFile extends AbstractObject
{

    /**
     * ID of the clearing file
     *
     * @var string
     */
    private $clearingID;

    /**
     * Start date
     *
     * @var \DateTimeInterface
     */
    private $from;

    /**
     * End date
     *
     * @var \DateTimeInterface
     */
    private $to;

    /**
     * Set the clearing ID for the object
     *
     * @param string $id
     *
     * @return $this
     */
    public function setClearingID($id)
    {
        $this->clearingID = $id;
        return $this;
    }

    /**
     * Get the clearing ID that has been set
     *
     * @return string
     */
    public function getClearingID()
    {
        return $this->clearingID;
    }

    /**
     * Set the start date
     *
     * @param \DateTimeInterface $from
     *
     * @return $this
     */
    public function setFrom(\DateTimeInterface $from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * Return the start date
     *
     * @return \DateTimeInterface
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Set the vat rate
     *
     * @param \DateTimeInterface $to
     *
     * @return $this
     */
    public function setTo(\DateTimeInterface $to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * Get the end date
     *
     * @return \DateTimeInterface
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Returns objects' data as array
     *
     * @return array
     */
    public function toArray()
    {
        $return = array(
            'clearingID' => $this->clearingID,
        );

        if ($this->from instanceof \DateTimeInterface) {
            $return['from'] = $this->getFrom()->format('Y-m-d');
        }

        if ($this->to instanceof \DateTimeInterface) {
            $return['to'] = $this->getTo()->format('Y-m-d');
        }

        return $return;
    }

    /**
     * Validation meta data
     *
     * @see https://docs.crefopay.de/api/#clearingfile
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['clearingID'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "ClearingID is required"
        );
        $validationData['from'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Start date is required"
        );
        $validationData['to'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "End date is required"
        );

        return $validationData;
    }
}
