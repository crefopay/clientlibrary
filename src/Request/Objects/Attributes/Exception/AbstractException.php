<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Request\Objects\Attributes\Exception;

/**
 * Class AbstractException
 *
 * Abstract Exception for the Object validation
 *
 * @package CrefoPay\Library\Request\Objects\Attributes\Exception
 */
abstract class AbstractException extends \CrefoPay\Library\AbstractException
{
    /**
     * Constructor
     *
     * @param string $message
     */
    public function __construct($message = 'Attribute Exception')
    {
        parent::__construct($message);
    }
}
