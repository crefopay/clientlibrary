<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Request\Objects;

use DateTimeInterface;
use CrefoPay\Library\Request\RequestInterface;
use CrefoPay\Library\Validation\Helper\Constants;

/**
 * Class Creditreform
 *
 * For creditreform json objects
 *
 * @link    https://docs.crefopay.de/api/#creditreform
 * @package CrefoPay\Library\Request\Objects
 */
class Creditreform extends AbstractObject
{

    const PRODUCT_IDENTIFICATION = "IDENTIFICATION";
    const PRODUCT_RISK_CHECK = "RISK_CHECK";
    const PRODUCT_TRAFFIC_LIGHT_REPORT = "TRAFFIC_LIGHT_REPORT";
    const PRODUCT_BRIEF_REPORT = "BRIEF_REPORT";
    const PRODUCT_COMPACT_REPORT = "COMPACT_REPORT";
    const PRODUCT_E_CREFO = "E_CREFO";

    const TRAFFIC_LIGHT_NONE = "NONE";
    const TRAFFIC_LIGHT_RED = "RED";
    const TRAFFIC_LIGHT_YELLOW = "YELLOW";
    const TRAFFIC_LIGHT_GREEN = "GREEN";

    const TAG_PRODUCT_NAME = "PRODUCT";
    const TAG_TRAFFIC_LIGHT = "TRAFFIC_LIGHT";


    /**
     * Date of request
     *
     * @var DateTimeInterface Request date
     */
    private $requestDate;

    /**
     * Result of identification
     *
     * @var string Identification result
     */
    private $crefoProductName;

    /**
     * Result of address validation check
     *
     * @var boolean Address validation result
     */
    private $blackWhiteResult = null;

    /**
     * Result of traffic light check
     *
     * @var string Traffic Light Result
     */
    private $trafficLightResult;

    /**
     * Score from compact report
     *
     * @var int Score from compact report
     */
    private $compactScore;

    /**
     * Date of the companys foundation
     *
     * @var DateTimeInterface Date of the companys foundation
     */
    private $dateOfFoundation;

    /**
     * The legal form key for this company
     *
     * @var string The legal form key for this company
     */
    private $legalForm;

    /**
     * The WZ 2008 key for this company
     *
     * @var string The WZ 2008 key for this company
     */
    private $lineOfBusiness;

    /**
     * The email address of the company
     *
     * @var string The email address of the company
     */
    private $companyEmail;

    /**
     * Set the requestDate field
     *
     * @param DateTimeInterface $requestDate
     *
     * @return $this
     */
    public function setRequestDate(DateTimeInterface $requestDate)
    {
        $this->requestDate = $requestDate;

        return $this;
    }

    /**
     * Get the requestDate field
     *
     * @return DateTimeInterface
     */
    public function getRequestDate()
    {
        return $this->requestDate;
    }

    /**
     * Set the crefoProductName field
     *
     * @param string $crefoProductName
     *
     * @return $this
     */
    public function setCrefoProductName($crefoProductName)
    {
        $this->crefoProductName = $crefoProductName;

        return $this;
    }

    /**
     * Get the crefoProductName field
     *
     * @return string
     */
    public function getCrefoProductName()
    {
        return $this->crefoProductName;
    }

    /**
     * Set the blackWhiteResult field
     *
     * @param boolean $blackWhiteResult
     *
     * @return $this
     */
    public function setBlackWhiteResult($blackWhiteResult)
    {
        $this->blackWhiteResult = $blackWhiteResult;

        return $this;
    }

    /**
     * Get the blackWhiteResult field
     *
     * @return boolean
     */
    public function getBlackWhiteResult()
    {
        return $this->blackWhiteResult;
    }

    /**
     * Set the trafficLightResult field
     *
     * @param string $trafficLightResult
     *
     * @return $this
     */
    public function setTrafficLightResult($trafficLightResult)
    {
        $this->trafficLightResult = $trafficLightResult;

        return $this;
    }

    /**
     * Get the trafficLightResult field
     *
     * @return string
     */
    public function getTrafficLightResult()
    {
        return $this->trafficLightResult;
    }

    /**
     * @return int
     */
    public function getCompactScore()
    {
        return $this->compactScore;
    }

    /**
     * @param int $compactScore
     *
     * @return Creditreform
     */
    public function setCompactScore($compactScore)
    {
        $this->compactScore = $compactScore;

        return $this;
    }

    /**
     * @return DateTimeInterface
     */
    public function getDateOfFoundation()
    {
        return $this->dateOfFoundation;
    }

    /**
     * @param DateTimeInterface $dateOfFoundation
     *
     * @return Creditreform
     */
    public function setDateOfFoundation(DateTimeInterface $dateOfFoundation)
    {
        $this->dateOfFoundation = $dateOfFoundation;

        return $this;
    }

    /**
     * @return string
     */
    public function getLegalForm()
    {
        return $this->legalForm;
    }

    /**
     * @param string $legalForm
     *
     * @return Creditreform
     */
    public function setLegalForm($legalForm)
    {
        $this->legalForm = $legalForm;

        return $this;
    }

    /**
     * @return string
     */
    public function getLineOfBusiness()
    {
        return $this->lineOfBusiness;
    }

    /**
     * @param string $lineOfBusiness
     *
     * @return Creditreform
     */
    public function setLineOfBusiness($lineOfBusiness)
    {
        $this->lineOfBusiness = $lineOfBusiness;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyEmail()
    {
        return $this->companyEmail;
    }

    /**
     * @param string $companyEmail
     *
     * @return Creditreform
     */
    public function setCompanyEmail($companyEmail)
    {
        $this->companyEmail = $companyEmail;

        return $this;
    }

    /**
     * Convert to array for validator
     *
     * @return array
     */
    public function toArray()
    {
        $return = array(
            'crefoProductName' => $this->getCrefoProductName(),
            'trafficLightResult' => $this->getTrafficLightResult(),
        );
        if ($this->requestDate && $this->requestDate instanceof DateTimeInterface) {
            $return['requestDate'] = $this->getRequestDate()->format('Y-m-d');
        } else {
            $return['requestDate'] = "";
        }

        if (isset($this->blackWhiteResult)) {
            $return['blackWhiteResult'] = $this->getBlackWhiteResult();
        }

        if ($this->crefoProductName === self::PRODUCT_COMPACT_REPORT) {
            $return['compactScore'] = $this->getCompactScore();
            if ($this->dateOfFoundation) {
                $return['dateOfFoundation'] = $this->getDateOfFoundation()->format('Y-m-d');
            }
            if ($this->legalForm) {
                $return['legalForm'] = $this->getLegalForm();
            }
            if ($this->lineOfBusiness) {
                $return['lineOfBusiness'] = $this->getLineOfBusiness();
            }
            if ($this->companyEmail) {
                $return['companyEmail'] = $this->getCompanyEmail();
            }
        }

        return $return;
    }

    /**
     * Validation meta data
     *
     * @see https://docs.crefopay.de/api/#creditreform
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['requestDate'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "RequestDate is required",
        );

        $validationData['crefoProductName'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "CrefoProductName is required",
        );

        $validationData['crefoProductName'][] = array(
            'name' => 'Callback',
            'value' => get_class($this).'::validateCrefoProductName',
            'message' => "CrefoProductName must have a valid value",
        );

        if (isset($this->blackWhiteResult)) {
            $validationData['blackWhiteResult'][] = array(
                'name' => 'Callback',
                'value' => get_class($this).'::validateBlackWhiteResult',
                'message' => "BlackWhiteResult is not a valid value",
            );
        }

        $validationData['trafficLightResult'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "TrafficLightResult is required",
        );

        $validationData['trafficLightResult'][] = array(
            'name' => 'Callback',
            'value' => get_class($this).'::validateTrafficLight',
            'message' => "TrafficLightResult must have a valid value",
        );

        if ($this->crefoProductName === self::PRODUCT_COMPACT_REPORT) {
            $validationData['compactScore'][] = array(
                'name' => 'required',
                'value' => null,
                'message' => "CompactScore is required",
            );
            $validationData['compactScore'][] = array(
                'name' => 'integer',
                'value' => null,
                'message' => "CompactScore must be an integer",
            );
            $validationData['compactScore'][] = array(
                'name' => 'Between',
                'value' => array('min' => '1', 'max' => '600',),
                'message' => "CompactScore must be between 1 and 600",
            );
            $validationData['dateOfFoundation'][] = array(
                'name' => 'Callback',
                'value' => get_class($this).'::validateDateOfFoundation',
                'message' => "DateOfFoundation must be a date in the past",
            );
            $validationData['companyEmail'][] = array(
                'name' => 'Email',
                'value' => null,
                'message' => "CompanyEmail must be a valid email",
            );
        }

        return $validationData;
    }

    /**
     * Validate the crefoProductName type
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateCrefoProductName($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_PRODUCT_NAME);
    }

    /**
     * Validate the trafficLight type
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateTrafficLight($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_TRAFFIC_LIGHT);
    }

    /**
     * Validate the blackWhite type
     *
     * @param $value
     *
     * @return boolean
     */
    public static function validateBlackWhiteResult($value)
    {
        return $value === true || $value === false;
    }

    /**
     * Validate the date of foundation
     *
     * @param $value
     *
     * @return boolean
     */
    public static function validateDateOfFoundation($value)
    {
        try {
            $now = new \DateTime();
            $dateOfFoundation = new \DateTime($value);

            return ($now->getTimestamp() > $dateOfFoundation->getTimestamp());

        } catch (\Exception $e) {
            return false;
        }
    }
}
