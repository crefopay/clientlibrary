<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Request\Objects;

use CrefoPay\Library\Request\RequestInterface;

/**
 * Class AmountRange
 *
 * For the AmountRange json objects
 *
 * @link    https://docs.crefopay.de/api/#getsubscriptionplans
 * @package CrefoPay\Library\Request\Objects
 */
class AmountRange extends AbstractRange
{
    /**
     * Constructor
     *
     * @param Amount $minAmount
     * @param Amount $maxAmount
     */
    public function __construct(Amount $minAmount = null, Amount $maxAmount = null)
    {
        if (!is_null($minAmount)) {
            $this->setMinimum($minAmount);
        }
        if (!is_null($maxAmount)) {
            $this->setMaximum($maxAmount);
        }
    }

    /**
     * Validation meta data
     *
     * @link https://docs.crefopay.de/api/#getsubscriptionplans
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['minimum'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Minimum amount is required"
        );
        $validationData['maximum'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "Maximum amount is required"
        );

        return $validationData;
    }
}
