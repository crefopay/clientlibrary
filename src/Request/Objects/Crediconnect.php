<?php
/**
 * Class file
 *
 * @author Vincent Mrose
 */

namespace CrefoPay\Library\Request\Objects;

use CrefoPay\Library\Request\RequestInterface;
use CrefoPay\Library\Validation\Helper\Constants;

/**
 * Class Crediconnect
 *
 * For crediconnect json objects
 *
 * @link    https://docs.crefopay.de/api/#crediconnect
 * @package CrefoPay\Library\Request\Objects
 */
class Crediconnect extends AbstractObject
{

    const TRAFFIC_LIGHT_NONE = "NONE";
    const TRAFFIC_LIGHT_RED = "RED";
    const TRAFFIC_LIGHT_YELLOW = "YELLOW";
    const TRAFFIC_LIGHT_GREEN = "GREEN";

    const TAG_TRAFFIC_LIGHT = "TRAFFIC_LIGHT";

    /**
     * @var \DateTimeInterface Request date
     */
    private $requestDate;

    /**
     * @var string Traffic Light Result
     */
    private $trafficLightResult;

    /**
     * @return \DateTimeInterface
     */
    public function getRequestDate()
    {
        return $this->requestDate;
    }

    /**
     * @param \DateTimeInterface $requestDate
     *
     * @return Crediconnect
     */
    public function setRequestDate($requestDate)
    {
        $this->requestDate = $requestDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getTrafficLightResult()
    {
        return $this->trafficLightResult;
    }

    /**
     * @param string $trafficLightResult
     *
     * @return Crediconnect
     */
    public function setTrafficLightResult($trafficLightResult)
    {
        $this->trafficLightResult = $trafficLightResult;
        return $this;
    }

    /**
     * Convert to array for validator
     *
     * @return array
     */
    public function toArray()
    {
        $data = array(
            'trafficLightResult' => $this->getTrafficLightResult(),
        );
        if ($this->requestDate && $this->requestDate instanceof \DateTimeInterface) {
            $data['requestDate'] = $this->getRequestDate()->format('Y-m-d');
        } else {
            $data['requestDate'] = "";
        }

        return $data;
    }

    /**
     * Validation data
     *
     * @see https://docs.crefopay.de/api/#crediconnect
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['requestDate'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "RequestDate is required"
        );

        $validationData['trafficLightResult'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "TrafficLightResult is required"
        );

        $validationData['trafficLightResult'][] = array(
            'name' => 'Callback',
            'value' => get_class($this) . '::validateTrafficLight',
            'message' => "TrafficLightResult must have a valid value"
        );

        return $validationData;
    }

    /**
     * Validate the trafficLightResult type
     *
     * @param $value
     *
     * @return mixed
     */
    public static function validateTrafficLight($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_TRAFFIC_LIGHT);
    }

}