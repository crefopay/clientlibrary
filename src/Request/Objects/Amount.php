<?php
/**
 * Amount class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Request\Objects;

use CrefoPay\Library\Request\RequestInterface;

/**
 * Class Amount
 *
 * For the Amount json objects
 *
 * @link    https://docs.crefopay.de/api/#amount
 * @package CrefoPay\Library\Request\Objects
 */
class Amount extends AbstractObject
{

    /**
     * Amount in smallest possible denomination ie cents, pence etc including all taxes
     *
     * @var int
     */
    private $amount;

    /**
     * Amount in smallest possible denomination ie cents, pence etc excluding all taxes
     *
     * @var int
     */
    private $netAmount;

    /**
     * Constructor
     *
     * @param int $amount
     * @param int $netAmount
     */
    public function __construct($amount = 0, $netAmount = 0)
    {
        if ($amount >= 0) {
            $this->setAmount($amount);
            $this->setNetAmount($netAmount);
        }
    }

    /**
     * Set the amount for the object
     *
     * @param int $amount
     *
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Get the amount that has been set
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set the net amount
     *
     * @param int $netAmount
     *
     * @return $this
     */
    public function setNetAmount($netAmount)
    {
        $this->netAmount = $netAmount;
        return $this;
    }

    /**
     * Return the net amount that has been set
     *
     * @return int
     */
    public function getNetAmount()
    {
        return $this->netAmount;
    }

    /**
     * Returns an array of all set values in this instance
     *
     * @return array
     */
    public function toArray()
    {
        $return = array(
            'amount' => $this->amount,
        );

        if ($this->netAmount) {
            $return['netAmount'] = $this->netAmount;
        }

        return $return;
    }

    /**
     * Validation meta data
     *
     * @see https://docs.crefopay.de/api/#amount
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['amount'][] = array('name' => 'required', 'value' => null, 'message' => "Amount is required");
        $validationData['amount'][] = array(
            'name' => 'Integer',
            'value' => null,
            'message' => "Amount must be an integer"
        );
        $validationData['amount'][] = array(
            'name' => 'Regex',
            'value' => '/^\d{0,16}$/',
            'message' => "Amount must be between 1 and 16 digits"
        );

        $validationData['netAmount'][] = array(
            'name' => 'Integer',
            'value' => null,
            'message' => "NetAmount must be an integer"
        );
        $validationData['netAmount'][] = array(
            'name' => 'Regex',
            'value' => '/^\d{0,16}$/',
            'message' => "NetAmount must be between 1 and 16 digits"
        );

        return $validationData;
    }
}
