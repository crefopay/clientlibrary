<?php
/**
 * AddPayment class file
 *
 * @author Vincent Mrose
 */

namespace CrefoPay\Library\Request;

use DateTimeInterface;
use CrefoPay\Library\Request\Objects\Amount;

/**
 * Class AddPayment
 *
 * This is the request class for any addPayment request
 *
 * @link    https://docs.crefopay.de/api/#addpayment
 * @package CrefoPay\Library\Request
 */
class AddPayment extends AbstractRequest
{
    /**
     * This is the order number of the shop.
     *
     * This id is created by the shop and is used as identifier for this API call
     *
     * @var string
     */
    private $orderID;

    /**
     * This is the unique reference of a capture or a partial capture (e.g. the invoice number)
     *
     * @var string
     */
    private $captureID;

    /**
     * Amount of the payment
     *
     * @var int
     */
    private $amount;

    /**
     * Date of the payment
     *
     * Date on which the payment was received.
     *
     * @var DateTimeInterface
     */
    private $valutaDate;

    /**
     * The IBAN the payment originated from
     *
     * IBAN of the bank account from which the payment was received.
     * Required if a refund is to be performed on this payment.
     *
     * @var string
     */
    private $iban;

    /**
     * The account holders name
     *
     * The account holder of the bank account from which the payment was received.
     * Required if a refund is to be performed on this payment later on.
     *
     * @var string
     */
    private $accountHolder;

    /**
     * The payment description
     *
     * @var string
     */
    private $message;

    /**
     * Set the Order ID
     *
     * @param string $orderID
     *
     * @return $this
     * @see AddPayment::$orderID
     *
     */
    public function setOrderID($orderID)
    {
        $this->orderID = $orderID;
        return $this;
    }

    /**
     * Get the set order ID
     *
     * @return string
     * @see AddPayment::$orderID
     */
    public function getOrderID()
    {
        return $this->orderID;
    }

    /**
     * Set the captureID field
     *
     * @param $captureID
     *
     * @return $this
     * @see AddPayment::$captureID
     *
     */
    public function setCaptureID($captureID)
    {
        $this->captureID = $captureID;
        return $this;
    }

    /**
     * Get the captureID field
     *
     * @return string
     * @see AddPayment::$captureID
     */
    public function getCaptureID()
    {
        return $this->captureID;
    }

    /**
     * Set the amount field
     *
     * @param int $amount
     *
     * @return $this
     * @see AddPayment::$amount
     *
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * Get the amount field
     *
     * @return int
     * @see AddPayment::$amount
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return DateTimeInterface
     * @see AddPayment::$valutaDate
     */
    public function getValutaDate()
    {
        return $this->valutaDate;
    }

    /**
     * @param DateTimeInterface $valutaDate
     *
     * @return $this
     * @see AddPayment::$valutaDate
     */
    public function setValutaDate(DateTimeInterface $valutaDate)
    {
        $this->valutaDate = $valutaDate;
        return $this;
    }

    /**
     * @return string
     * @see AddPayment::$iban
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * @param string $iban
     *
     * @return $this
     * @see AddPayment::$iban
     */
    public function setIban($iban)
    {
        $this->iban = $iban;
        return $this;
    }

    /**
     * @return string
     * @see AddPayment::$accountHolder
     */
    public function getAccountHolder()
    {
        return $this->accountHolder;
    }

    /**
     * @param string $accountHolder
     *
     * @return $this
     * @see AddPayment::$accountHolder
     */
    public function setAccountHolder($accountHolder)
    {
        $this->accountHolder = $accountHolder;
        return $this;
    }

    /**
     * @return string
     * @see AddPayment::$message
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return $this
     * @see AddPayment::$message
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Get the serializer data
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        $data = array(
            'orderID' => $this->getOrderID(),
            'captureID' => $this->getCaptureID(),
            'amount' => $this->getAmount()
        );

        if (!empty($this->valutaDate)) {
            $data['valutaDate'] = $this->getValutaDate()->format('Y-m-d');
        }

        if (!empty($this->iban)) {
            $data['iban'] = $this->getIban();
        }

        if (!empty($this->accountHolder)) {
            $data['accountHolder'] = $this->getAccountHolder();
        }

        if (!empty($this->message)) {
            $data['message'] = $this->getMessage();
        }

        return $data;

    }

    /**
     * Get the validation
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getClassValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['orderID'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "orderID is required"
        );

        $validationData['orderID'][] = array(
            'name' => 'MaxLength',
            'value' => '50',
            'message' => "orderID must be between 1 and 50 characters"
        );

        $validationData['captureID'][] = array(
            'name' => 'MaxLength',
            'value' => '50',
            'message' => "captureID must be between 1 and 50 characters"
        );

        $validationData['amount'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "amount is required"
        );

        $validationData['amount'][] = array(
            'name' => 'MaxLength',
            'value' => '16',
            'message' => "amount must be at maximum 16 digits"
        );

        $validationData['valutaDate'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "valutaDate is required"
        );

        $validationData['iban'][] = array(
            'name' => 'MaxLength',
            'value' => '34',
            'message' => "iban must be at maximum 34 characters"
        );

        $validationData['accountHolder'][] = array(
            'name' => 'MaxLength',
            'value' => '50',
            'message' => "accountHolder must be at maximum 50 characters"
        );

        $validationData['message'][] = array(
            'name' => 'MaxLength',
            'value' => '255',
            'message' => "message must be at maximum 255 characters"
        );

        return $validationData;
    }
}
