<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Request;

/**
 * Class GetTransactionPaymentInstruments
 *
 * The getTransactionPaymentInstruments call adds the functionality to get a transactions‘s payment instruments.
 * Input data consist of the following:
 * Order information (existing order-id)
 *
 * @link    https://docs.crefopay.de/api/#gettransactionpaymentinstruments
 * @package CrefoPay\Library\Request
 */
class GetTransactionPaymentInstruments extends AbstractRequest
{
    /**
     * This is the order number of the shop.
     *
     * This id is created by the shop and is used as identifier for this transaction
     *
     * @var string
     */
    private $orderID;

    /**
     * Set the Order ID
     *
     * @param string $orderID
     *
     * @return $this
     */
    public function setOrderID($orderID)
    {
        $this->orderID = $orderID;
        return $this;
    }

    /**
     * Get the set order ID
     *
     * @return string
     */
    public function getOrderID()
    {
        return $this->orderID;
    }

    /**
     * Get the serializer data
     *
     * @return array
     */
    public function getPreSerializerData()
    {
        return array(
            'orderID' => $this->getOrderID(),
        );
    }

    /**
     * Get the validation
     *
     * @param RequestInterface|null $parent
     *
     * @return array
     */
    public function getClassValidationData(RequestInterface $parent = null)
    {
        $validationData = array();

        $validationData['orderID'][] = array(
            'name' => 'required',
            'value' => null,
            'message' => "orderID is required"
        );

        $validationData['orderID'][] = array(
            'name' => 'MaxLength',
            'value' => '50',
            'message' => "orderID must be between 1 and 50 characters"
        );

        return $validationData;
    }
}
