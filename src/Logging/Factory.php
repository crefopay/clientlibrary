<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Logging;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use CrefoPay\Library\Config;

/**
 * Class Factory
 *
 * Provides an Logger
 *
 * @package CrefoPay\Library\Logging
 */
class Factory
{
    /**
     * Array of initialized loggers
     *
     * @var array
     */
    private static $loggers = array();

    /**
     * Gets a logger based on the logLocation
     *
     * @param Config $config
     * @param        $logLocation
     *
     * @return LoggerInterface
     */
    public static function getLogger(Config $config, $logLocation)
    {
        $log = new Blank();
        if ($config->getLogEnabled()) {
            if (!array_key_exists($logLocation, self::$loggers)) {
                $log = new Logger('crefopay');
                try {
                    $log->pushHandler(new StreamHandler($logLocation, $config->getLogLevel()));
                } catch (\Exception $e) {
                }
                self::$loggers[$logLocation] = $log;
            }
            return self::$loggers[$logLocation];
        }
        return $log;
    }
}
