<?php

/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Mns;

/**
 * Interface ProcessorInterface
 *
 * For the integrator to use to process incoming MNS notices.
 * Please note for this script please do not do any processing beyond saving the MNS to the database.
 * We recommend that the order actions should be processed using a cron job and this interface should be used for the
 * implementation of the database saving code
 *
 * @package CrefoPay\Library\Mns
 */
interface ProcessorInterface
{
    /**
     * Send data to the processor that will be used in the run method
     *
     * @param string  $additionalData     JSON string with additional data
     * @param string  $amount             This is either the amount of an incoming payment or “0” in case of some status
     *                                    changes
     * @param string  $captureID          The confirmation ID of the capture. Only sent for Notifications that belong to
     *                                    captures
     * @param string  $captureStatus      Current status of the capture. Possible values: PAYPENDING, PAID, CLEARED, 
     *                                    PAYMENTFAILED, CHARGEBACK, INDUNNING, IN_COLLECTION
     * @param string  $currency           Currency code according to ISO4217.
     * @param integer $merchantID         This is the merchantID assigned by CrefoPay.
     * @param string  $merchantReference  Reference that can be set by the merchant during the createTransaction call.
     * @param string  $orderID            This is the order number that the shop has assigned
     * @param string  $orderStatus        Current status of the order. Possible values: PAID, PAYPENDING, PAYMENTFAILED,
     *                                    CHARGEBACK, CLEARED.
     * @param string  $paymentReference   The reference number of the
     * @param string  $storeID            This is the store ID of a merchant assigned by CrefoPay as a merchant can have
     *                                    more than one store.
     * @param string  $subscriptionId     This is a unique identifier for a subscription which is created by the shop. 
     *                                    This value is only set, if the transaction is related to a subscription.
     * @param string  $timestamp          Unix timestamp, Notification timestamp
     * @param integer $transactionBalance The sum of all received payments minus the sum of all captured amounts plus all 
     *                                    reductions. This value is negative when still waiting for payments,
     *                                    positive in case a transaction was overpaid, and zero when the debt is paid 
     *                                    exactly or no capture was performed yet.
     * @param string  $transactionStatus  Current status of the transaction. Same values as resultCode
     * @param string  $userID             The unique user id of the customer.
     * @param string  $version            notification version (currently 2.3)
     *
     * @link https://docs.crefopay.de/api/#notification-call
     */
    public function sendData(
        $additionalData,
        $amount,
        $captureID,
        $captureStatus,
        $currency,
        $merchantID,
        $merchantReference,
        $orderID,
        $orderStatus,
        $paymentReference,
        $storeID,
        $subscriptionId,
        $timestamp,
        $transactionBalance,
        $transactionStatus,
        $userID,
        $version
    );

    /**
     * The run method used by the processor to run successfully validated MNS notifications.
     *
     * This should not return anything
     */
    public function run();
}
