<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Mns\Exception;

/**
 * Class AbstractException
 *
 * For MNS exceptions
 *
 * @package CrefoPay\Library\Mns\Exception
 */
abstract class AbstractException extends \CrefoPay\Library\AbstractException
{
    /**
     * Constructor
     *
     * @param string $message
     */
    public function __construct($message = 'MNS Exception')
    {
        parent::__construct($message);
    }
}
