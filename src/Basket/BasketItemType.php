<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Basket;

use CrefoPay\Library\Validation\Helper\Constants;

/**
 * Class BasketItemType
 *
 * Basket Item Type class definitions
 *
 * @package CrefoPay\Library\Basket
 * @link    https://docs.crefopay.de/api/#basket-item
 */
class BasketItemType
{
    /**
     * Default basket item type
     */
    const BASKET_ITEM_TYPE_DEFAULT = 'DEFAULT';

    /**
     * Type for basket item which represents the shipping cost
     */
    const BASKET_ITEM_TYPE_SHIPPINGCOST = 'SHIPPINGCOSTS';

    /**
     * Type for an basket item which represents any coupons applied to the order
     */
    const BASKET_ITEM_TYPE_COUPON = 'COUPON';

    /**
     * Validation function for this class
     *
     * @param string $value
     *
     * @return bool
     */
    public static function validateBasketItemType($value)
    {
        return Constants::validateConstant(__CLASS__, $value, 'BASKET_ITEM_TYPE');
    }
}
