<?php

namespace CrefoPay\Library\Validation\Helper;

class RegexValidator extends \Sirius\Validation\Rule\Regex
{
    public function validate($value, string $valueIdentifier = null): bool
    {
        if (is_int($value))
        {
            $value=''.$value;
        }
        elseif (is_float($value))
        {
            $value=''.$value;
        }

        return parent::validate($value, $valueIdentifier);
    }
}