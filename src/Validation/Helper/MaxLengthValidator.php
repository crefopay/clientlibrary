<?php

namespace CrefoPay\Library\Validation\Helper;

use Sirius\Validation\Rule\MaxLength;

class MaxLengthValidator extends MaxLength
{
    public function validate($value, string $valueIdentifier = null): bool
    {
        if (is_int($value))
        {
            $value=''.$value;
        }
        elseif (is_float($value))
        {
            $value=''.$value;
        }

        return parent::validate($value, $valueIdentifier);
    }
}