<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Validation;

use CrefoPay\Library\Request\RequestInterface as RequestInterface;

/**
 * Interface WrapperInterface
 *
 * Interface for the validator
 *
 * @package CrefoPay\Library\Validation
 */
interface WrapperInterface
{
    /**
     * Gets the validator for the given requests' class
     *
     * @param RequestInterface $request
     */
    public function getValidator(RequestInterface $request);
}
