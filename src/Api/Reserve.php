<?php
/**
 * Reserve class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class Reserve
 * API stub for the reserve method
 *
 * @link    https://docs.crefopay.de/api/#reserve
 * @package CrefoPay\Library\Api
 */
class Reserve extends AbstractApi
{
    /**
     * The URI for the api endpoint
     */
    const RESERVE_PATH = 'reserve';

    /**
     * Constructor
     *
     * @param Config                       $config  Config for merchant
     * @param \CrefoPay\Library\Request\Reserve $request Request to be sent
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\Reserve $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Get the URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::RESERVE_PATH);
    }
}
