<?php
/**
 * GetUser class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class GetUser
 * APi stub for the getUser method
 *
 * @link    https://docs.crefopay.de/api/#getuser
 * @package CrefoPay\Library\Api
 */
class GetUser extends AbstractApi
{
    /**
     * URI for the user path
     */
    const GET_USER_PATH = 'getUser';

    /**
     * Construct the API stub class
     *
     * @param Config                       $config  Merchant config
     * @param \CrefoPay\Library\Request\GetUser $request Request to be sent
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\GetUser $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Get the URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::GET_USER_PATH);
    }
}
