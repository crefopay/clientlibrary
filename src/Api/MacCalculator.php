<?php
/**
 * MacCalculator class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Api\Exception\JsonDecode;
use CrefoPay\Library\Api\Exception\MacValidation;
use CrefoPay\Library\Config;
use CrefoPay\Library\Logging\Factory;
use CrefoPay\Library\Mac\AbstractCalculator;
use CrefoPay\Library\Mac\Exception\MacInvalid;

/**
 * Class MacCalculator
 *
 * @link
 * @package CrefoPay\Library\Api
 */
class MacCalculator extends AbstractCalculator
{

    /**
     * Raw request string
     *
     * @var string
     */
    private $rawRequest;

    /**
     * Received MAC
     *
     * @var string
     */
    private $mac;

    /**
     * Logger instance
     *
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * Constructor for the validator
     *
     * @param Config $config Merchant config
     */
    public function __construct(Config $config)
    {
        $this->logger = Factory::getLogger($config, $config->getLogLocationRequest());
        $this->setConfig($config);
    }

    /**
     * Set the response to validate
     *
     * @param string $response The response
     * @param string $headers  The headers
     *
     * @return $this
     *
     * @throws JsonDecode
     */
    public function setResponse($response, $headers)
    {
        $this->rawRequest = $response;

        $responseData = json_decode($response, true);

        if (!is_array($responseData)) {
            $code = json_last_error();
            throw new JsonDecode($code, $response);
        }

        if (!empty($headers)) {
            $this->mac = $this->lookUpMacInRawHeader($headers);
        } else {
            $this->mac = array_key_exists('mac', $responseData) ? $responseData['mac'] : '';
        }

        $this->setCalculationArray(array('response' => $response));
        return $this;
    }

    /**
     * Finds the MAC in the raw header string
     *
     * @param string $header
     *
     * @return string
     */
    private function lookUpMacInRawHeader($header)
    {
        $headerArray = explode("\r\n", $header);

        foreach ($headerArray as $value) {
            if (stripos($value, 'X-Payco-HMAC:') === 0) {
                return trim(str_ireplace('X-Payco-HMAC:', '', $value));
            }
        }

        return '';
    }

    /**
     * Validate the Response
     *
     * @return bool
     *
     * @throws MacValidation
     * @throws MacInvalid
     */
    public function validateResponse()
    {
        if (!parent::validate($this->mac, false)) {
            $this->logger->error("Invalid mac from api response. Expected: '" .
                $this->mac . "', got '" . $this->calculateMac() . "'");
            $this->logger->debug("Invalid mac from api for request: " .
                $this->rawRequest . ". Expected: '" . $this->mac . "', got '" . $this->calculateMac() . "'");
            throw new MacValidation($this->calculateMac(), $this->mac, $this->rawRequest);
        }

        return true;
    }
}
