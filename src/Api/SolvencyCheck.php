<?php
/**
 * SolvencyCheck class file
 *
 * @author Vincent Mrose
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class SolvencyCheck
 * Stub for the solvencyCheck call
 *
 * @link    https://docs.crefopay.de/api/#solvencycheck
 * @package CrefoPay\Library\Api
 */
class SolvencyCheck extends AbstractApi
{
    /**
     * URI to the api end point
     */
    const SOLVENCY_CHECK_PATH = 'solvencyCheck';

    /**
     * Constructor
     *
     * @param Config                             $config  Merchant config
     * @param \CrefoPay\Library\Request\SolvencyCheck $request Request to be sent
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\SolvencyCheck $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Get the URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();

        return $this->combineUrlUri($baseUrl, self::SOLVENCY_CHECK_PATH);
    }
}
