<?php
/**
 * Capture class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class Capture
 * The API stub for the capture call
 *
 * @link    https://docs.crefopay.de/api/#capture
 * @package CrefoPay\Library\Api
 */
class Capture extends AbstractApi
{
    /**
     * URI for the capture call
     */
    const CAPTURE_PATH = 'capture';

    /**
     * Construct the API stub
     *
     * @param Config                       $config  Config for the merchant
     * @param \CrefoPay\Library\Request\Capture $request Request for the capture that is to be sent
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\Capture $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Return the full url using base url in the config
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::CAPTURE_PATH);
    }
}
