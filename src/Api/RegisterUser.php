<?php
/**
 * RegisterUser class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class RegisterUser
 * Stub for the registerUser call
 *
 * @link    https://docs.crefopay.de/api/#registeruser
 * @package CrefoPay\Library\Api
 */
class RegisterUser extends AbstractApi
{
    /**
     * URI to the api end point
     */
    const REGISTER_USER_PATH = 'registerUser';

    /**
     * Constructor
     *
     * @param Config                            $config  Merchant config
     * @param \CrefoPay\Library\Request\RegisterUser $request Request to be sent
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\RegisterUser $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Get the URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::REGISTER_USER_PATH);
    }
}
