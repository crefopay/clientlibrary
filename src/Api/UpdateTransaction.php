<?php
/**
 * UpdateTransaction class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class UpdateTransaction
 *
 * @link    https://docs.crefopay.de/api/#updateinvoice
 * @package CrefoPay\Library\Api
 * @deprecated 1.1.3
 */
class UpdateTransaction extends AbstractApi
{
    /**
     * URI for the updateTransaction call
     */
    const UPDATE_TRANSACTION_PATH = 'updateTransaction';

    /**
     * Constructor
     *
     * @param Config                                 $config  Config for merchant
     * @param \CrefoPay\Library\Request\UpdateTransaction $request Request to be sent
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\UpdateTransaction $request)
    {
        $this->request = $request;
        $this->submitType = self::SUBMIT_TYPE_MULTIPART;
        parent::__construct($config);
    }

    /**
     * Get the url
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::UPDATE_TRANSACTION_PATH);
    }
}
