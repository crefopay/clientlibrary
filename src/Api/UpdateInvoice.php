<?php
/**
 * UpdateInvoice class file
 *
 * @author Vincent Mrose
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class UpdateInvoice
 *
 * @link    https://docs.crefopay.de/api/#updatetransaction
 * @package CrefoPay\Library\Api
 */
class UpdateInvoice extends AbstractApi
{
    /**
     * URI for the updateTransaction call
     */
    const UPDATE_INVOICE_PATH = 'updateInvoice';

    /**
     * Constructor
     *
     * @param Config                             $config  Config for merchant
     * @param \CrefoPay\Library\Request\UpdateInvoice $request Request to be sent
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\UpdateInvoice $request)
    {
        $this->request = $request;
        $this->submitType = self::SUBMIT_TYPE_MULTIPART;
        parent::__construct($config);
    }

    /**
     * Get the url
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::UPDATE_INVOICE_PATH);
    }
}
