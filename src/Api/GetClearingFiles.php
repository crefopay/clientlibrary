<?php
/**
 * GetClearingFiles class file
 *
 * @author Vincent Mrose
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class GetClearingFiles
 * The API stub method for the getClearingFiles call
 *
 * @link    https://docs.crefopay.de/api/#getclearingfiles
 * @package CrefoPay\Library\Api
 */
class GetClearingFiles extends AbstractApi
{
    /**
     * URI for the call
     */
    const GET_CLEARING_FILES_PATH = 'getClearingFiles';

    /**
     * Construct the stub
     *
     * @param Config                                $config  Merchant config
     * @param \CrefoPay\Library\Request\GetClearingFiles $request Request to be sent
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\GetClearingFiles $request)
    {
        $this->request = $request;
        $this->awaitedResponse = self::RESPONSE_TYPE_OCTET_STREAM;
        parent::__construct($config);
    }

    /**
     * Get the URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::GET_CLEARING_FILES_PATH);
    }
}
