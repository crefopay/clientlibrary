<?php
/**
 * GetTransactionStatus class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class GetTransactionStatus
 * Api stub for getTransactionStatus
 *
 * @link    https://docs.crefopay.de/api/#gettransactionstatus
 * @package CrefoPay\Library\Api
 */
class GetTransactionStatus extends AbstractApi
{
    const UPDATE_TRANSACTION_STATUS_PATH = 'getTransactionStatus';

    /**
     * Constructor
     *
     * @param Config                                    $config
     * @param \CrefoPay\Library\Request\GetTransactionStatus $request
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\GetTransactionStatus $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Returns API endpoint URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::UPDATE_TRANSACTION_STATUS_PATH);
    }
}
