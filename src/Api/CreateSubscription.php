<?php
/**
 * CreateSubscription class file
 *
 * @author Vincent Mrose
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class CreateSubscription
 * API stub for the CreateSubscription call
 *
 * @link    https://docs.crefopay.de/api/#createsubscription
 * @package CrefoPay\Library\Api
 */
class CreateSubscription extends AbstractApi
{
    /**
     * URI for the createSubscription call
     */
    const CREATE_SUBSCRIPTION_PATH = 'createSubscription';

    /**
     * Construct the API stub class
     *
     * @param Config                                  $config  Config for store owner
     * @param \CrefoPay\Library\Request\CreateSubscription $request Request to be sent
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\CreateSubscription $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Get the url
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::CREATE_SUBSCRIPTION_PATH);
    }
}
