<?php
/**
 * Refund class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class Refund
 * Stub method for the refund call
 *
 * @link    https://docs.crefopay.de/api/#refund
 * @package CrefoPay\Library\Api
 */
class Refund extends AbstractApi
{
    /**
     * URI for the endpoint
     */
    const REFUND_PATH = 'refund';

    /**
     * Construct the API stub
     *
     * @param Config                      $config  Merchant config
     * @param \CrefoPay\Library\Request\Refund $request Request to be sent
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\Refund $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Get the URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::REFUND_PATH);
    }
}
