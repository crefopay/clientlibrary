<?php
/**
 * UpdateUser class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class UpdateUser
 * API stub for updateUser
 *
 * @link    https://docs.crefopay.de/api/#updateuser
 * @package CrefoPay\Library\Api
 */
class UpdateUser extends AbstractApi
{
    /**
     * The URI
     */
    const UPDATE_USER_PATH = 'updateUser';

    /**
     * Constructor
     *
     * @param Config                            $config  Merchant Config
     * @param \CrefoPay\Library\Request\RegisterUser $request Request to be sent
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\RegisterUser $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Get the URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::UPDATE_USER_PATH);
    }
}
