<?php
/**
 * Finish class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class Finish
 * API stub for the finish call
 *
 * @link    https://docs.crefopay.de/api/#finish
 * @package CrefoPay\Library\Api
 */
class Finish extends AbstractApi
{
    /**
     * URI for the finish method
     */
    const FINISH_PATH = 'finish';

    /**
     * Construct the API stub
     *
     * @param Config                      $config  Merchant config
     * @param \CrefoPay\Library\Request\Finish $request Request to be sent
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\Finish $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * URL for the request
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::FINISH_PATH);
    }
}
