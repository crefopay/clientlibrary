<?php
/**
 * UpdateSubscription class file
 *
 * @author Vincent Mrose
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class UpdateSubscription
 * Api stub for updateSubscription
 *
 * @link    https://docs.crefopay.de/api/#updatesubscription
 * @package CrefoPay\Library\Api
 */
class UpdateSubscription extends AbstractApi
{
    const UPDATE_SUBSCRIPTION_STATUS_PATH = 'updateSubscription';

    /**
     * Constructor
     *
     * @param Config                                  $config
     * @param \CrefoPay\Library\Request\UpdateSubscription $request
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\UpdateSubscription $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Returns API endpoint URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::UPDATE_SUBSCRIPTION_STATUS_PATH);
    }
}
