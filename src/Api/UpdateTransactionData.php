<?php
/**
 * UpdateTransactionData class file
 *
 * @author Vincent Mrose
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class UpdateTransactionData
 * Api stub for updateTransactionData
 *
 * @package CrefoPay\Library\Api
 */
class UpdateTransactionData extends AbstractApi
{
    const UPDATE_TRANSACTION_DATA_PATH = 'updateTransactionData';

    /**
     * Constructor
     *
     * @param Config                                     $config
     * @param \CrefoPay\Library\Request\UpdateTransactionData $request
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\UpdateTransactionData $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Returns API endpoint URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::UPDATE_TRANSACTION_DATA_PATH);
    }
}
