<?php
/**
 * AddPayment class file
 *
 * @author Vincent Mrose
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class AddPayment
 * The API stub for the addPayment call
 *
 * @link    https://docs.crefopay.de/api/#addpayment
 * @package CrefoPay\Library\Api
 */
class AddPayment extends AbstractApi
{
    /**
     * URI for the addPayment call
     */
    const ADD_PAYMENT_PATH = 'addPayment';

    /**
     * Construct the API stub
     *
     * @param Config                          $config  Config for the merchant
     * @param \CrefoPay\Library\Request\AddPayment $request Request for the payment that is to be sent
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\AddPayment $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Return the full url using base url in the config
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::ADD_PAYMENT_PATH);
    }
}
