<?php
/**
 * InvalidUrl class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Api\Exception;

/**
 * Class InvalidUrl
 * Raised if the URL for the API is invalid
 *
 * @package CrefoPay\Library\Api\Exception
 */
class InvalidUrl extends AbstractException
{
    /**
     * Constructor
     *
     * @param string $url
     */
    public function __construct($url)
    {
        parent::__construct("URL is invalid: " . $url);
    }
}
