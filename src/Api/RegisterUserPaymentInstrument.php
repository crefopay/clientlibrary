<?php
/**
 * RegisterUserPaymentInstrument class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class RegisterUserPaymentInstrument
 * APi stub for registerUserPaymentInstrument calls
 *
 * @link    https://docs.crefopay.de/api/#registeruserpaymentinstrument
 * @package CrefoPay\Library\Api
 */
class RegisterUserPaymentInstrument extends AbstractApi
{
    /**
     * URI for end point
     */
    const REGISTER_USER_PAYMENT_INSTRUMENT_PATH = 'registerUserPaymentInstrument';

    /**
     * Constructor
     *
     * @param Config                                             $config  Merchant Config
     * @param \CrefoPay\Library\Request\RegisterUserPaymentInstrument $request Request to be sent
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\RegisterUserPaymentInstrument $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Get the URL for the API call
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::REGISTER_USER_PAYMENT_INSTRUMENT_PATH);
    }
}
