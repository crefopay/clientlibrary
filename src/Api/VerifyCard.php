<?php
/**
 * VerifyCard class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Api;

use CrefoPay\Library\Config;

/**
 * Class VerifyCard
 *
 * Api stub for verifyCard
 *
 * @package    CrefoPay\Library\Api
 * @deprecated 1.1.6 verifyCard API was removed by CrefoPay
 */
class VerifyCard extends AbstractApi
{
    const VERIFY_CARD_PATH = 'verifyCard';

    /**
     * Constructor
     *
     * @param Config                          $config
     * @param \CrefoPay\Library\Request\VerifyCard $request
     */
    public function __construct(Config $config, \CrefoPay\Library\Request\VerifyCard $request)
    {
        $this->request = $request;
        parent::__construct($config);
    }

    /**
     * Returns API endpoint URL
     *
     * @return string
     */
    public function getUrl()
    {
        $baseUrl = $this->getBaseUrl();
        return $this->combineUrlUri($baseUrl, self::VERIFY_CARD_PATH);
    }
}
