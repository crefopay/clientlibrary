<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Response\Unserializer\Handler;

use CrefoPay\Library\Request\Objects\Amount as PlanAmount;
use CrefoPay\Library\Request\Objects\SubscriptionPlan;
use CrefoPay\Library\Response\Unserializer\Processor;

/**
 * Class ArraySubscriptionPlans
 *
 * Unserializer for subscriptionPlan data
 *
 * @link    https://docs.crefopay.de/api/#subscriptionplan
 * @package CrefoPay\Library\Response\Unserializer\Handler
 */
class ArraySubscriptionPlans implements UnserializerInterface
{
    /**
     * Return the string of the property that the unserializer will handle
     *
     * @return array
     */
    public function getAttributeNameHandler()
    {
        return array(
            'subscriptionPlans',
        );
    }

    /**
     * Function that will handle the deserialized data
     *
     * @param Processor $processor
     * @param           $value
     *
     * @return array
     */
    public function unserializeProperty(Processor $processor, $value)
    {
        $data = array();

        foreach ($value as $subscriptionPlanData) {
            $subscriptionPlan = new SubscriptionPlan();
            $subscriptionPlan->setUnserializedData($subscriptionPlanData);

            $amount = new PlanAmount();
            $amount->setUnserializedData($subscriptionPlanData['amount']);
            $subscriptionPlan->setAmount($amount);

            $data[] = $subscriptionPlan;
        }

        return $data;
    }
}
