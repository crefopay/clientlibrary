<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Response\Unserializer\Handler;

use CrefoPay\Library\Request\Objects\Person as PersonClass;
use CrefoPay\Library\Response\Unserializer\Processor;

/**
 * Class Person
 *
 * Unserializer for user data
 *
 * @link    https://docs.crefopay.de/api/#person
 * @package CrefoPay\Library\Response\Unserializer\Handler
 */
class Person implements UnserializerInterface
{
    /**
     * Return the string of the property that the unserializer will handle
     *
     * @return string
     */
    public function getAttributeNameHandler()
    {
        return 'userData';
    }

    /**
     * Function that will handle the deserialized data
     *
     * @param Processor $processor
     * @param           $value
     *
     * @return PersonClass
     */
    public function unserializeProperty(Processor $processor, $value)
    {

        $person = new PersonClass();
        $person->setUnserializedData($value);

        if (isset($value['dateOfBirth']) && !empty($value['dateOfBirth'])) {
            $dob = new \DateTime();
            list($year, $month, $day) = explode('-', $value['dateOfBirth']);
            $dob->setDate($year, $month, $day);
            $person->setDateOfBirth($dob);
        }

        return $person;
    }
}
