<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Response\Unserializer\Handler;

use CrefoPay\Library\Request\Objects\Amount as AmountClass;
use CrefoPay\Library\Response\Unserializer\Processor;

/**
 * Class Amount
 *
 * Unserializer for amount data
 *
 * @link    https://docs.crefopay.de/api/#amount
 * @package CrefoPay\Library\Response\Unserializer\Handler
 */
class Amount implements UnserializerInterface
{
    /**
     * Return the string of the property that the unserializer will handle
     *
     * @return array
     */
    public function getAttributeNameHandler()
    {
        return array(
            'amount',
        );
    }

    /**
     * Function that will handle the deserialized data
     *
     * @param Processor $processor
     * @param           $value
     *
     * @return AmountClass
     */
    public function unserializeProperty(Processor $processor, $value)
    {

        $amount = new AmountClass();
        $amount->setUnserializedData($value);

        return $amount;
    }
}
