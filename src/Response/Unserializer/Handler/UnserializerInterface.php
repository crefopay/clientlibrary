<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Response\Unserializer\Handler;

use CrefoPay\Library\Response\Unserializer\Processor;

/**
 * Interface UnserializerInterface
 *
 * Unserializer interface
 *
 * @package CrefoPay\Library\Response\Unserializer\Handler
 */
interface UnserializerInterface
{
    /**
     * Return the string of the property that the unserializer will handle
     *
     * Please note the method can return an array of strings
     *
     * @return string | array
     */
    public function getAttributeNameHandler();

    /**
     * Function that will handle the deserialized data
     *
     * @param Processor $processor
     * @param           $value
     *
     * @return \CrefoPay\Library\Request\RequestInterface
     */
    public function unserializeProperty(Processor $processor, $value);
}
