<?php
/**
 * Class file
 *
 * @author Vincent Mrose
 */

namespace CrefoPay\Library\Response\Unserializer\Handler;

use CrefoPay\Library\Response\Unserializer\Processor;

/**
 * Class RiskData
 *
 * Unserializer for risk data
 *
 * @link    https://docs.crefopay.de/api/#riskdata
 * @package CrefoPay\Library\Response\Unserializer\Handler
 */
class RiskData implements UnserializerInterface
{
    /**
     * Return the string of the property that the deserializer will handle
     *
     * @return array
     */
    public function getAttributeNameHandler()
    {
        return array(
            'riskData',
        );
    }

    /**
     * Function that will handle the deserialization of the value
     *
     * @param Processor $processor
     * @param           $value
     *
     * @return array
     */
    public function unserializeProperty(Processor $processor, $value)
    {
        return $value;
    }
}
