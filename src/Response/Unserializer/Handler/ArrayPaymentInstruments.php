<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Response\Unserializer\Handler;

use CrefoPay\Library\Request\Objects\PaymentInstrument;
use CrefoPay\Library\Response\Unserializer\Processor;

/**
 * Class ArrayPaymentInstruments
 *
 * @link
 * @package CrefoPay\Library\Response\Unserializer\Handler
 */
class ArrayPaymentInstruments implements UnserializerInterface
{
    /**
     * Return the string of the property that the deserializer will handle
     *
     * @return array
     */
    public function getAttributeNameHandler()
    {
        return array(
            'allowedPaymentInstruments',
            'paymentInstruments',
        );
    }

    /**
     * Function that will handle the deserialized data
     *
     * @param Processor $processor
     * @param           $value
     *
     * @return array
     */
    public function unserializeProperty(Processor $processor, $value)
    {
        $data = array();

        foreach ($value as $paymentInstrumentData) {
            $paymentInstrument = new PaymentInstrument();
            $paymentInstrument->setUnserializedData($paymentInstrumentData);

            if ($paymentInstrument->getPaymentInstrumentType() == PaymentInstrument::PAYMENT_INSTRUMENT_TYPE_CARD) {
                list($year, $month) = explode('-', $paymentInstrumentData['validity']);
                $dateTime = new \DateTime();
                $dateTime->setDate($year, $month, 1);
                $paymentInstrument->setValidity($dateTime);
            }

            $data[] = $paymentInstrument;
        }

        return $data;
    }
}
