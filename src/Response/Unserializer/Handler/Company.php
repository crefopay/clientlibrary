<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Response\Unserializer\Handler;

use CrefoPay\Library\Request\Objects\Company as CompanyClass;
use CrefoPay\Library\Response\Unserializer\Processor;

/**
 * Class Company
 *
 * Unserializer for company data
 *
 * @link    https://docs.crefopay.de/api/#company
 * @package CrefoPay\Library\Response\Unserializer\Handler
 */
class Company implements UnserializerInterface
{
    /**
     * Return the string of the property that the unserializer will handle
     *
     * @return array
     */
    public function getAttributeNameHandler()
    {
        return array(
            'companyData',
        );
    }

    /**
     * Function that will handle the deserialized data
     *
     * @param Processor $processor
     * @param mixed     $value
     *
     * @return CompanyClass
     */
    public function unserializeProperty(Processor $processor, $value)
    {

        $company = new CompanyClass();
        $company->setUnserializedData($value);

        return $company;
    }
}
