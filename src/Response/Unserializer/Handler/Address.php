<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Response\Unserializer\Handler;

use CrefoPay\Library\Request\Objects\Address as AddressClass;
use CrefoPay\Library\Response\Unserializer\Processor;

/**
 * Class Address
 *
 * Unserializer for address data
 *
 * @link    https://docs.crefopay.de/api/#address
 * @package CrefoPay\Library\Response\Unserializer\Handler
 */
class Address implements UnserializerInterface
{
    /**
     * Return the string of the property that the deserializer will handle
     *
     * @return array
     */
    public function getAttributeNameHandler()
    {
        return array(
            'billingAddress',
            'shippingAddress'
        );
    }

    /**
     * Function that will handle the deserialized data
     *
     * @param Processor $processor
     * @param           $value
     *
     * @return AddressClass
     */
    public function unserializeProperty(Processor $processor, $value)
    {

        $address = new AddressClass();
        $address->setUnserializedData($value);

        return $address;
    }
}
