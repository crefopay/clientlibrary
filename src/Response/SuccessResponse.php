<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Response;

/**
 * Class SuccessResponse
 *
 * Used for success responses
 *
 * @see     AbstractResponse
 * @package CrefoPay\Library\Response
 */
class SuccessResponse extends AbstractResponse
{
}
