<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Mac\Exception;

/**
 * Class AbstractException
 *
 * For Mac validation exception
 *
 * @package CrefoPay\Library\Mac\Exception
 */
abstract class AbstractException extends \CrefoPay\Library\AbstractException
{
}
