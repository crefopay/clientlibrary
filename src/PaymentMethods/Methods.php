<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\PaymentMethods;

use CrefoPay\Library\Validation\Helper\Constants;

/**
 * Class PaymentMethods
 *
 * Contains the payment methods and validator
 *
 * @package CrefoPay\Library\PaymentMethods
 */
class Methods
{
    /**
     * Payment method: Direct Debit
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_DD = "DD";

    /**
     * Payment methods: Credit Card
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_CC = "CC";

    /**
     * Payment method: Credit Card 3D Secure
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_CC3D = "CC3D";

    /**
     * Payment method: Cash in advance
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_PREPAID = "PREPAID";

    /**
     * Payment method: PayPal
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_PAYPAL = "PAYPAL";

    /**
     * Payment method: Sofort
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_SU = "SU";

    /**
     * Payment method: Bill Payment
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_BILL = "BILL";

    /**
     * Payment method: Bill Payment Secure
     *
     * @deprecated 1.1.4
     * @link       https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_BILL_SECURE = "BILL_SECURE";

    /**
     * Payment method: Cash on delivery
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_COD = "COD";

    /**
     * Payment method: Ideal
     *
     * @deprecated 1.1.4
     * @link       https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_IDEAL = "IDEAL";

    /**
     * Payment method: Installment
     *
     * @deprecated 1.1.4
     * @link       https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_INSTALLMENT = "INSTALLMENT";

    /**
     * Payment method: Amazon Pay
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_AMAZON_PAY = "AMAZON_PAY";

    /**
     * Payment method: Dummy
     *
     * @link https://docs.crefopay.de/api/#paymentmethods
     */
    const PAYMENT_METHOD_TYPE_DUMMY = "DUMMY";

    /**
     * Tag for the constraint validator
     */
    const VALIDATION_TAG_PAYMENT_METHOD = "PAYMENT_METHOD_TYPE";

    /**
     * Validation function
     *
     * @param string $value
     *
     * @return bool
     */
    public static function validate($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::VALIDATION_TAG_PAYMENT_METHOD);
    }
}
