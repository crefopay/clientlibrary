<?php

/**
 * Handler class file
 *
 * @author Daniel Kazior
 */

namespace CrefoPay\Library\Pns;

use CrefoPay\Library\Callback\MacCalculator;
use CrefoPay\Library\Config;
use CrefoPay\Library\Mac\Exception\MacInvalid;
use CrefoPay\Library\Pns\Exception\ParamNotProvided;

/**
 * Class Handler
 *
 * Handler for PNS class
 * Even if only one notification can not be delivered successfully, no other notifications are sent until the
 * problem is fixed.
 * If the merchant server answers with HTTP code 500 because the message could not be processed internally,
 * CrefoPay will block the queue. The merchant should always answer with HTTP code 200 as soon as the message
 * was received successfully.
 * The processing of the message should be implemented asynchronously.
 * IE the processor should save the validated data to an database for processing
 * Although this class will return an exception you should log and flag up a critical error and return an 200
 *
 * @link    https://docs.crefopay.de/api/#notification-call
 * @package CrefoPay\Library\Pns
 */
class Handler
{
    /**
     * Config class for the library
     *
     * @var Config
     */
    private $config;

    /**
     * Data from the PNS call
     *
     * @var array
     */
    private $data;

    /**
     * Class provided by the integrator to be ran once a PNS notification is validated
     *
     * @var ProcessorInterface
     */
    private $processor;

    /**
     * Stores required parameter names
     *
     * @var array
     */
    private $requiredFields = array(
        'merchantID',
        'storeID',
        'amount',
        'currency',
        'notificationType',
        'gvc',
        'accountNo',
        'sortCode',
        'owner',
        'merchantIBAN',
        'statementNumber',
        'debitCreditMarker',
        'valueDate',
        'bookingDate',
        'timestamp',
        'version',
        'mac'
    );

    /**
     * Stores required optional names
     *
     * @var array
     */
    private $optionalFields = array(
        'orderID',
        'subscriptionId',
        'merchantReference',
        'paymentReference',
        'gvcText',
        'usage'
    );

    /**
     * Logger instance
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger = null;

    /**
     * Instantiate the handler
     *
     * @link https://docs.crefopay.de/api/#notification-call
     *
     * @param Config             $config    Config class for the library
     * @param array              $data      Data in the PNS call. Please see API documentation for possible values
     * @param ProcessorInterface $processor Class provided by the Integration to be ran once a PNS call is validated
     *
     * @throws ParamNotProvided
     * @throws \CrefoPay\Library\Callback\Exception\MacValidation
     * @throws MacInvalid
     */
    public function __construct(Config $config, array $data, ProcessorInterface $processor)
    {
        $this->config = $config;
        $this->processor = $processor;
        $pnsLogPath = $config->getLogLocationPNS();
        if (!empty($pnsLogPath)) {
            $this->logger = \CrefoPay\Library\Logging\Factory::getLogger($config, $config->getLogLocationPNS());
        }

        $missingParams = array();

        foreach ($this->requiredFields as $param) {
            if (array_key_exists($param, $data)) {
                $this->data[$param] = $data[$param];
            } else {
                $missingParams[] = $param;
            }
        }

        foreach ($this->optionalFields as $param) {
            if (array_key_exists($param, $data)) {
                $this->data[$param] = $data[$param];
            } else {
                $this->data[$param] = '';
            }
        }
        if ($this->logger) {
            $this->logger->debug("PNS request data", array("data" => $this->data));
        }
        if (!empty($missingParams)) {
            throw new ParamNotProvided(implode(', ', $missingParams));
        }

        $macCalculator = new MacCalculator($this->config, $this->data);
        $macCalculator->validateResponse();

        $this->processor->sendData(
            $this->data['merchantID'],
            $this->data['storeID'],
            $this->data['orderID'],
            $this->data['amount'],
            $this->data['currency'],
            $this->data['notificationType'],
            $this->data['gvc'],
            $this->data['accountNo'],
            $this->data['sortCode'],
            $this->data['owner'],
            $this->data['merchantIBAN'],
            $this->data['statementNumber'],
            $this->data['debitCreditMarker'],
            $this->data['valueDate'],
            $this->data['bookingDate'],
            $this->data['timestamp'],
            $this->data['version'],
            $this->data['mac'],
            $this->data['subscriptionId'],
            $this->data['merchantReference'],
            $this->data['paymentReference'],
            $this->data['gvcText'],
            $this->data['usage']
        );
    }

    /**
     * Run the processor callback
     *
     * The processor should ensure that a 200 status is returned to CrefoPay.
     * If there is an error please handle logging and recover.
     * Also please do processing of PNS calls with a cronjob
     *
     * @return void
     */
    public function run()
    {
        $this->processor->run();
    }
}
