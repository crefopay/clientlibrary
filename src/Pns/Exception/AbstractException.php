<?php
/**
 * Class file
 *
 * @author Daniel Kazior
 */

namespace CrefoPay\Library\Pns\Exception;

/**
 * Class AbstractException
 *
 * For PNS exceptions
 *
 * @package CrefoPay\Library\Pns\Exception
 */
abstract class AbstractException extends \CrefoPay\Library\AbstractException
{
    /**
     * Constructor
     *
     * @param string $message
     */
    public function __construct($message = 'PNS Exception')
    {
        parent::__construct($message);
    }
}
