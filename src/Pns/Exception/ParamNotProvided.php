<?php
/**
 * Class file
 *
 * @author Daniel Kazior
 */

namespace CrefoPay\Library\Pns\Exception;

/**
 * Class ParamNotProvided
 *
 * Raised if not all required MNS parameters are not provided
 *
 * @package CrefoPay\Library\Pns\Exception
 */
class ParamNotProvided extends AbstractException
{
    /**
     * Constructor
     *
     * @param string $params
     */
    public function __construct($params)
    {
        parent::__construct("The following parameters were not provided or are empty: " . $params);
    }
}
