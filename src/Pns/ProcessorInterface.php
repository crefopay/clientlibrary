<?php

/**
 * Class file
 *
 * @author Daniel Kazior
 */

namespace CrefoPay\Library\Pns;

/**
 * Interface ProcessorInterface
 *
 * For the integrator to use to process incoming PNS notices.
 * Please note for this script please do not do any processing beyond saving the PNS to the database.
 * We recommend that the order actions should be processed using a cron job and this interface should be used for the
 * implementation of the database saving code
 *
 * @package CrefoPay\Library\Pns
 */
interface ProcessorInterface
{
    /**
     * Send data to the processor that will be used in the run method
     *
     * @param string  $merchantID           This is the merchant ID assigned by CrefoPay.
     * @param string  $storeID              This is the store ID of a merchant assigned by CrefoPay as a merchant 
     *                                      can have more than one store.
     * @param string  $orderID              This is a unique identifier for a transaction which is created by the shop. 
     *                                      This value is only set if the payment was associated to a CrefoPay transaction.
     * @param string  $subscriptionId       This is a unique identifier for a subscription which is created by the shop. 
     *                                      This value is only set, if the transaction is related to a subscription.
     * @param string  $captureID            The confirmation ID of the capture. This parameter is only sent for notifications 
     *                                      that belong to captures.
     * @param string  $merchantReference    Optional/additional reference for the transaction. This parameter is returned
     *                                      with every call from CrefoPay to the shop.
     * @param string  $paymentReference     The reference text to which the user needs to refer within his remittance so that CrefoPay
     *                                      can link the incoming payments with the outstanding amounts of a transaction.
     * @param string  $notificationType     Describes in which context this notification was triggered. For a description of each option, 
     *                                      see the table below. Possible values: FAILED, IGNORED, MATCHED, REFUNDED, SENTTOACCREDIS,
     *                                      MATCHEDBYACCREDIS, ACCREDISFEE
     * @param integer $gvc                  Transaction code from the bank statement (Geschäftsvorfall-Code) which describes the type of payment.
     * @param string  $gvcText              Transaction description from the bank statement which describes the type of payment.
     * @param string  $accountNo            IBAN from/to which the payment is sent.
     * @param string  $sortCode             Bank identifier/SWIFT code
     * @param string  $owner                Name of the account holder.
     * @param string  $usage                Customer reference as provided by the customer in case of incoming payments. Set by CrefoPay in case of refunds.
     * @param string  $merchantIBAN         The bank account for which the payment notification was sent.
     * @param string  $statementNumber      Sequential number of the associated bank statement.
     * @param string  $debitCreditMarker    Indicates whether it is a debit or credit payment. Possible values: C (credit) and D (debit)
     * @param string  $valueDate            Value date of the payment in milliseconds since 1970-01-01 (Unix timestamp in milliseconds)
     * @param integer $bookingDate          Booking date in milliseconds since 1970-01-01 (Unix timestamp in milliseconds)
     * @param string  $currency             3 characters, Currency code according to ISO4217. Currency of the payment.
     * @param integer $amount               Balance of the payment.
     * @param integer $timestamp            The time when the payment notification was triggered in milliseconds since 1970-01-01 (Unix timestamp in milliseconds)
     * @param string  $version              Notification version
     * @param string  $mac                	Message Authentication Code see MAC calculation
     * 
     * @link https://docs.crefopay.de/api/#notification-call
     */
    public function sendData(
        $merchantID,
        $storeID,
        $orderID,
        $amount,
        $currency,
        $notificationType,
        $gvc,
        $accountNo,
        $sortCode,
        $owner,
        $merchantIBAN,
        $statementNumber,
        $debitCreditMarker,
        $valueDate,
        $bookingDate,
        $timestamp,
        $version,
        $mac,
        $subscriptionId,
        $merchantReference,
        $paymentReference,
        $gvcText,
        $usage
    );

    /**
     * The run method used by the processor to run successfully validated PNS notifications.
     *
     * This should not return anything
     */
    public function run();
}
