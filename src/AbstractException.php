<?php
/**
 * AbstractException global class
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library;

/**
 * Class AbstractException
 *
 * Exception class for all exceptions thrown by the library
 *
 * @package CrefoPay\Library
 */
abstract class AbstractException extends \Exception
{
}
