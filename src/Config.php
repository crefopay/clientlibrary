<?php
/**
 * Config class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library;

/**
 * Class config
 *
 * Stores configuration for the API and is used in most all requests
 *
 * @package CrefoPay\Library
 */
class Config
{
    /**
     * Debug log level
     *
     * @link https://github.com/Seldaek/monolog/blob/master/src/Monolog/Logger.php
     */
    const LOG_LEVEL_DEBUG = 100;

    /**
     * Info log level
     *
     * @link https://github.com/Seldaek/monolog/blob/master/src/Monolog/Logger.php
     */
    const LOG_LEVEL_INFO = 200;

    /**
     * Warning log level
     *
     * @link https://github.com/Seldaek/monolog/blob/master/src/Monolog/Logger.php
     */
    const LOG_LEVEL_WARNING = 300;

    /**
     * Error log level
     *
     * @link https://github.com/Seldaek/monolog/blob/master/src/Monolog/Logger.php
     */
    const LOG_LEVEL_ERROR = 400;

    /**
     * This is the merchant password for the Mac Calculation
     *
     * @var string
     */
    private $merchantPassword;

    /**
     * This is the merchantID assigned by CrefoPay.
     *
     * @var string
     */
    private $merchantID;

    /**
     * This is the store ID of a merchant assigned by CrefoPay as a merchant can have more than one store.
     *
     * @var string
     */
    private $storeID;

    /**
     * Is logging enabled
     *
     * @var bool
     */
    private $logEnabled = false;

    /**
     * The log level
     *
     * @see https://github.com/Seldaek/monolog/blob/master/src/Monolog/Logger.php
     *
     * @var int
     */
    private $logLevel = self::LOG_LEVEL_ERROR;

    /**
     * Main Log Location
     *
     * @var
     */
    private $logLocationMain;

    /**
     * File path tp location for request logging
     *
     * @var string
     */
    private $logLocationRequest;

    /**
     * File path tp location for MNS logging
     *
     * @var string
     */
    private $logLocationMNS = '';

    /**
     * File path tp location for PNS logging
     *
     * @var string
     */
    private $logLocationPNS = '';

    /**
     * File path to location for Callback logging
     *
     * @var string
     */
    private $logLocationCallbacks = '';

    /**
     * Default risk class for all requests
     *
     * @var int
     */
    private $defaultRiskClass = Risk\RiskClass::RISK_CLASS_DEFAULT;

    /**
     * Default locale for transactions
     *
     * @see  Locale\Codes
     * @link https://docs.crefopay.de/api/#languages
     *
     * @var string
     */
    private $defaultLocale;

    /**
     * Automatically add salt to requests
     *
     * @var bool
     */
    private $sendRequestsWithSalt = true;

    /**
     * Base url for the API such as:
     * https://sandbox.crefopay.de/2.0/
     * https://api.crefopay.de/2.0/
     *
     * @var string
     */
    private $baseUrl;

    /**
     * Options to set after curl_init()
     * @var array
     */
    private $curlOptions=[];

    /**
     * Constructor can pass in a assertive array with config
     *
     * @see Config::setData()
     *
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        if (!empty($config)) {
            $this->setData($config);
        }

        return $this;
    }

    /**
     * Mass set configuration option using an assertive array
     *
     * Possible keys:
     * merchantPassword      string   This is the private key for mac calculation
     * merchantID            string   This is the merchantID assigned by CrefoPay.
     * storeID               string   This is the store ID of a merchant.
     * logEnabled            bool     Should logging be enabled
     * logLevel              int      Log level
     * logLocationMain       string   Main log Location
     * logLocationRequest    string   Log location for API requests
     * logLocationMNS        string   Log for MNS asynchronous callbacks
     * logLocationCallbacks  string   Log location for synchronous callbacks
     * defaultRiskClass      string   Default risk class
     * defaultLocale         string   Default locale
     * sendRequestsWithSalt  bool     Automatically add salt to requests
     * baseUrl               string   Base URL of requests
     * curlOptions           array    Options to pass to curl_setopt() after curl_init()
     *
     * @param array $config
     *
     * @return $this
     */
    public function setData(array $config)
    {
        foreach ($config as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }

        return $this;
    }

    /**
     * Returns the set configuration data in an array
     *
     * @return array
     */
    public function getConfigData()
    {
        try {
            $reflector = new \ReflectionClass($this);
        } catch (\ReflectionException $re) {
            return array();
        }
        $properties = $reflector->getProperties();

        $configData = array();

        foreach ($properties as $property) {
            $property->setAccessible(true);
            $configData[$property->getName()] = $property->getValue($this);
            $property->setAccessible(false);
        }

        return $configData;
    }

    /**
     * Get the merchant password
     *
     * @return string
     */
    public function getMerchantPassword()
    {
        return $this->merchantPassword;
    }

    /**
     * Get the merchant Id
     *
     * @return string
     */
    public function getMerchantID()
    {
        return $this->merchantID;
    }

    /**
     * Return the store Id
     *
     * @return string
     */
    public function getStoreID()
    {
        return $this->storeID;
    }

    /**
     * Check if log is enabled
     *
     * @return bool
     */
    public function getLogEnabled()
    {
        return $this->logEnabled;
    }

    /**
     * Get the log level
     *
     * @return int
     */
    public function getLogLevel()
    {
        return $this->logLevel;
    }

    /**
     * Get the main log location
     *
     * @return string
     */
    public function getLogLocationMain()
    {
        return $this->logLocationMain;
    }

    /**
     * Get the request log Location
     *
     * @return string
     */
    public function getLogLocationRequest()
    {
        return $this->logLocationRequest;
    }

    /**
     * Get the MNS logger location path
     *
     * @return string
     */
    public function getLogLocationMNS()
    {
        return $this->logLocationMNS;
    }

    /**
     * Get the MNS logger location path
     *
     * @return string
     */
    public function getLogLocationPNS()
    {
        return $this->logLocationMNS;
    }

    /**
     * Return the log location
     *
     * @return string
     */
    public function getLogLocationCallbacks()
    {
        return $this->logLocationCallbacks;
    }

    /**
     * Get default risk class
     *
     * @return int
     */
    public function getDefaultRiskClass()
    {
        return $this->defaultRiskClass;
    }

    /**
     * Returns the default locale
     *
     * @return string
     */
    public function getDefaultLocale()
    {
        return $this->defaultLocale;
    }

    /**
     * Should requests be salted
     *
     * @see Config::sendRequestsWithSalt
     *
     * @return bool
     */
    public function isSendRequestsWithSalt()
    {
        return ($this->sendRequestsWithSalt ? true : false);
    }

    /**
     * Returns the API base URL
     *
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    public function getCurlOptions()
    {
        $tmp=$this->curlOptions;
        if (is_array($tmp))
        {
            return $tmp;
        }

        return [];
    }
}
