<?php
/**
 * Type class file
 *
 * @author Vincent Mrose
 */

namespace CrefoPay\Library\Integration;

use CrefoPay\Library\Validation\Helper\Constants;

class Type
{
    /**
     * Integration type: API
     *
     * CrefoPay is integrated as pure API solution. No hosted pages are used. To transfer credit card data the merchant
     * has to be PCI compliant or he has to use the PayCoBridge to transfer the data.
     *
     * @link https://docs.crefopay.de/api/#integration-options
     */
    const INTEGRATION_TYPE_API = "API";

    /**
     * Integration type: Secure Fields
     *
     * The method selection page is integrated before the confirmation page of the merchant is shown.
     * The hosted fields for credit card data are injected as iFrames into the page by the SecureFields JS library
     *
     * @link https://docs.crefopay.de/api/#integration-options
     * @see  https://docs.crefopay.de/api/#securefields
     */
    const INTEGRATION_TYPE_SECURE_FIELDS = "SecureFields";

    /**
     * Integration type: Hosted Pages Before
     *
     * The hosted payment method selection page is integrated before the confirmation page of the merchant is shown.
     * The hosted page allows the user the select a payment method and enter payment instruments like credit card and
     * bank account.
     *
     * @link https://docs.crefopay.de/api/#integration-options
     */
    const INTEGRATION_TYPE_HOSTED_BEFORE = "HostedPageBefore";

    /**
     * Integration type: Hosted Pages After
     *
     * The hosted payment method selection page is integrated after the confirmation page of the merchant is shown.
     * The hosted page allows the user the select a payment method and enter payment instruments like credit card and
     * bank account.
     *
     * @link https://docs.crefopay.de/api/#integration-options
     */
    const INTEGRATION_TYPE_HOSTED_AFTER = "HostedPageAfter";

    /**
     * Tag for the validator method for integration type
     */
    const VALIDATION_TAG_INTEGRATION_TYPE = "INTEGRATION_TYPE";

    /**
     * Validate if value is a valid integration type
     *
     * @param $value
     *
     * @return bool
     */
    public static function validate($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::VALIDATION_TAG_INTEGRATION_TYPE);
    }
}