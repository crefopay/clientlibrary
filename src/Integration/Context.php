<?php
/**
 * Type class file
 *
 * @author Daniel Kazior
 */

namespace CrefoPay\Library\Integration;

use CrefoPay\Library\Validation\Helper\Constants;

/**
 * Determines if the transaction was initiated during online-checkout 
 * process or offline where no direct user interaction happens. 
 * Will be set to ONLINE by default if not provided. Possible Values: 
 * ONLINE | MAIL_ORDER | TELEPHONE_ORDER | PAY_BY_LINK
 */
class Type
{
    /**
     * Integration Context ONLINE
     */
    const INTEGRATION_CONTEXT_ONLINE = "ONLINE";

    /**
     * Integration Context MAIL_ORDER
     */
    const INTEGRATION_CONTEXT_MAIL_ORDER = "MAIL_ORDER";

    /**
     * Integration Context TELEPHONE_ORDER
     */
    const INTEGRATION_CONTEXT_TELEPHONE_ORDER = "TELEPHONE_ORDER";

    /**
     * Integration Context PAY_BY_LINK
     */
    const INTEGRATION_CONTEXT_PAY_BY_LINK = "PAY_BY_LINK";

    /**
     * Tag for the validator method for integration type
     */
    const VALIDATION_TAG_INTEGRATION_CONTEXT = "INTEGRATION_CONTEXT";

    /**
     * Validate if value is a valid integration type
     *
     * @param $value
     *
     * @return bool
     */
    public static function validate($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::VALIDATION_TAG_INTEGRATION_CONTEXT);
    }
}
