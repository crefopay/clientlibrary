<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Risk;

use CrefoPay\Library\Validation\Helper\Constants;

/**
 * Class RiskClass
 *
 * Risk class values that are used in the API
 *
 * @link    https://docs.crefopay.de/api/#risk-management
 * @package CrefoPay\Library\Risk
 */
class RiskClass
{
    /**
     * Trusted Risk Class
     */
    const RISK_CLASS_TRUSTED = 0;

    /**
     * Default Risk Class
     */
    const RISK_CLASS_DEFAULT = 1;

    /**
     * High Risk Class
     */
    const RISK_CLASS_HIGH = 2;

    /**
     * Return a ordered array of the risk classes
     *
     * @return array
     */
    public static function getRiskClasses()
    {
        return array(
            "Trusted" => 0,
            "Default" => 1,
            "High" => 2
        );
    }

    /**
     * Validation function
     *
     * @param string $value
     *
     * @return bool
     */
    public static function validateRiskClass($value)
    {
        if (Constants::validateConstant(__CLASS__, $value, 'RISK_CLASS')) {
            return true;
        }

        if (is_float($value)) {
            if ($value < 0.0) return false;
            if ($value > 2.0) return false;
            return true;
        }

        return false;
    }
}
