<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Serializer\Exception;

use CrefoPay\Library\Request\RequestInterface as RequestInterface;

/**
 * Class VisitorCouldNotBeFound
 *
 * Raised if serializer could not be found
 *
 * @package CrefoPay\Library\Serializer\Exception
 */
class VisitorCouldNotBeFound extends AbstractException
{
    /**
     * Constructor
     *
     * @param string           $lookupCode
     * @param RequestInterface $object
     */
    public function __construct($lookupCode, RequestInterface $object)
    {
        parent::__construct("Serializer could not be found: " . $lookupCode . " for object of class '" . get_class($object) . "'");
    }
}
