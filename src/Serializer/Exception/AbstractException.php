<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Serializer\Exception;

/**
 * Class AbstractException
 *
 * Abstract exception for serializer errors
 *
 * @package CrefoPay\Library\Serializer\Exception
 */
abstract class AbstractException extends \CrefoPay\Library\AbstractException
{
    /**
     * Constructor
     *
     * @param string $message
     */
    public function __construct($message = 'Serializer Exception')
    {
        parent::__construct($message);
    }
}
