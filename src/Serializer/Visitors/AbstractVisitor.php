<?php
/**
 * Visitors for serialization
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Serializer\Visitors;

use CrefoPay\Library\Request\RequestInterface as RequestInterface;
use CrefoPay\Library\Serializer\Exception\VisitorCouldNotBeFound;
use CrefoPay\Library\Serializer\Serializer as Serializer;
use CrefoPay\Library\Serializer\Visitors\VisitorInterface as VisitorInterface;

/**
 * Class AbstractVisitor
 *
 * Abstract class for visitors
 *
 * @package CrefoPay\Library\Serializer\Visitors
 */
abstract class AbstractVisitor implements VisitorInterface
{

    /**
     * Checks whether the value needs walker for the sub properties
     *
     * @param Object $value
     *
     * @return bool
     */
    protected function checkIfWalkerIsNeeded($value)
    {
        if ($value instanceof RequestInterface) {
            return true;
        }

        return false;
    }

    /**
     * Checks the provided array and serializes deeper values
     *
     * @param array                              $data
     * @param \CrefoPay\Library\Serializer\Serializer $serializer
     *
     * @return array
     * @throws VisitorCouldNotBeFound
     */
    protected function checkSerializeArray($data, Serializer $serializer)
    {
        foreach ($data as $key => $value) {
            if ($this->checkIfWalkerIsNeeded($value)) {
                $data[$key] = $serializer->serialize($value);
            }
        }

        return $data;
    }
}
