<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Serializer\Visitors;

use CrefoPay\Library\Request\RequestInterface as RequestInterface;
use CrefoPay\Library\Serializer\Serializer as Serializer;

/**
 * Interface VisitorInterface
 *
 * Interface for visitors to use
 *
 * @package CrefoPay\Library\Serializer\Visitors
 */
interface VisitorInterface
{
    /**
     * The method by which the object is visited and is serialized
     *
     * @param RequestInterface $object
     * @param Serializer       $serializer
     *
     * @return string|array Returns a formatted string such as json, post data from the object
     */
    public function visit(RequestInterface $object, Serializer $serializer);

    /**
     * Returns the datatype the visitor outputs such as xml,json or post form
     *
     * @return string
     */
    public function getType();
}
