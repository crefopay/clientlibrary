<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Serializer\Visitors\Exception;

/**
 * Class AbstractException
 *
 * Abstract exception for serializer errors
 *
 * @package CrefoPay\Library\Serializer\Visitors\Exception
 */
abstract class AbstractException extends \CrefoPay\Library\AbstractException
{
}
