<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Locale;

use CrefoPay\Library\Validation\Helper\Constants;

/**
 * Class Codes
 *
 * @link
 * @package CrefoPay\Library\Locale
 */
class Codes
{
    /**
     * Locale : German - Deutsch
     *
     * @link https://docs.crefopay.de/api/#languages
     */
    const LOCALE_DE = "DE";

    /**
     * Locale : English
     *
     * @link https://docs.crefopay.de/api/#languages
     */
    const LOCALE_EN = "EN";

    /**
     * Locale : Spanish - Español
     *
     * @link https://docs.crefopay.de/api/#languages
     */
    const LOCALE_ES = "ES";

    /**
     * Locale : Finnish - Suomi
     *
     * @deprecated 1.1.4
     * @link       https://docs.crefopay.de/api/#languages
     */
    const LOCALE_FI = "FI";

    /**
     * Locale : French - Français
     *
     * @link https://docs.crefopay.de/api/#languages
     */
    const LOCALE_FR = "FR";

    /**
     * Locale : Italian - Italiano
     *
     * @link https://docs.crefopay.de/api/#languages
     */
    const LOCALE_IT = "IT";

    /**
     * Locale : Dutch - Nederlands
     *
     * @link https://docs.crefopay.de/api/#languages
     */
    const LOCALE_NL = "NL";

    /**
     * Locale : Turkish - Türkçe
     *
     * @deprecated 1.1.4
     * @link       https://docs.crefopay.de/api/#languages
     */
    const LOCALE_TU = "TU";

    /**
     * Locale : Russian - Русский
     *
     * @deprecated 1.1.4
     * @link       https://docs.crefopay.de/api/#languages
     */
    const LOCALE_RU = "RU";

    /**
     * Locale : Portuguese - Português
     *
     * @deprecated 1.1.4
     * @link       https://docs.crefopay.de/api/#languages
     */
    const LOCALE_PT = "PT";

    /**
     * Tag for the constraint validator
     */
    const TAG_LOCALE = "LOCALE";

    /**
     * Validation function
     *
     * @param string $value
     *
     * @return bool
     */
    public static function validateLocale($value)
    {
        return Constants::validateConstant(__CLASS__, $value, static::TAG_LOCALE);
    }
}
