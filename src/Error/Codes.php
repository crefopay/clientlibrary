<?php
/**
 * Class file
 *
 * @author Michael Fisher
 */

namespace CrefoPay\Library\Error;

/**
 * Class Codes
 *
 * @link    https://docs.crefopay.de/api/#errors
 * @package CrefoPay\Library\Error
 */
class Codes
{
    /**
     * Technical or interface errors
     */
    const ERROR_UNKNOWN = 1000;
    const ERROR_MAC = 1001;
    const ERROR_INVALID_REQUEST = 1002;

    /**
     * Status codes for rejected requests
     */
    const ERROR_INVALID_TOKEN = 2024;

    const ERROR_PAYMENT_METHODS_NOT_CONFIGURED = 2027;
    const ERROR_PAYMENT_METHOD_UNAVAILABLE = 2000;
    const ERROR_PAYMENT_METHOD_REJECTED = 2001;

    const ERROR_TRANSACTION_METHOD_CALL_NOT_ALLOWED = 2002;
    const ERROR_TRANSACTION_EXPIRED = 2003;
    const ERROR_TRANSACTION_ORDER_NUMBER_NOT_FOUND = 2004;
    const ERROR_TRANSACTION_IN_PROGRESS = 2005;
    const ERROR_TRANSACTION_ALREADY_FINISHED = 2023;
    const ERROR_TRANSACTION_CAPTURE_NOT_ALLOWED_IN_STATE = 2030;

    const ERROR_SHOP_NOT_FOUND = 2006;
    const ERROR_SHOP_NOT_ACTIVE = 2007;

    const ERROR_ORDER_EXISTS = 2008;
    const ERROR_ORDER_ALREADY_CAPTURED = 2028;
    const ERROR_ORDER_ALREADY_CAPTURED_WITH_DIFFERENT_AMOUNT = 2029;

    const ERROR_CREDIT_CARD_EXPIRED = 2010;
    const ERROR_CREDIT_CARD_NUMBER_NOT_MATCH_ISSUER = 2011;
    const ERROR_CREDIT_CARD_ALREADY_STORED_FOR_USER = 2012;
    const ERROR_CREDIT_BANK_ACCOUNT_ALREADY_STORED_FOR_USER = 2013;

    const ERROR_USER_ALREADY_EXISTS = 2014;
    const ERROR_USER_NOT_FOUND = 2015;

    const ERROR_CAPTUREID_NOT_FOUND = 2019;
    const ERROR_REFUND_FAILED = 2018;
    const ERROR_REFUND_MUST_NOT_BE_GREATER_THAN_CAPTURE = 2016;
    const ERROR_PAYMENT_DECLINED_FRAUD = 2017;

    const ERROR_MERCHANT_CREATION_NOT_ALLOWED = 2020;
    const ERROR_MERCHANT_NOT_ALLOWED_TO_MAKE_CALL = 2025;
    const ERROR_MERCHANT_NOTIFICATION_FAILED = 2032;

    const ERROR_MSA_ALREADY_EXISTS = 2021;
    const ERROR_SHOP_ALREADY_EXISTS = 2022;

    const ERROR_PAYMENT_INSTRUMENT_UNKNOWN = 2026;
    const ERROR_PAYMENT_INSTRUMENT_INVALID = 2033;
    const ERROR_PAYMENT_INSTRUMENT_TYPE_INVALID = 2031;
    const ERROR_PAYMENT_INSTRUMENT_NO_SEPA_SUPPORT = 2034;
    const ERROR_ACCOUNT_ROUTING_MATCH = 2035;

    const ERROR_CVV_NOT_ALLOWED = 2036;
    const ERROR_CAPTURE_REJECTED = 2037;
    const ERROR_MULTIPLE_CAPTURES_NOT_ALLOWED = 2038;
    const ERROR_ISSUER_NOT_SUPPORTED = 2039;

    const ERROR_CREDITCARD_ISSUER_UNKNOWN = 2040;
    const ERROR_CREDITCARD_NUMBER_EMPTY = 2041;
    const ERROR_CREDITCARD_VALIDITY_MONTH_EMPTY = 2042;
    const ERROR_CREDITCARD_VALIDITY_YEAR_EMPTY = 2043;
    const ERROR_CREDITCARD_ACCOUNTHOLDER_EMPTY = 2044;

    const ERROR_ERROR_SENDING_MAIL = 2045;
    const ERROR_INVALID_CREDITCARD_NUMBER = 2046;
    const ERROR_DUNNING_NOT_ENABLED = 2047;
    const ERROR_SUBSCRIPTION_PLAN_NOT_FOUND = 2048;
    const ERROR_INTEGRATIONTYPE_NOT_ALLOWED = 2049;
    const ERROR_SUBSCRIPTION_ALREADY_EXISTS = 2050;
    const ERROR_SUBSCRIPTION_NOT_FOUND = 2051;
    const ERROR_ACTION_NOT_ALLOWED_FOR_STATUS = 2052;
    const ERROR_OPTIONAL_PARAMETERS_HAVE_NO_VALUES = 2053;
    const ERROR_NO_SOLVENCYCHECK_REPORT_CONFIG = 2054;

    /**
     * Additional status codes for notification callbacks
     */
    const ERROR_CALLBACK_PARAMETER_FORMAT = 6001;
    const ERROR_CALLBACK_UNKNOWN = 6002;
    const ERROR_CALLBACK_TRANSACTION_PAYMENT_METHOD_NOT_ALLOWED = 6003;

    const ERROR_CALLBACK_SYSTEM_TIMEOUT_NORESPONSE = 6004;
    const ERROR_CALLBACK_CANCELED_USER = 6005;
    const ERROR_CALLBACK_AUTHENTICATION_FAILED = 6006;
    const ERROR_CALLBACK_BLOCK_STOLEN_DECLINED_NOT_REASON = 6007;
    const ERROR_CALLBACK_NO_JS_USER_BROWSER = 6008;
    const ERROR_CALLBACK_OVERLOAD_PROCESSING_NOT_AVAILABLE = 6009;
    const ERROR_CALLBACK_INVALID_AMOUNT = 6010;
    const ERROR_CALLBACK_FRAUD = 6011;

    const ERROR_CALLBACK_COMMUNICATION = 6012;
    const ERROR_CALLBACK_RESERVATION_EXPIRED = 6013;

    const ERROR_CALLBACK_CONFIGURATION = 6014;

    const ERROR_CALLBACK_PARTIAL_CAPTURE_NOT_ALLOWED = 6015;

    const ERROR_CALLBACK_THIRD_PARTY = 6016;

    const ERROR_CALLBACK_NO_FUNDS = 6017;
    const ERROR_CALLBACK_PARTIAL_REFUND_FAILED = 6018;

    /**
     * Any code between 0 and 999 is not an error
     */
    const CODE_NON_ERROR_START = 0;
    /**
     * Any code between 0 and 999 is not an error
     */
    const CODE_NON_ERROR_END = 999;

    /**
     * Error codes array
     *
     * @var array
     */
    private static $errorCodes;

    /**
     * Returns the error description / message
     *
     * @param int $code
     *
     * @return string
     */
    public static function getErrorName($code)
    {
        if (!self::$errorCodes) {
            self::$errorCodes = array(
                self::ERROR_UNKNOWN => 'An unknown error occurred, please contact CrefoPay support to find out details.',
                self::ERROR_MAC => 'The calculated MAC is invalid.',
                self::ERROR_INVALID_REQUEST => 'The request is invalid. Please refer to the message for further explanation of the cause.',
                self::ERROR_PAYMENT_METHOD_UNAVAILABLE => 'The payment method is not available. Please use another payment method.',
                self::ERROR_PAYMENT_METHOD_REJECTED => 'The payment has been rejected. Please use another payment method.',
                self::ERROR_TRANSACTION_METHOD_CALL_NOT_ALLOWED => 'Method call is not allowed in this state of transaction.',
                self::ERROR_TRANSACTION_EXPIRED => 'The transaction is expired.',
                self::ERROR_TRANSACTION_ORDER_NUMBER_NOT_FOUND => 'The requested order number does not exists.',
                self::ERROR_TRANSACTION_IN_PROGRESS => 'Method call already in process for this transaction.',
                self::ERROR_SHOP_NOT_FOUND => 'No shop found with this shopID.',
                self::ERROR_SHOP_NOT_ACTIVE => 'Shop is not activated.',
                self::ERROR_ORDER_EXISTS => 'Order ID already exist',
                self::ERROR_CREDIT_CARD_EXPIRED => 'The credit card is expired.',
                self::ERROR_CREDIT_CARD_NUMBER_NOT_MATCH_ISSUER => 'The credit card number does not match the issuer.',
                self::ERROR_CREDIT_CARD_ALREADY_STORED_FOR_USER => 'The credit card is already stored for this user.',
                self::ERROR_CREDIT_BANK_ACCOUNT_ALREADY_STORED_FOR_USER => 'The bank account is already stored for this user.',
                self::ERROR_USER_ALREADY_EXISTS => 'The user already exists.',
                self::ERROR_USER_NOT_FOUND => 'The user does not exist.',
                self::ERROR_REFUND_MUST_NOT_BE_GREATER_THAN_CAPTURE => 'Refund amount must not be greater than capture amount.',
                self::ERROR_PAYMENT_DECLINED_FRAUD => 'Payment declined by fraud check. Please use another payment method.',
                self::ERROR_REFUND_FAILED => 'Refund failed.',
                self::ERROR_CAPTUREID_NOT_FOUND => 'The provided captureID is unknown.',
                self::ERROR_MERCHANT_CREATION_NOT_ALLOWED => 'Merchant creation is not allowed.',
                self::ERROR_MSA_ALREADY_EXISTS => 'The MSA user already exists.',
                self::ERROR_SHOP_ALREADY_EXISTS => 'The Shop already exists.',
                self::ERROR_TRANSACTION_ALREADY_FINISHED => 'Transaction is already finished.',
                self::ERROR_INVALID_TOKEN => 'Invalid token.',
                self::ERROR_MERCHANT_NOT_ALLOWED_TO_MAKE_CALL => 'Merchant doesn\'t have the right to do this call.',
                self::ERROR_PAYMENT_INSTRUMENT_UNKNOWN => 'The provided paymentInstrumentID is unknown.',
                self::ERROR_PAYMENT_METHODS_NOT_CONFIGURED => 'No payment method available due to configuration.',
                self::ERROR_ORDER_ALREADY_CAPTURED => 'This order was already captured.',
                self::ERROR_ORDER_ALREADY_CAPTURED_WITH_DIFFERENT_AMOUNT => 'This order was already captured with different Amount.',
                self::ERROR_TRANSACTION_CAPTURE_NOT_ALLOWED_IN_STATE => 'Capture is not allowed in this state of transaction.',
                self::ERROR_PAYMENT_INSTRUMENT_TYPE_INVALID => 'PaymentInstrument is not a bank account or credit card.',
                self::ERROR_MERCHANT_NOTIFICATION_FAILED => 'Merchant notification failed',
                self::ERROR_PAYMENT_INSTRUMENT_INVALID => 'The payment instrument is invalid.',
                self::ERROR_PAYMENT_INSTRUMENT_NO_SEPA_SUPPORT => 'The payment instrument does not support SEPA direct debit',
                self::ERROR_ACCOUNT_ROUTING_MATCH => 'Bank account or routing number do not match',
                self::ERROR_CVV_NOT_ALLOWED => "Non PCI compliant merchant is not allowed to send CVV",
                self::ERROR_CAPTURE_REJECTED => "Capture rejected",
                self::ERROR_MULTIPLE_CAPTURES_NOT_ALLOWED => "Multiple captures not allowed",
                self::ERROR_ISSUER_NOT_SUPPORTED => "Credit card issuer is not supported by the shop. Please try another card",
                self::ERROR_CREDITCARD_ISSUER_UNKNOWN => "The issuer of the credit card could not be detected. Please use another card",
                self::ERROR_CREDITCARD_NUMBER_EMPTY => "The credit card number is empty. Please enter a credit card number",
                self::ERROR_CREDITCARD_VALIDITY_MONTH_EMPTY => "The validity month of the credit card is empty. Please enter a validity month",
                self::ERROR_CREDITCARD_VALIDITY_YEAR_EMPTY => "The validity year of the credit card is empty. Please enter a validity year",
                self::ERROR_CREDITCARD_ACCOUNTHOLDER_EMPTY => "The name of the credit card holder is empty. Please enter an account holder",
                self::ERROR_ERROR_SENDING_MAIL => "Error in sending email",
                self::ERROR_INVALID_CREDITCARD_NUMBER => "Invalid card number",
                self::ERROR_DUNNING_NOT_ENABLED => "No active dunning configuration for transaction",
                self::ERROR_SUBSCRIPTION_PLAN_NOT_FOUND => "No plan found with this planReference",
                self::ERROR_INTEGRATIONTYPE_NOT_ALLOWED => "Transaction integration type not allowed",
                self::ERROR_SUBSCRIPTION_ALREADY_EXISTS => "Subscription ID already exists",
                self::ERROR_SUBSCRIPTION_NOT_FOUND => "The provided subscription id is unknown",
                self::ERROR_ACTION_NOT_ALLOWED_FOR_STATUS => "Method call is not allowed in this state of subscription",
                self::ERROR_OPTIONAL_PARAMETERS_HAVE_NO_VALUES => "No changes were performed. Please provide at least one optional parameter",
                self::ERROR_NO_SOLVENCYCHECK_REPORT_CONFIG => "Solvency checks are not configured",
                self::ERROR_CALLBACK_PARAMETER_FORMAT => 'Parameter or format error',
                self::ERROR_CALLBACK_UNKNOWN => 'Unknown',
                self::ERROR_CALLBACK_TRANSACTION_PAYMENT_METHOD_NOT_ALLOWED => 'Transaction or payment method not allowed',
                self::ERROR_CALLBACK_SYSTEM_TIMEOUT_NORESPONSE => 'System error, no response, timeout',
                self::ERROR_CALLBACK_CANCELED_USER => 'Cancelled by user',
                self::ERROR_CALLBACK_AUTHENTICATION_FAILED => 'Authentication failed',
                self::ERROR_CALLBACK_BLOCK_STOLEN_DECLINED_NOT_REASON => 'Expired, blocked, stolen, declined with no specific reason.',
                self::ERROR_CALLBACK_NO_JS_USER_BROWSER => 'No Javascript in user’s browser',
                self::ERROR_CALLBACK_OVERLOAD_PROCESSING_NOT_AVAILABLE => 'Overload, merchant busy, processing temporarily not possible',
                self::ERROR_CALLBACK_INVALID_AMOUNT => 'Invalid amount',
                self::ERROR_CALLBACK_FRAUD => 'Fraud',
                self::ERROR_CALLBACK_COMMUNICATION => 'Communication error',
                self::ERROR_CALLBACK_RESERVATION_EXPIRED => 'Configuration error',
                self::ERROR_CALLBACK_THIRD_PARTY => 'Third party gateway returned an error or declined transaction, details will be in the error description',
                self::ERROR_CALLBACK_NO_FUNDS => 'No funds',
                self::ERROR_CALLBACK_PARTIAL_REFUND_FAILED => 'Partial refund failed'
            );
        }

        if (array_key_exists($code, self::$errorCodes)) {
            return self::$errorCodes[$code];
        }

        return "Unknown Error Code";
    }

    /**
     * Checks if a code is a defined error
     *
     * @param int $code
     *
     * @return bool
     */
    public static function checkCodeIsError($code)
    {
        if ($code >= self::CODE_NON_ERROR_START && $code <= self::CODE_NON_ERROR_END) {
            return false;
        }

        return true;
    }
}
