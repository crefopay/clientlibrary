# CrefoPay Client Library for PHP  

PHP Client Library for the CrefoPay API.
Based on the API Documentation found here: https://docs.crefopay.de/api/
For questions regarding this library please send an email to [service@crefopay.de](mailto:service@crefopay.de)

## Reporting Issues

Please use our [Ticket-Board](https://repo.crefopay.de/crefopay/clientlibrary/-/boards) to create bug-reports and feature-requests.

## Known Issues

The following issues are known and will be added in coming versions:

See [Ticket-Board](https://repo.crefopay.de/crefopay/clientlibrary/-/boards)

## Unreleased Features

See [Changelog](https://repo.crefopay.de/crefopay/clientlibrary/-/blob/master/CHANGELOG.md)

## Installing the Library

Please use [Composer](https://getcomposer.org/) to install the library in your local environment:

```sh  
composer require crefopay/php-clientlibrary
```

After composer has been finished you will find the source files in `vendor/crefopay/php-clientlibrary/`

## Using the Library

For more detailed information, please refer to our [Wiki](https://repo.crefopay.de/crefopay/clientlibrary/wikis/home).

### Include the Library

To include all required sourcefiles you can easily use:

```php
require_once 'vendor/autoload.php';
```

### Requests ###
The Library for requests is split in three parts:
```CrefoPay\Library\Request``` contains the request classes.
```CrefoPay\Library\Request\Objects``` contains classes for the JSON objects that are documented in the API documentation.
If a request has a property that requires a JSON object please pass in the appropriately populated ```CrefoPay\Library\Request\Objects``` class for that property.

All properties in the request and JSON objects have getters and setters. For example, to set a field called userType on the request or object you would call `setUserType` and to get it you would call `getUserType`.

### Sending requests ###
Once you have populated a request object with the appropriate values simply create an instance of a ```CrefoPay\Library\Api```
class for the appropriate method call. Pass in a config object and the request you want to send. Then call the
`sendRequest()` method to send the response. This method might raise an Exception or provide an object of the class SuccessResponse.

The exceptions which can be raised are in ```CrefoPay\Library\Api\Exception```.
For any parsed responses you will have access to an ```CrefoPay\Library\Response\SuccessResponse``` or ```CrefoPay\Library\Response\FailureResponse``` object.
The SuccessResponse object is returned if no exception is thrown.
The FailureResponse object is returned in a ```CrefoPay\Library\Api\Exception\ApiError``` exception.

## Handling MNS notifications ##

The library provides a helper class to validate MNS notifications:
```CrefoPay\Library\Mns\Handler```

It takes the following as a constructor:

  * $config: The config object for the integration
  * $data: The data from the post variables which should be an associated array of the MNS callback
  * $processor: An instance of a class which implements the ```CrefoPay\Library\Mns\ProcessorInterface```, which the method will invoke after validation.

The processor object should implement `sendData` to get data from the handler and a `run` method which executes your callback after successful validation.

The processor callback should avoid processing the request, instead it should save it to a database for asynchronous processing via a cron script.

If you use the handler class to save a MNS to the database for later processing you can assume the MNS is perfectly valid with out checking the MAC.

Please note the MNS call must always return a 200 response to CrefoPay otherwise the CrefoPay system will retry to send this notification and eventually lock the MNS queue for your system.

## Working on the library ##

If you want to contribute to the library, please note that all code should be written according to the **PSR2 standard**.
There are many tools to conform to this standard, e.g. PHP-Codesniffer or PHP Coding Standards Fixer.
We will also specify a contributing guideline in the future.

## Exmaples

Some examples how the library works can be found [here](https://repo.crefopay.de/crefopay/clientlibrary/-/tree/master/examples).
