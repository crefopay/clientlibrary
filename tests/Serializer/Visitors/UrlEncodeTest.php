<?php

namespace CrefoPay\Library\Tests\Serializer\Visitors;

use PHPUnit\Framework\TestCase;
use CrefoPay\Library\Serializer\Serializer;
use CrefoPay\Library\Serializer\Visitors\Json as Json;
use CrefoPay\Library\Serializer\Visitors\UrlEncode as UrlEncode;
use CrefoPay\Library\Tests\Mock\Request\NonRecursiveUrlEncodeRequest;
use CrefoPay\Library\Tests\Mock\Request\RecursiveUrlEncodeRequest;
use CrefoPay\Library\Tests\Mock\Request\UrlRequestWithArray;

class UrlEncodeTest extends TestCase
{

    public function testNonRecursiveSerialization()
    {
        $request = new NonRecursiveUrlEncodeRequest();

        $formWalker = new UrlEncode();

        $serializedData = $formWalker->visit($request, new Serializer());

        $this->assertEquals("test=1&test2=2", $serializedData, "Message serialized string is not what is expected");

    }

    public function testRecursiveSerializationWithJson()
    {
        $request = new RecursiveUrlEncodeRequest();

        $formWalker = new UrlEncode();
        $serializer = new Serializer();

        $serializer->setVisitor($formWalker);
        $serializer->setVisitor(new Json());

        $serializedData = $formWalker->visit($request, $serializer);

        $this->assertEquals(
            "test=1&test2=2&testc=%7B%22test%22%3A1%2C%22test2%22%3A2%7D",
            $serializedData,
            "Message serialized string is not what is expected"
        );

    }

    /**
     * Test serialization of an array
     */
    public function testArraySerialization()
    {
        $request = new UrlRequestWithArray();

        $formWalker = new UrlEncode();
        $serializer = new Serializer();

        $serializer->setVisitor($formWalker);
        $serializer->setVisitor(new Json());

        $serializedData = $formWalker->visit($request, $serializer);

        $this->assertEquals(
            "test=1&test2=2&arrayValue=%5B%7B%22test%22%3A1%2C%22test2%22%3A2%7D%2C%7B%22test%22%3A1%2C%22test2%22%3A2%7D%5D",
            $serializedData,
            "Message serialized string is not what is expected"
        );
    }
}

