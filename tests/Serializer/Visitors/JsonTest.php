<?php

namespace CrefoPay\Library\Tests\Serializer\Visitors;

use PHPUnit\Framework\TestCase;
use CrefoPay\Library\Serializer\Serializer as Serializer;
use CrefoPay\Library\Serializer\Visitors\Json as Json;
use CrefoPay\Library\Tests\Mock\Request\JsonRequestWithArray;
use CrefoPay\Library\Tests\Mock\Request\NonRecursiveJsonRequest as NonRecursiveJsonRequest;
use CrefoPay\Library\Tests\Mock\Request\RecursiveJsonRequest;

class JsonTest extends TestCase
{

    public function testNonRecursiveSerialization()
    {
        $request = new NonRecursiveJsonRequest();

        $jsonWalker = new Json();

        $serializedData = $jsonWalker->visit($request, new Serializer());

        $this->assertJson($serializedData, "Walker did not return json");
        $this->assertJsonStringEqualsJsonString($serializedData, '{"test":1,"test2":2}');

    }

    public function testRecursiveSerialization()
    {
        $request = new RecursiveJsonRequest();

        $jsonWalker = new Json();
        $serializer = new Serializer();
        $serializer->setVisitor($jsonWalker);

        $serializedData = $jsonWalker->visit($request, $serializer);

        $this->assertJson($serializedData, "Walker did not return json");
        $this->assertJsonStringEqualsJsonString($serializedData, '{"testa":1,"testb":2,"testc":{"test":1,"test2":2}}');
    }

    /**
     * Test serialization of an array
     */
    public function testArraySerialization()
    {
        $request = new JsonRequestWithArray();

        $jsonWalker = new Json();
        $serializer = new Serializer();
        $serializer->setVisitor($jsonWalker);

        $serializedData = $jsonWalker->visit($request, $serializer);

        $this->assertJson($serializedData, "Walker did not return json");
        $this->assertJsonStringEqualsJsonString(
            $serializedData,
            '{"test":1,"test2":2,"arrayValue":[{"test":1,"test2":2},{"test":1,"test2":2}]}'
        );
    }
}
