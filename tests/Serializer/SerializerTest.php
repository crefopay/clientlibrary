<?php

namespace CrefoPay\Library\Tests\Serializer;

use Exception;
use PHPUnit\Framework\TestCase;
use CrefoPay\Library\AbstractException;
use CrefoPay\Library\Serializer\Serializer as Serializer;
use CrefoPay\Library\Tests\Mock\Request\NonRecursiveJsonRequest as NonRecursiveJsonRequest;
use CrefoPay\Library\Tests\Mock\Serializer\Visitors\Json as Json;

class SerializerTest extends TestCase
{
    /**
     * Test if a successful return of a string when a mock walker is found for a mock object
     */
    public function testSerializeSuccess()
    {
        $serializer = new Serializer();

        $serializer->setVisitor(new Json());

        $value = $serializer->serialize(new NonRecursiveJsonRequest());

        $this->assertInternalType('string', $value, "The serializer should return a string");
        $this->assertEquals('{"testa": 1}', $value, "The serialized string is not what is expected
        Please either amend the mock object or test");

    }

    /**
     * Test if an exception gets raised if a walker could not be found
     */
    public function testSerializerCouldNotFindVisitor()
    {
        try {
            $serializer = new Serializer();
            $serializer->serialize(new NonRecursiveJsonRequest());
            $this->fail("No exception was raised");

        } catch (AbstractException $e) {
            $this->assertInstanceOf(
                'CrefoPay\Library\Serializer\Exception\VisitorCouldNotBeFound',
                $e,
                "Incorrect Exception type raised"
            );

        } catch (Exception $e) {
            $instance = get_class($e);
            $this->fail("A non library serializer exception was raised instance is $instance, got: {$e->getMessage()}");

        }
    }
}
