<?php

namespace CrefoPay\Library\Tests\Endtoend\Integration;

use DateInterval;
use DateTime;
use CrefoPay\Library\Api\GetUserPaymentInstrument as GetUserPaymentInstrumentApi;
use CrefoPay\Library\Api\RegisterUser as RegisterUserApi;
use CrefoPay\Library\Api\RegisterUserPaymentInstrument as RegisterUserPaymentInstrumentApi;
use CrefoPay\Library\Locale\Codes;
use CrefoPay\Library\Request\GetUserPaymentInstrument as GetUserPaymentInstrumentRequest;
use CrefoPay\Library\Request\Objects\Address;
use CrefoPay\Library\Request\Objects\PaymentInstrument;
use CrefoPay\Library\Request\Objects\PaymentInstrument as PaymentInstrumentJson;
use CrefoPay\Library\Request\Objects\Person;
use CrefoPay\Library\Request\RegisterUser as RegisterUserRequest;
use CrefoPay\Library\Request\RegisterUserPaymentInstrument as RegisterUserPaymentInstrumentRequest;
use CrefoPay\Library\Risk\RiskClass;
use CrefoPay\Library\User\Type;

/**
 * @group Endtoend
 */
class GetUserPaymentInstrumentTest extends AbstractEndtoendTest
{
    private $user;

    private $address;

    private $paymentInstrument;

    private function getUser()
    {
        if (is_null($this->user)) {
            $date = new DateTime();
            $date->setDate(1980, 1, 1);

            $this->user = new Person();
            $this->user->setSalutation(Person::SALUTATIONMALE)
                ->setName($this->faker->name)
                ->setSurname($this->faker->name)
                ->setDateOfBirth($date)
                ->setEmail($this->email)
                ->setPhoneNumber('03452696645')
                ->setFaxNumber('03452696645');
        }

        return $this->user;
    }

    private function getAddress()
    {
        if (is_null($this->address)) {
            $this->address = new Address();
            $this->address->setStreet("Test")
                ->setNo(45)
                ->setZip("LS1 4TN")
                ->setCity("City")
                ->setState("State")
                ->setCountry("GB");
        }

        return $this->address;
    }

    private function getPaymentInstrument()
    {
        if (is_null($this->paymentInstrument)) {
            list($month, $year) = explode('/', $this->faker->creditCardExpirationDateString);
            $year = '20' . $year;

            $date = new DateTime();
            $date->setDate($year, $month, 1);
            $date->add(new DateInterval("P1Y"));

            $this->paymentInstrument = new PaymentInstrumentJson();
            $this->paymentInstrument->setPaymentInstrumentType(PaymentInstrumentJson::PAYMENT_INSTRUMENT_TYPE_CARD)
                ->setAccountHolder($this->faker->name)
                ->setIssuer(PaymentInstrumentJson::ISSUER_VISA)
                ->setValidity($date)
                ->setNumber('4539272776120245');
        }

        return $this->paymentInstrument;
    }

    /**
     * Make an successful call
     * Create a transaction then do the reserve call
     */
    public function testSuccessfulApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $userId = "REGISTERED:" . hash('md5', microtime());
        $billingRecipient = $this->faker->name;
        $shippingRecipient = $this->faker->name;

        $registerRequest = new RegisterUserRequest($this->config);
        $registerRequest->setUserID($userId)
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setBillingRecipient($billingRecipient)
            ->setShippingAddress($this->getAddress())
            ->setShippingRecipient($shippingRecipient)
            ->setLocale(Codes::LOCALE_EN);

        $registerUserApi = new RegisterUserApi($this->config, $registerRequest);
        $registerUserResult = $registerUserApi->sendRequest();

        $this->assertEquals(0, $registerUserResult->getData('resultCode'));
        $this->assertEmpty($registerUserResult->getData('message'));
        $this->assertNotEmpty($registerUserResult->getData('salt'));


        $registerUserPaymentInstrumentRequest = new RegisterUserPaymentInstrumentRequest($this->config);
        $registerUserPaymentInstrumentRequest->setUserID($userId)
            ->setPaymentInstrument($this->getPaymentInstrument());

        $registerUserPaymentInstrument = new RegisterUserPaymentInstrumentApi(
            $this->config,
            $registerUserPaymentInstrumentRequest
        );

        $registerUserPaymentInstrumentResult = $registerUserPaymentInstrument->sendRequest();

        $paymentId = $registerUserPaymentInstrumentResult->getData('paymentInstrumentID');

        $getUserPaymentInstrumentRequest = new GetUserPaymentInstrumentRequest($this->config);
        $getUserPaymentInstrumentRequest->setUserID($userId);

        $getUserPaymentInstrumentApi = new GetUserPaymentInstrumentApi($this->config, $getUserPaymentInstrumentRequest);

        $getUserPaymentInstrumentResult = $getUserPaymentInstrumentApi->sendRequest();

        $this->assertEquals(0, $getUserPaymentInstrumentResult->getData('resultCode'));
        $this->assertEmpty($getUserPaymentInstrumentResult->getData('message'));
        $this->assertNotEmpty($getUserPaymentInstrumentResult->getData('salt'));
        $this->assertEquals(1, count($getUserPaymentInstrumentResult->getData('paymentInstruments')));

        $paymentInstruments = $getUserPaymentInstrumentResult->getData('paymentInstruments');
        /** @var $paymentInstrument PaymentInstrument */
        $paymentInstrument = $paymentInstruments[0];

        $this->assertEquals($paymentInstrument->getPaymentInstrumentID(), $paymentId);
        $this->assertEquals($this->getPaymentInstrument()->getIssuer(), $paymentInstrument->getIssuer());
        $this->assertEquals(
            $this->getPaymentInstrument()->getPaymentInstrumentType(),
            $paymentInstrument->getPaymentInstrumentType()
        );

        $this->assertEquals(
            $this->getPaymentInstrument()->getValidity()->format('Y-m'),
            $paymentInstrument->getValidity()->format('Y-m')
        );

    }
}