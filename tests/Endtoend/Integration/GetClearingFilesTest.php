<?php

namespace CrefoPay\Library\Tests\Endtoend\Integration;

use CrefoPay\Library\Api\Exception\ApiError;
use CrefoPay\Library\Api\GetClearingFiles as GetClearingFilesApi;
use CrefoPay\Library\Request\GetClearingFiles as GetClearingFilesRequest;

/**
 * @group Endtoend
 */
class GetClearingFilesTest extends AbstractEndtoendTest
{
    /**
     * Make an successful call
     */
    public function testSuccessfulApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $clearingFilesRequest = new GetClearingFilesRequest($this->config);
        $clearingFilesRequest->setClearingFileID("8")
            ->setPath("");

        $clearingFiles = new GetClearingFilesApi($this->config, $clearingFilesRequest);
        $result = $clearingFiles->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('filename'));
        $file = $result->getData('filename');
        $this->assertNotEmpty($file);
        unlink($file);
    }

    /**
     * Make an successful call
     */
    public function testFailedApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $clearingFilesRequest = new GetClearingFilesRequest($this->config);
        $clearingFilesRequest->setClearingFileID("1a")
            ->setPath("");

        $clearingFiles = new GetClearingFilesApi($this->config, $clearingFilesRequest);
        try {
            $clearingFiles->sendRequest();
        } catch (ApiError $e) {
            $this->assertEquals(6002, $e->getParsedResponse()->getData('resultCode'));
        }
    }
}