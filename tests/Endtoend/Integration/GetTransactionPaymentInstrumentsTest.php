<?php

namespace CrefoPay\Library\Tests\Endtoend\Integration;

use DateInterval;
use DateTime;
use CrefoPay\Library\Api\CreateTransaction;
use CrefoPay\Library\Api\GetTransactionPaymentInstruments as GetTransactionPaymentInstrumentsApi;
use CrefoPay\Library\Api\RegisterUserPaymentInstrument as RegisterUserPaymentInstrumentApi;
use CrefoPay\Library\Integration\Type as IntegrationType;
use CrefoPay\Library\Locale\Codes;
use CrefoPay\Library\Request\CreateTransaction as CreateTransactionRequest;
use CrefoPay\Library\Request\GetTransactionPaymentInstruments as GetTransactionPaymentInstrumentsRequest;
use CrefoPay\Library\Request\Objects\Address;
use CrefoPay\Library\Request\Objects\Amount;
use CrefoPay\Library\Request\Objects\BasketItem;
use CrefoPay\Library\Request\Objects\PaymentInstrument;
use CrefoPay\Library\Request\Objects\PaymentInstrument as PaymentInstrumentJson;
use CrefoPay\Library\Request\Objects\Person;
use CrefoPay\Library\Request\RegisterUserPaymentInstrument as RegisterUserPaymentInstrumentRequest;
use CrefoPay\Library\Risk\RiskClass;
use CrefoPay\Library\User\Type;

/**
 * @group Endtoend
 */
class GetTransactionPaymentInstrumentsTest extends AbstractEndtoendTest
{
    private $paymentInstrument;
    private $paymentInstrumentDD;

    private function getUser()
    {
        $date = new DateTime();
        $date->setDate(1980, 1, 1);

        $user = new Person();
        $user->setSalutation(PERSON::SALUTATIONMALE)
            ->setName($this->faker->name)
            ->setSurname($this->faker->name)
            ->setDateOfBirth($date)
            ->setEmail($this->email)
            ->setPhoneNumber('03452696645')
            ->setFaxNumber('03452696645');

        return $user;
    }

    private function getAddress()
    {
        $address = new Address();
        $address->setStreet("Test")
            ->setNo(45)
            ->setZip("LS1 4TN")
            ->setCity("City")
            ->setState("State")
            ->setCountry("GB");

        return $address;
    }

    private function getPaymentInstrument()
    {
        if (is_null($this->paymentInstrument)) {
            list($month, $year) = explode('/', $this->faker->creditCardExpirationDateString);
            $year = '20' . $year;

            $date = new DateTime();
            $date->setDate($year, $month, 1);
            $date->add(new DateInterval("P1Y"));

            $this->paymentInstrument = new PaymentInstrumentJson();
            $this->paymentInstrument->setPaymentInstrumentType(PaymentInstrumentJson::PAYMENT_INSTRUMENT_TYPE_CARD)
                ->setAccountHolder($this->faker->name)
                ->setIssuer(PaymentInstrumentJson::ISSUER_VISA)
                ->setValidity($date)
                ->setNumber('4539272776120245');
        }

        return $this->paymentInstrument;
    }

    private function getPaymentInstrumentDD()
    {
        if (is_null($this->paymentInstrumentDD)) {
            $this->paymentInstrumentDD = new PaymentInstrumentJson();
            $this->paymentInstrumentDD->setPaymentInstrumentType(PaymentInstrumentJson::PAYMENT_INSTRUMENT_TYPE_BANK)
                ->setAccountHolder($this->faker->name)
                ->setIban('DE94410500955127507621')
                ->setBic('WELADED1HAM');
        }

        return $this->paymentInstrumentDD;
    }

    private function getAmount()
    {
        return new Amount(101, 0, 0);
    }

    private function getBasketItem()
    {
        $item = new BasketItem();
        $item->setBasketItemText("Test Item CALL")
            ->setBasketItemCount(1)
            ->setBasketItemAmount($this->getAmount());

        return $item;
    }

    /**
     * Make an successful call
     * Create a transaction
     * Register credit card and bank account
     * Do the getTransactionPaymentInstruments call
     */
    public function testSuccessfulApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());
        $userId = "GUEST:" . hash('md5', microtime());

        $request->setOrderID($orderId)
            ->setUserID($userId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_API)
            ->setAutoCapture(false)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setMerchantReference("TEST")
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $createTransactionResult = $apiEndPoint->sendRequest();

        $this->assertEquals(0, $createTransactionResult->getData('resultCode'));

        $allowedPayments = $createTransactionResult->getData('allowedPaymentMethods');

        $this->assertTrue(in_array('CC', $createTransactionResult->getData('allowedPaymentMethods')));

        if (!in_array('CC', $allowedPayments)) {
            $this->fail("Allowed payment from CreateTransaction lacks CC can not continue");
        }

        /**
         * Register payment instrument
         */
        $registerUserPaymentInstrumentRequest = new RegisterUserPaymentInstrumentRequest($this->config);
        $registerUserPaymentInstrumentRequest->setUserID($userId)
            ->setPaymentInstrument($this->getPaymentInstrument());

        $registerUserPaymentInstrument = new RegisterUserPaymentInstrumentApi(
            $this->config,
            $registerUserPaymentInstrumentRequest
        );

        $registerUserPaymentInstrumentResult = $registerUserPaymentInstrument->sendRequest();

        $paymentInstrumentId = $registerUserPaymentInstrumentResult->getData('paymentInstrumentID');

        /**
         * Register payment instrument
         */
        $registerDirectDebitRequest = new RegisterUserPaymentInstrumentRequest($this->config);
        $registerDirectDebitRequest->setUserID($userId)
            ->setPaymentInstrument($this->getPaymentInstrumentDD());

        $registerDirectDebit = new RegisterUserPaymentInstrumentApi(
            $this->config,
            $registerDirectDebitRequest
        );

        $registerDirectDebitResult = $registerDirectDebit->sendRequest();

        $directDebitPaymentInstrumentId = $registerDirectDebitResult->getData('paymentInstrumentID');

        /**
         * Do the getTransactionPaymentInstruments call
         */
        $txRequest = new GetTransactionPaymentInstrumentsRequest($this->config);
        $txRequest->setOrderID($orderId);

        $txApi = new GetTransactionPaymentInstrumentsApi($this->config, $txRequest);

        $result = $txApi->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertNotEmpty($result->getData('allowedPaymentInstruments'));
        $creditCardId = null;
        $directDebitId = null;
        $directDebitInstrument = null;
        foreach ($result->getData('allowedPaymentInstruments') as $object) {
            /** @var $object PaymentInstrument */
            if ($object->getPaymentInstrumentType() === PaymentInstrumentJson::PAYMENT_INSTRUMENT_TYPE_CARD) {
                $creditCardId = $object->getPaymentInstrumentID();
            }
            if ($object->getPaymentInstrumentType() === PaymentInstrumentJson::PAYMENT_INSTRUMENT_TYPE_BANK) {
                $directDebitId = $object->getPaymentInstrumentID();
                $directDebitInstrument = $object;
            }
        }
        if ($creditCardId === null) {
            $this->fail(
                "allowedPaymentInstruments doesn't include a credit card, the test must have failed to register one earlier"
            );
        }
        // Asserting, that the credit card is the same as the one registered earlier.
        $this->assertEquals($paymentInstrumentId, $creditCardId);
        if ($directDebitId === null) {
            $this->fail(
                "allowedPaymentInstruments doesn't include a direct debit account, the test must have failed to register one earlier"
            );
        }
        // Asserting, that the direct debit account is the same as the one registered earlier.
        $this->assertEquals($directDebitPaymentInstrumentId, $directDebitId);
        $this->assertEquals('DE94410500955127507621', $directDebitInstrument->getUnmaskedIban());
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('salt'));
    }
}
