<?php

namespace CrefoPay\Library\Tests\Endtoend\Integration;

use DateInterval;
use DateTime;
use CrefoPay\Library\Api\CreateTransaction;
use CrefoPay\Library\Api\Exception\ApiError;
use CrefoPay\Library\Api\GetTransactionStatus as GetTransactionStatusApi;
use CrefoPay\Library\Api\RegisterUserPaymentInstrument as RegisterUserPaymentInstrumentApi;
use CrefoPay\Library\Api\Reserve as ReserveApi;
use CrefoPay\Library\Integration\Type as IntegrationType;
use CrefoPay\Library\Locale\Codes;
use CrefoPay\Library\PaymentMethods\Methods;
use CrefoPay\Library\Request\CreateTransaction as CreateTransactionRequest;
use CrefoPay\Library\Request\GetTransactionStatus as GetTransactionStatusRequest;
use CrefoPay\Library\Request\Objects\Address;
use CrefoPay\Library\Request\Objects\Amount;
use CrefoPay\Library\Request\Objects\BasketItem;
use CrefoPay\Library\Request\Objects\PaymentInstrument as PaymentInstrumentJson;
use CrefoPay\Library\Request\Objects\Person;
use CrefoPay\Library\Request\Objects\SolvencyData;
use CrefoPay\Library\Request\RegisterUserPaymentInstrument as RegisterUserPaymentInstrumentRequest;
use CrefoPay\Library\Request\Reserve as ReserveRequest;
use CrefoPay\Library\Risk\RiskClass;
use CrefoPay\Library\User\Type;

/**
 * @group Endtoend
 */
class GetTransactionStatusTest extends AbstractEndtoendTest
{
    private $paymentInstrument;

    private $user;

    private function getUser()
    {
        if (is_null($this->user)) {
            $date = new DateTime();
            $date->setDate(1980, 1, 1);

            $this->user = new Person();
            $this->user->setSalutation(PERSON::SALUTATIONMALE)
                ->setName($this->faker->name)
                ->setSurname($this->faker->name)
                ->setDateOfBirth($date)
                ->setEmail(strtolower($this->email))
                ->setPhoneNumber('03452696645')
                ->setFaxNumber('03452696645');
        }

        return $this->user;
    }

    private function getAddress()
    {
        $address = new Address();
        $address->setStreet("Test")
            ->setNo(45)
            ->setZip("12100")
            ->setCity("City")
            ->setState("State")
            ->setCountry("DE");

        return $address;
    }

    private function getPaymentInstrument()
    {
        if (is_null($this->paymentInstrument)) {
            list($month, $year) = explode('/', $this->faker->creditCardExpirationDateString);
            $year = '20' . $year;

            $date = new DateTime();
            $date->setDate($year, $month, 1);
            $date->add(new DateInterval("P1Y"));

            $this->paymentInstrument = new PaymentInstrumentJson();
            $this->paymentInstrument->setPaymentInstrumentType(PaymentInstrumentJson::PAYMENT_INSTRUMENT_TYPE_CARD)
                ->setAccountHolder($this->faker->name)
                ->setIssuer(PaymentInstrumentJson::ISSUER_VISA)
                ->setValidity($date)
                ->setNumber('4539272776120245');
        }

        return $this->paymentInstrument;
    }

    private function getAmount()
    {
        return new Amount(101, 0, 0);
    }

    private function getBasketItem()
    {
        $item = new BasketItem();
        $item->setBasketItemText("Test Item CALL")
            ->setBasketItemCount(1)
            ->setBasketItemAmount($this->getAmount());

        return $item;
    }

    /**
     * Make an successful call
     * Create a transaction then do the reserve call
     */
    public function testSuccessfulApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());
        $userId = "GUEST:" . hash('md5', microtime());

        $request->setOrderID($orderId)
            ->setUserID($userId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_API)
            ->setAutoCapture(false)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setMerchantReference("TEST")
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $createTransactionResult = $apiEndPoint->sendRequest();

        $this->assertEquals(0, $createTransactionResult->getData('resultCode'));

        $allowedPayments = $createTransactionResult->getData('allowedPaymentMethods');

        $this->assertTrue(in_array('CC', $createTransactionResult->getData('allowedPaymentMethods')));

        if (!in_array('CC', $allowedPayments)) {
            $this->fail("Allowed payment from CreateTransaction lacks CC can not continue");
        }

        /**
         * Register payment instrument
         */
        $registerUserPaymentInstrumentRequest = new RegisterUserPaymentInstrumentRequest($this->config);
        $registerUserPaymentInstrumentRequest->setUserID($userId)
            ->setPaymentInstrument($this->getPaymentInstrument());

        $registerUserPaymentInstrument = new RegisterUserPaymentInstrumentApi(
            $this->config,
            $registerUserPaymentInstrumentRequest
        );

        $registerUserPaymentInstrumentResult = $registerUserPaymentInstrument->sendRequest();

        $paymentInstrumentId = $registerUserPaymentInstrumentResult->getData('paymentInstrumentID');

        $reserveRequest = new ReserveRequest($this->config);

        $reserveRequest->setOrderID($orderId)
            ->setPaymentMethod(Methods::PAYMENT_METHOD_TYPE_CC)
            ->setPaymentInstrumentID($paymentInstrumentId)
            ->setCvv(123);

        $reserveApi = new ReserveApi($this->config, $reserveRequest);
        $reserveApi->sendRequest();

        $getTransactionStatusRequest = new GetTransactionStatusRequest($this->config);
        $getTransactionStatusRequest->setOrderID($orderId);

        /**
         * Now test the getTransactionStatus call
         */
        $getTransactionStatusApi = new GetTransactionStatusApi($this->config, $getTransactionStatusRequest);
        $result = $getTransactionStatusApi->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('salt'));
        $this->assertEquals('MERCHANTPENDING', $result->getData('transactionStatus'));

        $additionalData = $result->getData('additionalData');
        $this->assertEquals('CC', $additionalData['paymentMethod']);
        $this->assertEquals(101, $additionalData['transactionAmount']);
        $this->assertEquals($this->getUser()->getEmail(), $additionalData['customerEmail']);
    }

    /**
     * Make an successful call with no risk data set
     * Create a transaction then request the risk data
     */
    public function testSuccessfulRiskDataEmptyApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());
        $userId = "GUEST:" . hash('md5', microtime());

        $request->setOrderID($orderId)
            ->setUserID($userId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_API)
            ->setAutoCapture(false)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setMerchantReference("TEST")
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $createTransactionResult = $apiEndPoint->sendRequest();

        $this->assertEquals(0, $createTransactionResult->getData('resultCode'));

        $allowedPayments = $createTransactionResult->getData('allowedPaymentMethods');

        $this->assertTrue(in_array('CC', $createTransactionResult->getData('allowedPaymentMethods')));

        if (!in_array('CC', $allowedPayments)) {
            $this->fail("Allowed payment from CreateTransaction lacks CC can not continue");
        }

        /**
         * Register payment instrument
         */
        $registerUserPaymentInstrumentRequest = new RegisterUserPaymentInstrumentRequest($this->config);
        $registerUserPaymentInstrumentRequest->setUserID($userId)
            ->setPaymentInstrument($this->getPaymentInstrument());

        $registerUserPaymentInstrument = new RegisterUserPaymentInstrumentApi(
            $this->config,
            $registerUserPaymentInstrumentRequest
        );

        $registerUserPaymentInstrumentResult = $registerUserPaymentInstrument->sendRequest();

        $paymentInstrumentId = $registerUserPaymentInstrumentResult->getData('paymentInstrumentID');

        $reserveRequest = new ReserveRequest($this->config);

        $reserveRequest->setOrderID($orderId)
            ->setPaymentMethod(Methods::PAYMENT_METHOD_TYPE_CC)
            ->setPaymentInstrumentID($paymentInstrumentId)
            ->setCvv(123);

        $reserveApi = new ReserveApi($this->config, $reserveRequest);
        $reserveApi->sendRequest();

        $getTransactionStatusRequest = new GetTransactionStatusRequest($this->config);
        $getTransactionStatusRequest->setOrderID($orderId)
            ->setReturnRiskData(true);

        /**
         * Now test the getTransactionStatus call
         */
        $getTransactionStatusApi = new GetTransactionStatusApi($this->config, $getTransactionStatusRequest);
        $result = $getTransactionStatusApi->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('salt'));
        $this->assertEquals('MERCHANTPENDING', $result->getData('transactionStatus'));

        $riskData = $result->getData('riskData');
        $this->assertNotEmpty($riskData);
        $this->assertEmpty($riskData['solvencyData']);
    }

    /**
     * Make an successful call with risk data set
     * Create a transaction then request the risk data
     */
    public function testSuccessfulRiskDataSetApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());
        $userId = "GUEST:" . hash('md5', microtime());

        $request->setOrderID($orderId)
            ->setUserID($userId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_API)
            ->setAutoCapture(false)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setMerchantReference("TEST")
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $createTransactionResult = $apiEndPoint->sendRequest();

        $this->assertEquals(0, $createTransactionResult->getData('resultCode'));

        $allowedPayments = $createTransactionResult->getData('allowedPaymentMethods');

        $this->assertTrue(in_array('BILL', $createTransactionResult->getData('allowedPaymentMethods')));

        if (!in_array('BILL', $allowedPayments)) {
            $this->fail("Allowed payment from CreateTransaction lacks BILL can not continue");
        }

        $reserveRequest = new ReserveRequest($this->config);

        $reserveRequest->setOrderID($orderId)
            ->setPaymentMethod(Methods::PAYMENT_METHOD_TYPE_BILL);

        $reserveApi = new ReserveApi($this->config, $reserveRequest);
        try {
            $reserveApi->sendRequest();
        } catch (ApiError $ae) {
        }

        $getTransactionStatusRequest = new GetTransactionStatusRequest($this->config);
        $getTransactionStatusRequest->setOrderID($orderId)
            ->setReturnRiskData(true);

        /**
         * Now test the getTransactionStatus call
         */
        $getTransactionStatusApi = new GetTransactionStatusApi($this->config, $getTransactionStatusRequest);
        $result = $getTransactionStatusApi->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('salt'));
        $this->assertEquals('NEW', $result->getData('transactionStatus'));

        $riskData = $result->getData('riskData');
        $this->assertNotEmpty($riskData);
        $this->assertNotEmpty($riskData['solvencyData']);
        $this->assertEquals(1, count($riskData['solvencyData']));
        $this->assertInstanceOf('\CrefoPay\Library\Request\Objects\SolvencyData', $riskData['solvencyData'][0]);
        /** @var SolvencyData $solvencyResult */
        $solvencyResult = $riskData['solvencyData'][0];
        $this->assertEquals(SolvencyData::CHECK_INTERFACE_BONIMA, $solvencyResult->getSolvencyInterface());
        $this->assertEquals(SolvencyData::CHECK_TYPE_SECONDLEVEL, $solvencyResult->getCheckType());
        $this->assertEquals(Methods::PAYMENT_METHOD_TYPE_BILL, $solvencyResult->getPaymentMethod());
        $this->assertEquals("Anmeldung Fehler", $solvencyResult->getMessage());
        $this->assertEquals(true, $solvencyResult->isThirdPartyRequested());
    }
}
