<?php

namespace CrefoPay\Library\Tests\Endtoend\Integration;

use DateTime;
use CrefoPay\Library\Api\SolvencyCheck as SolvencyCheckApi;
use CrefoPay\Library\Request\Objects\Address;
use CrefoPay\Library\Request\Objects\Company;
use CrefoPay\Library\Request\Objects\Person;
use CrefoPay\Library\Request\SolvencyCheck as SolvencyCheckRequest;
use CrefoPay\Library\User\Type;

/**
 * @group Endtoend
 */
class SolvencyCheckTest extends AbstractEndtoendTest
{
    private function getUser()
    {
        $date = new DateTime();
        $date->setDate(1980, 1, 1);

        $user = new Person();
        $user->setSalutation(Person::SALUTATIONMALE)
            ->setName($this->faker->name)
            ->setSurname($this->faker->name)
            ->setDateOfBirth($date)
            ->setEmail($this->email)
            ->setPhoneNumber('03452696645')
            ->setFaxNumber('03452696645');

        return $user;
    }

    private function getCompany()
    {
        $company = new Company();
        $company->setCompanyName('CrefoPayment GmbH & Co. KG')
            ->setEmail('service@crefopay.de');
        return $company;
    }

    private function getAddress()
    {
        $address = new Address();
        $address->setStreet("Test")
            ->setNo(45)
            ->setZip("LS1 4TN")
            ->setCity("City")
            ->setState("State")
            ->setCountry("GB");

        return $address;
    }

    /**
     * Make an successful call
     * Create a transaction then do the reserve call
     * @expectedException \CrefoPay\Library\Api\Exception\ApiError
     */
    public function testSuccessfulApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $userId = "REGISTERED:" . hash('md5', microtime());

        $solvencyCheckRequest = new SolvencyCheckRequest($this->config);
        $solvencyCheckRequest->setUserID($userId)
            ->setUserType(Type::USER_TYPE_BUSINESS)
            ->setCompanyData($this->getCompany())
            ->setBillingAddress($this->getAddress())
            ->setBillingRecipient($this->faker->name);

        $solvencyCheckApi = new SolvencyCheckApi($this->config, $solvencyCheckRequest);
        $result = $solvencyCheckApi->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('checkResult'));
        $this->assertNotEmpty($result->getData('salt'));
    }
}
