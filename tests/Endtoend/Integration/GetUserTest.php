<?php

namespace CrefoPay\Library\Tests\Endtoend\Integration;

use DateTime;
use CrefoPay\Library\Api\CreateTransaction;
use CrefoPay\Library\Api\Exception\ApiError;
use CrefoPay\Library\Api\GetUser as GetUserApi;
use CrefoPay\Library\Api\RegisterUser as RegisterUserApi;
use CrefoPay\Library\Api\Reserve as ReserveApi;
use CrefoPay\Library\Integration\Type as IntegrationType;
use CrefoPay\Library\Locale\Codes;
use CrefoPay\Library\PaymentMethods\Methods;
use CrefoPay\Library\Request\CreateTransaction as CreateTransactionRequest;
use CrefoPay\Library\Request\GetUser as GetUserRequest;
use CrefoPay\Library\Request\Objects\Address;
use CrefoPay\Library\Request\Objects\Amount;
use CrefoPay\Library\Request\Objects\BasketItem;
use CrefoPay\Library\Request\Objects\Person;
use CrefoPay\Library\Request\Objects\SolvencyData;
use CrefoPay\Library\Request\RegisterUser as RegisterUserRequest;
use CrefoPay\Library\Request\Reserve as ReserveRequest;
use CrefoPay\Library\Risk\RiskClass;
use CrefoPay\Library\User\Type;

/**
 * @group Endtoend
 */
class GetUserTest extends AbstractEndtoendTest
{
    private $user;

    private $address;

    public static $userId;

    public static function setUpBeforeClass() : void
    {
        self::$userId = "REGISTERED:" . hash('md5', microtime());
    }

    private function getUser()
    {
        if (is_null($this->user)) {
            $date = new DateTime();
            $date->setDate(1980, 1, 1);

            $this->user = new Person();
            $this->user->setSalutation(Person::SALUTATIONMALE)
                ->setName($this->faker->name)
                ->setSurname($this->faker->name)
                ->setDateOfBirth($date)
                ->setEmail(strtolower($this->email))
                ->setPhoneNumber('03452696645')
                ->setFaxNumber('03452696645');
        }

        return $this->user;
    }

    private function getAddress()
    {
        if (is_null($this->address)) {
            $this->address = new Address();
            $this->address->setStreet("Test")
                ->setNo(45)
                ->setZip("12100")
                ->setCity("City")
                ->setState("State")
                ->setCountry("DE");
        }

        return $this->address;
    }

    private function getBasketItem()
    {
        $item = new BasketItem();
        $item->setBasketItemText("Test Item CALL")
            ->setBasketItemCount(1)
            ->setBasketItemAmount(new Amount(101));

        return $item;
    }

    /**
     * Make an successful call
     * Create a transaction then do the reserve call
     */
    public function testSuccessfulApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }
        $billingRecipient = $this->faker->name;
        $shippingRecipient = $this->faker->name;

        $registerRequest = new RegisterUserRequest($this->config);
        $registerRequest->setUserID(self::$userId)
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setBillingRecipient($billingRecipient)
            ->setShippingAddress($this->getAddress())
            ->setShippingRecipient($shippingRecipient)
            ->setLocale(Codes::LOCALE_EN);

        $registerUserApi = new RegisterUserApi($this->config, $registerRequest);
        $registerUserApi->sendRequest();

        /**
         * ok now get the user
         */
        $getUserRequest = new GetUserRequest($this->config);
        $getUserRequest->setUserID(self::$userId);

        $getUserApi = new GetUserApi($this->config, $getUserRequest);
        $result = $getUserApi->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('salt'));

        /**
         * @var Person $returnedPerson
         */
        $returnedPerson = $result->getData('userData');
        $this->assertEquals(Person::SALUTATIONMALE, $returnedPerson->getSalutation());
        $this->assertEquals($this->getUser()->getName(), $returnedPerson->getName());
        $this->assertEquals($this->getUser()->getSurname(), $returnedPerson->getSurname());
        $this->assertEquals(
            $this->getUser()->getDateOfBirth()->format('Y-m-d'),
            $returnedPerson->getDateOfBirth()->format('Y-m-d')
        );

        $this->assertEquals($this->getUser()->getEmail(), $returnedPerson->getEmail());
        $this->assertEquals($this->getUser()->getPhoneNumber(), $returnedPerson->getPhoneNumber());

        $this->assertEquals($billingRecipient, $result->getData('billingRecipient'));

        /**
         * billingAddress and shippingAddress should be converted to an address object
         */
        $this->assertEquals($this->getAddress()->getNo(), $result->getData('billingAddress')->getNo());
        $this->assertEquals($this->getAddress()->getStreet(), $result->getData('billingAddress')->getStreet());
        $this->assertEquals($this->getAddress()->getZip(), $result->getData('billingAddress')->getZip());
        $this->assertEquals($this->getAddress()->getCity(), $result->getData('billingAddress')->getCity());
        $this->assertEquals($this->getAddress()->getState(), $result->getData('billingAddress')->getState());
        $this->assertEquals($this->getAddress()->getCountry(), $result->getData('billingAddress')->getCountry());

        $this->assertEquals($this->getAddress()->getNo(), $result->getData('shippingAddress')->getNo());
        $this->assertEquals($this->getAddress()->getStreet(), $result->getData('shippingAddress')->getStreet());
        $this->assertEquals($this->getAddress()->getZip(), $result->getData('shippingAddress')->getZip());
        $this->assertEquals($this->getAddress()->getCity(), $result->getData('shippingAddress')->getCity());
        $this->assertEquals($this->getAddress()->getState(), $result->getData('shippingAddress')->getState());
        $this->assertEquals($this->getAddress()->getCountry(), $result->getData('shippingAddress')->getCountry());
    }

    /**
     * Make an successful call with no risk data set
     * Create a transaction then request the risk data
     *
     * @depends testSuccessfulApiCall
     */
    public function testSuccessfulRiskDataEmptyApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        // Unique order ID for the tests
        $orderId = hash('crc32b', microtime());

        $request->setOrderID($orderId)
            ->setUserID(self::$userId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_API)
            ->setAutoCapture(false)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setMerchantReference("TEST")
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setAmount(new Amount(101))
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN)
            ->setKnownCustomer(true);

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $createTransactionResult = $apiEndPoint->sendRequest();

        $this->assertEquals(0, $createTransactionResult->getData('resultCode'));

        $allowedPayments = $createTransactionResult->getData('allowedPaymentMethods');

        if (!in_array('PREPAID', $allowedPayments)) {
            $this->fail("Allowed payment from CreateTransaction lacks PREPAID can not continue");
        }

        $reserveRequest = new ReserveRequest($this->config);

        $reserveRequest->setOrderID($orderId)
            ->setPaymentMethod(Methods::PAYMENT_METHOD_TYPE_PREPAID);

        $reserveApi = new ReserveApi($this->config, $reserveRequest);
        $reserveApi->sendRequest();

        $getUserRequest = new GetUserRequest($this->config);
        $getUserRequest->setUserID(self::$userId)
            ->setReturnRiskData(true);

        /**
         * Now test the getTransactionStatus call
         */
        $getUserApi = new GetUserApi($this->config, $getUserRequest);
        $result = $getUserApi->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('salt'));

        $riskData = $result->getData('riskData');
        $this->assertNotEmpty($riskData);
        $this->assertEmpty($riskData['solvencyData']);
    }

    /**
     * Make an successful call with risk data set
     * Create a transaction then request the risk data
     *
     * @depends testSuccessfulApiCall
     */
    public function testSuccessfulRiskDataSetApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }
        $request = new CreateTransactionRequest($this->config);

        // Unique order ID for the tests
        $orderId = hash('crc32b', microtime());

        $request->setOrderID($orderId)
            ->setUserID(self::$userId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_API)
            ->setAutoCapture(false)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setMerchantReference("TEST")
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setAmount(new Amount(101))
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN)
            ->setKnownCustomer(true);

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $createTransactionResult = $apiEndPoint->sendRequest();

        $this->assertEquals(0, $createTransactionResult->getData('resultCode'));

        $allowedPayments = $createTransactionResult->getData('allowedPaymentMethods');

        if (!in_array('BILL', $allowedPayments)) {
            $this->fail("Allowed payment from CreateTransaction lacks BILL can not continue");
        }

        $reserveRequest = new ReserveRequest($this->config);

        $reserveRequest->setOrderID($orderId)
            ->setPaymentMethod(Methods::PAYMENT_METHOD_TYPE_BILL);

        $reserveApi = new ReserveApi($this->config, $reserveRequest);
        try {
            $reserveApi->sendRequest();
        } catch (ApiError $ae) {
        }

        $getUserRequest = new GetUserRequest($this->config);
        $getUserRequest->setUserID(self::$userId)
            ->setReturnRiskData(true);

        /**
         * Now test the getTransactionStatus call
         */
        $getUserApi = new GetUserApi($this->config, $getUserRequest);
        $result = $getUserApi->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('salt'));

        $riskData = $result->getData('riskData');
        $this->assertNotEmpty($riskData);
        $this->assertNotEmpty($riskData['solvencyData']);
        $this->assertEquals(1, count($riskData['solvencyData']));
        $this->assertInstanceOf('\CrefoPay\Library\Request\Objects\SolvencyData', $riskData['solvencyData'][0]);
        /** @var SolvencyData $solvencyResult */
        $solvencyResult = $riskData['solvencyData'][0];
        $this->assertEquals(SolvencyData::CHECK_INTERFACE_BONIMA, $solvencyResult->getSolvencyInterface());
        $this->assertEquals(SolvencyData::CHECK_TYPE_SECONDLEVEL, $solvencyResult->getCheckType());
        $this->assertEquals(Methods::PAYMENT_METHOD_TYPE_BILL, $solvencyResult->getPaymentMethod());
        $this->assertEquals("Anmeldung Fehler", $solvencyResult->getMessage());
        $this->assertEquals(true, $solvencyResult->isThirdPartyRequested());
    }

    public static function tearDownAfterClass() : void
    {
        self::$userId = null;
    }
}
