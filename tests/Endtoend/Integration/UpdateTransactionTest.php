<?php

namespace CrefoPay\Library\Tests\Endtoend\Integration;

use DateTime;
use CrefoPay\Library\Api\Capture as CaptureApi;
use CrefoPay\Library\Api\CreateTransaction;
use CrefoPay\Library\Api\Reserve as ReserveApi;
use CrefoPay\Library\Api\UpdateTransaction as UpdateTransactionApi;
use CrefoPay\Library\Integration\Type as IntegrationType;
use CrefoPay\Library\Locale\Codes;
use CrefoPay\Library\PaymentMethods\Methods;
use CrefoPay\Library\Request\Capture as CaptureRequest;
use CrefoPay\Library\Request\CreateTransaction as CreateTransactionRequest;
use CrefoPay\Library\Request\Objects\Address;
use CrefoPay\Library\Request\Objects\Amount;
use CrefoPay\Library\Request\Objects\Attributes\File;
use CrefoPay\Library\Request\Objects\BasketItem;
use CrefoPay\Library\Request\Objects\Person;
use CrefoPay\Library\Request\Reserve as ReserveRequest;
use CrefoPay\Library\Request\UpdateTransaction as UpdateTransactionRequest;
use CrefoPay\Library\Risk\RiskClass;
use CrefoPay\Library\User\Type;


/**
 * @group Endtoend
 */
class UpdateTransactionTest extends AbstractEndtoendTest
{
    private function getUser()
    {
        $date = new DateTime();
        $date->setDate(1980, 1, 1);

        $user = new Person();
        $user->setSalutation(PERSON::SALUTATIONMALE)
            ->setName($this->faker->name)
            ->setSurname($this->faker->name)
            ->setDateOfBirth($date)
            ->setEmail($this->email)
            ->setPhoneNumber('03452696645')
            ->setFaxNumber('03452696645');

        return $user;
    }

    private function getAddress()
    {
        $address = new Address();
        $address->setStreet("Test")
            ->setNo(45)
            ->setZip("LS1 4TN")
            ->setCity("City")
            ->setState("State")
            ->setCountry("GB");

        return $address;
    }

    private function getAmount()
    {
        return new Amount(101, 0, 0);
    }

    private function getBasketItem()
    {
        $item = new BasketItem();
        $item->setBasketItemText("Test Item CALL")
            ->setBasketItemCount(1)
            ->setBasketItemAmount($this->getAmount());

        return $item;
    }

    private function getPDF()
    {
        $path = realpath(dirname(__FILE__));
        $pdf = $path . "/UpdateTransactionTest.pdf";

        $file = new File();
        $file->setPath($pdf);

        return $file;
    }

    /**
     * Make an successful call
     * Create a transaction then do the reserve call
     */
    public function testSuccessfulApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());
        $captureId = hash('crc32b', microtime());
        $userId = "GUEST:" . hash('md5', microtime());

        $request->setOrderID($orderId)
            ->setUserID($userId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_API)
            ->setAutoCapture(false)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setMerchantReference("TEST")
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $createTransactionResult = $apiEndPoint->sendRequest();

        $this->assertEquals(0, $createTransactionResult->getData('resultCode'));

        $allowedPayments = $createTransactionResult->getData('allowedPaymentMethods');

        $this->assertTrue(in_array('BILL', $createTransactionResult->getData('allowedPaymentMethods')));

        if (!in_array('BILL', $allowedPayments)) {
            $this->fail("Allowed payment from CreateTransaction lacks BILL can not continue");
        }

        $reserveRequest = new ReserveRequest($this->config);

        $reserveRequest->setOrderID($orderId)
            ->setPaymentMethod(Methods::PAYMENT_METHOD_TYPE_BILL);

        $reserveApi = new ReserveApi($this->config, $reserveRequest);
        $reserveApi->sendRequest();

        /**
         * Do the capture
         */
        $captureRequest = new CaptureRequest($this->config);
        $captureRequest->setOrderID($orderId)
            ->setCaptureID($captureId)
            ->setAmount($this->getAmount());

        $captureApi = new CaptureApi($this->config, $captureRequest);
        $captureApi->sendRequest();

        /**
         * Do the updateTransaction test
         */
        $updateTransactionRequest = new UpdateTransactionRequest($this->config);
        $updateTransactionRequest->setOrderID($orderId)
            ->setCaptureID($captureId)
            ->setInvoiceNumber($orderId . ':1')
            ->setInvoiceDate(new DateTime())
            ->setOriginalInvoiceAmount($this->getAmount())
            ->setDueDate(new DateTime())
            ->setPaymentTarget(new DateTime())
            ->setInvoicePDF($this->getPDF())
            ->setShippingDate(new DateTime())
            ->setTrackingID("111222")
            ->setRemark('a test string');

        $updateTransactionApi = new UpdateTransactionApi($this->config, $updateTransactionRequest);

        $result = $updateTransactionApi->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('salt'));

        return true;
    }
}
