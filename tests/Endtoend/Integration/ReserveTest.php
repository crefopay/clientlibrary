<?php

namespace CrefoPay\Library\Tests\Endtoend\Integration;

use DateInterval;
use DateTime;
use CrefoPay\Library\Api\CreateTransaction;
use CrefoPay\Library\Api\RegisterUserPaymentInstrument as RegisterUserPaymentInstrumentApi;
use CrefoPay\Library\Api\Reserve as ReserveApi;
use CrefoPay\Library\Integration\Type as IntegrationType;
use CrefoPay\Library\Locale\Codes;
use CrefoPay\Library\PaymentMethods\Methods;
use CrefoPay\Library\Request\CreateTransaction as CreateTransactionRequest;
use CrefoPay\Library\Request\Objects\Address;
use CrefoPay\Library\Request\Objects\Amount;
use CrefoPay\Library\Request\Objects\BasketItem;
use CrefoPay\Library\Request\Objects\PaymentInstrument as PaymentInstrumentJson;
use CrefoPay\Library\Request\Objects\Person;
use CrefoPay\Library\Request\RegisterUserPaymentInstrument as RegisterUserPaymentInstrumentRequest;
use CrefoPay\Library\Request\Reserve as ReserveRequest;
use CrefoPay\Library\Risk\RiskClass;
use CrefoPay\Library\User\Type;

/**
 * @group Endtoend
 */
class ReserveTest extends AbstractEndtoendTest
{
    private $paymentInstrument;

    private function getUser()
    {
        $date = new DateTime();
        $date->setDate(1980, 1, 1);

        $user = new Person();
        $user->setSalutation(PERSON::SALUTATIONMALE)
            ->setName($this->faker->name)
            ->setSurname($this->faker->name)
            ->setDateOfBirth($date)
            ->setEmail($this->email)
            ->setPhoneNumber('03452696645')
            ->setFaxNumber('03452696645');

        return $user;
    }

    private function getAddress()
    {
        $address = new Address();
        $address->setStreet("Test")
            ->setNo(45)
            ->setZip("LS1 4TN")
            ->setCity("City")
            ->setState("State")
            ->setCountry("GB");

        return $address;
    }

    private function getPaymentInstrument()
    {
        if (is_null($this->paymentInstrument)) {
            list($month, $year) = explode('/', $this->faker->creditCardExpirationDateString);
            $year = '20' . $year;

            $date = new DateTime();
            $date->setDate($year, $month, 1);
            $date->add(new DateInterval("P1Y"));

            $this->paymentInstrument = new PaymentInstrumentJson();
            $this->paymentInstrument->setPaymentInstrumentType(PaymentInstrumentJson::PAYMENT_INSTRUMENT_TYPE_CARD)
                ->setAccountHolder($this->faker->name)
                ->setIssuer(PaymentInstrumentJson::ISSUER_VISA)
                ->setValidity($date)
                ->setNumber('4539272776120245');
        }

        return $this->paymentInstrument;
    }

    private function getAmount()
    {
        return new Amount(101, 0, 0);
    }

    private function getBasketItem()
    {
        $item = new BasketItem();
        $item->setBasketItemText("Test Item CALL")
            ->setBasketItemCount(1)
            ->setBasketItemAmount($this->getAmount());

        return $item;
    }

    /**
     * Make an successful call
     * Create a transaction then do the reserve call
     */
    public function testSuccessfulApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());
        $userId = "GUEST:" . hash('md5', microtime());

        $request->setOrderID($orderId)
            ->setUserID($userId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_API)
            ->setAutoCapture(true)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setMerchantReference("TEST")
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $createTransactionResult = $apiEndPoint->sendRequest();

        $this->assertEquals(0, $createTransactionResult->getData('resultCode'));

        $allowedPayments = $createTransactionResult->getData('allowedPaymentMethods');

        $this->assertTrue(in_array('CC', $createTransactionResult->getData('allowedPaymentMethods')));

        if (!in_array('CC', $allowedPayments)) {
            $this->fail("Allowed payment from CreateTransaction lacks CC can not continue");
        }

        /**
         * Register payment instrument
         */
        $registerUserPaymentInstrumentRequest = new RegisterUserPaymentInstrumentRequest($this->config);
        $registerUserPaymentInstrumentRequest->setUserID($userId)
            ->setPaymentInstrument($this->getPaymentInstrument());

        $registerUserPaymentInstrument = new RegisterUserPaymentInstrumentApi(
            $this->config,
            $registerUserPaymentInstrumentRequest
        );

        $registerUserPaymentInstrumentResult = $registerUserPaymentInstrument->sendRequest();

        $paymentInstrumentId = $registerUserPaymentInstrumentResult->getData('paymentInstrumentID');

        $reserveRequest = new ReserveRequest($this->config);

        $reserveRequest->setOrderID($orderId)
            ->setPaymentMethod(Methods::PAYMENT_METHOD_TYPE_CC)
            ->setPaymentInstrumentID($paymentInstrumentId)
            ->setCvv(123);

        $reserveApi = new ReserveApi($this->config, $reserveRequest);
        $result = $reserveApi->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('salt'));

    }
}
