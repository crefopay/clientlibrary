<?php

namespace CrefoPay\Library\Tests\Endtoend\Integration;

use DateTime;
use CrefoPay\Library\Api\CreateTransaction;
use CrefoPay\Library\Config;
use CrefoPay\Library\Integration\Type as IntegrationType;
use CrefoPay\Library\Locale\Codes;
use CrefoPay\Library\Request\CreateTransaction as CreateTransactionRequest;
use CrefoPay\Library\Request\Objects\AdditionalPaymentOptions;
use CrefoPay\Library\Request\Objects\Address;
use CrefoPay\Library\Request\Objects\Amount;
use CrefoPay\Library\Request\Objects\BasketItem;
use CrefoPay\Library\Request\Objects\Bonima;
use CrefoPay\Library\Request\Objects\Company;
use CrefoPay\Library\Request\Objects\Creditreform;
use CrefoPay\Library\Request\Objects\Person;
use CrefoPay\Library\Request\Objects\SolvencyCheckInformation;
use CrefoPay\Library\Risk\RiskClass;
use CrefoPay\Library\User\Type;

/**
 * @group Endtoend
 */
class CreateTransactionTest extends AbstractEndtoendTest
{

    private function getUser($minimal = false)
    {
        $user = new Person();
        $user->setSalutation(PERSON::SALUTATIONMALE)
            ->setName($this->faker->name)
            ->setSurname($this->faker->name)
            ->setEmail($this->email);
        if (!$minimal) {
            $date = new DateTime();
            $date->setDate(1980, 1, 1);
            $user->setDateOfBirth($date)
                ->setPhoneNumber('03452696645')
                ->setFaxNumber('03452696645');
        }

        return $user;
    }

    private function getPayByLinkUser()
    {
        $user = new Person();

        return $user->setEmail($this->email);
    }

    private function getPayByLinkCompany()
    {
        $company = new Company();

        return $company->setEmail($this->email);
    }

    private function getAddress($minimal = false)
    {
        $address = new Address();
        $address->setStreet("Test")
            ->setZip("LS1 4TN")
            ->setCity("City")
            ->setCountry("GB");
        if (!$minimal) {
            $address->setAdditional("c/o Mustermann")
                ->setNo(45)
                ->setState("State");
        }

        return $address;
    }

    private function getAmount()
    {
        return new Amount(101, 0, 0);
    }

    private function getBasketItem()
    {
        $item = new BasketItem();
        $item->setBasketItemText("Test Item CALL")
            ->setBasketItemCount(1)
            ->setBasketItemAmount($this->getAmount());

        return $item;
    }

    private function getB2CSolvencyInformation()
    {
        $solvencyInformation = new SolvencyCheckInformation();
        $bonima = new Bonima();
        $bonima->setRequestDate(new DateTime("2017-10-01"))
            ->setIdentification(Bonima::IDENTIFICATION_PERSON_IDENTIFIED)
            ->setAddressValidationStatus(Bonima::ADDRESS_SUCCESSFULLY_VALIDATED)
            ->setTrafficLightColor(Bonima::TRAFFIC_LIGHT_GREEN)
            ->setScore(1500);

        $solvencyInformation->setBonimaResult($bonima);

        return $solvencyInformation;
    }

    private function getB2BSolvencyInformation()
    {
        $solvencyInformation = new SolvencyCheckInformation();
        $crefoCheck = new Creditreform();
        $crefoCheck->setRequestDate(new DateTime("2017-10-01"))
            ->setCrefoProductName(Creditreform::PRODUCT_TRAFFIC_LIGHT_REPORT)
            ->setTrafficLightResult(Creditreform::TRAFFIC_LIGHT_GREEN);

        $solvencyInformation->setCrefoResult($crefoCheck);

        return $solvencyInformation;
    }

    private function getAdditionalPaymentOptions()
    {
        $paymentOptions = new AdditionalPaymentOptions();

        $paymentOptions->setRecurringCustomer(true)
            ->setRecurringTransactionType(AdditionalPaymentOptions::RECURRING_TYPE_FIRST);

        return $paymentOptions;
    }

    /**
     * Make a successful API call
     *
     * @group API
     */
    public function testSuccessfulApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());
        $userId = "GUEST:" . hash('md5', microtime());

        $request->setOrderID($orderId)
            ->setUserID($userId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_API)
            ->setAutoCapture(true)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setMerchantReference("TEST")
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN)
            ->setSolvencyCheckInformation($this->getB2CSolvencyInformation())
            ->setAdditionalPaymentOptions($this->getAdditionalPaymentOptions());

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $result = $apiEndPoint->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertGreaterThan(1, count($result->getData('allowedPaymentMethods')));
    }

    /**
     * Make a successful SecureFields call
     *
     * @group SecureFields
     */
    public function testSuccessfulSecureFieldsCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());
        $userId = "GUEST:" . hash('md5', microtime());

        $request->setOrderID($orderId)
            ->setUserID($userId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_SECURE_FIELDS)
            ->setAutoCapture(true)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setMerchantReference("TEST")
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN)
            ->setSolvencyCheckInformation($this->getB2CSolvencyInformation())
            ->setAdditionalPaymentOptions($this->getAdditionalPaymentOptions());

        $apiEndPoint = new CreateTransaction($this->config, $request);
        $result = $apiEndPoint->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertNotEmpty($result->getData('allowedPaymentMethods'));
    }

    /**
     * @group HostedPages
     */
    public function testSuccessfulApiHostedBeforeIntegrationCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());
        $userId = "GUEST:" . hash('md5', microtime());

        $request->setOrderID($orderId)
            ->setUserID($userId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_HOSTED_BEFORE)
            ->setAutoCapture(true)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setMerchantReference("TEST")
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $result = $apiEndPoint->sendRequest();

        $this->assertEquals(1, $result->getData('resultCode'));
        $this->assertNotEmpty($result->getData('redirectUrl'));

        $ch = curl_init($result->getData('redirectUrl'));
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals(200, $httpCode);
    }

    /**
     * @group HostedPages
     */
    public function testSuccessfulApiHostedAfterIntegrationCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());
        $userId = "GUEST:" . hash('md5', microtime());

        $request->setOrderID($orderId)
            ->setUserID($userId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_HOSTED_AFTER)
            ->setAutoCapture(true)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setMerchantReference("TEST")
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $result = $apiEndPoint->sendRequest();

        $this->assertEquals(1, $result->getData('resultCode'));
        $this->assertNotEmpty($result->getData('redirectUrl'));

        $ch = curl_init($result->getData('redirectUrl'));
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals(200, $httpCode);
    }

    /**
     * @group PayByLink
     */
    public function testSuccessfulApiPayByLinkPageCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());

        $request->setOrderID($orderId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_HOSTED_AFTER)
            ->setContext(CreateTransactionRequest::CONTEXT_PAYBYLINK)
            ->setAmount($this->getAmount());

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $result = $apiEndPoint->sendRequest();

        $this->assertEquals(1, $result->getData('resultCode'));
        $this->assertNotEmpty($result->getData('redirectUrl'));

        $ch = curl_init($result->getData('redirectUrl'));
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals(200, $httpCode);
    }

    /**
     * @group PayByLink
     */
    public function testSuccessfulApiPayByLinkEmailCallB2C()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());

        $request->setOrderID($orderId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_HOSTED_AFTER)
            ->setContext(CreateTransactionRequest::CONTEXT_PAYBYLINK)
            ->setSendPayByLinkEmail(true)
            ->setUserData($this->getPayByLinkUser())
            ->setAmount($this->getAmount());


        $apiEndPoint = new CreateTransaction($this->config, $request);

        $result = $apiEndPoint->sendRequest();

        $this->assertEquals(1, $result->getData('resultCode'));
        $this->assertNotEmpty($result->getData('redirectUrl'));

        $ch = curl_init($result->getData('redirectUrl'));
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals(200, $httpCode);
    }

    /**
     * @group PayByLink
     */
    public function testSuccessfulApiPayByLinkEmailCallB2B()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());

        $request->setOrderID($orderId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_HOSTED_AFTER)
            ->setContext(CreateTransactionRequest::CONTEXT_PAYBYLINK)
            ->setSendPayByLinkEmail(true)
            ->setUserType(Type::USER_TYPE_BUSINESS)
            ->setCompanyData($this->getPayByLinkCompany())
            ->setAmount($this->getAmount());


        $apiEndPoint = new CreateTransaction($this->config, $request);

        $result = $apiEndPoint->sendRequest();

        $this->assertEquals(1, $result->getData('resultCode'));
        $this->assertNotEmpty($result->getData('redirectUrl'));

        $ch = curl_init($result->getData('redirectUrl'));
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals(200, $httpCode);
    }

    /**
     * This test should return an API error
     *
     * @group APIFailing
     * @expectedException \CrefoPay\Library\Api\Exception\ApiError
     */
    public function testApiCallThatReturnsAnAPIError()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $configData = $this->config->getConfigData();

        $configData['storeID'] = "WRONGSTOREID";

        $config = new Config($configData);

        $request = new CreateTransactionRequest($config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());
        $userId = "GUEST:" . hash('md5', microtime());

        $request->setOrderID($orderId)
            ->setUserID($userId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_API)
            ->setAutoCapture(true)
            ->setContext(CreateTransactionRequest::CONTEXT_OFFLINE)
            ->setMerchantReference("TEST")
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $apiEndPoint = new CreateTransaction($config, $request);

        $apiEndPoint->sendRequest();
    }

    /**
     * @group HostedPages
     * @group B2B
     */
    public function testSuccessfulB2bTransaction()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());
        $userId = "GUEST:" . hash('md5', microtime());

        $request->setOrderID($orderId)
            ->setUserID($userId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_HOSTED_AFTER)
            ->setAutoCapture(true)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setMerchantReference("TEST")
            ->setUserType(Type::USER_TYPE_BUSINESS)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN)
            ->setSolvencyCheckInformation($this->getB2BSolvencyInformation());

        $company = new Company();
        $company->setCompanyName($this->faker->company)
            ->setCompanyRegisterType(Company::COMPANY_TYPE_FN)
            ->setCompanyVatID(4087620)
            ->setCompanyVatID(4087620);

        $request->setCompanyData($company);

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $result = $apiEndPoint->sendRequest();

        $this->assertEquals(1, $result->getData('resultCode'));
        $this->assertNotEmpty($result->getData('redirectUrl'));

        $ch = curl_init($result->getData('redirectUrl'));
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->assertEquals(200, $httpCode);
    }

    /**
     * Make a successful API call with minimal data
     *
     * @group API
     */
    public function testSuccessfulMinimalApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());
        $userId = "GUEST:" . hash('md5', microtime());

        $request->setOrderID($orderId)
            ->setUserID($userId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_API)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser(true))
            ->setBillingAddress($this->getAddress(true))
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $result = $apiEndPoint->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertGreaterThan(1, count($result->getData('allowedPaymentMethods')));
        /** @var Person $person */
        $person = $result->getData('userData');
        $this->assertEmpty($person->getDateOfBirth());
    }
}
