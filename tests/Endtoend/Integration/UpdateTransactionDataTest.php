<?php

namespace CrefoPay\Library\Tests\Endtoend\Integration;

use DateTime;
use CrefoPay\Library\Api\CreateTransaction;
use CrefoPay\Library\Api\Exception\ApiError;
use CrefoPay\Library\Api\Reserve as ReserveApi;
use CrefoPay\Library\Api\UpdateTransactionData as UpdateTransactionDataApi;
use CrefoPay\Library\Integration\Type as IntegrationType;
use CrefoPay\Library\Locale\Codes;
use CrefoPay\Library\PaymentMethods\Methods;
use CrefoPay\Library\Request\CreateTransaction as CreateTransactionRequest;
use CrefoPay\Library\Request\Objects\Address;
use CrefoPay\Library\Request\Objects\Amount;
use CrefoPay\Library\Request\Objects\BasketItem;
use CrefoPay\Library\Request\Objects\Person;
use CrefoPay\Library\Request\Reserve as ReserveRequest;
use CrefoPay\Library\Request\UpdateTransactionData as UpdateTransactionDataRequest;
use CrefoPay\Library\Risk\RiskClass;
use CrefoPay\Library\User\Type;

/**
 * @group Endtoend
 */
class UpdateTransactionDataTest extends AbstractEndtoendTest
{
    private $user;

    private $oldMerchantReference = "TEST";

    private $newMerchantReference = "TESTCHANGED";

    private function getUser()
    {
        if (is_null($this->user)) {
            $date = new DateTime();
            $date->setDate(1980, 1, 1);

            $this->user = new Person();
            $this->user->setSalutation(PERSON::SALUTATIONMALE)
                ->setName($this->faker->name)
                ->setSurname($this->faker->name)
                ->setDateOfBirth($date)
                ->setEmail(strtolower($this->email))
                ->setPhoneNumber('03452696645')
                ->setFaxNumber('03452696645');
        }

        return $this->user;
    }

    private function getAddress()
    {
        $address = new Address();
        $address->setStreet("Test")
            ->setNo(45)
            ->setZip("12100")
            ->setCity("City")
            ->setState("State")
            ->setCountry("DE");

        return $address;
    }

    private function getAmount()
    {
        return new Amount(101, 0, 0);
    }

    private function getBasketItem()
    {
        $item = new BasketItem();
        $item->setBasketItemText("Test Item CALL")
            ->setBasketItemCount(1)
            ->setBasketItemAmount($this->getAmount());

        return $item;
    }

    private function getUpdatedData()
    {
        return array(
            'merchantReference' => [
                'oldValue' => $this->oldMerchantReference,
                'newValue' => $this->newMerchantReference,
            ],
        );
    }

    /**
     * Make an successful call
     * Create a transaction then do the reserve call
     */
    public function testSuccessfulApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $request = new CreateTransactionRequest($this->config);

        //unique ID for the tests
        $orderId = hash('crc32b', microtime());
        $userId = "GUEST:" . hash('md5', microtime());

        $request->setOrderID($orderId)
            ->setUserID($userId)
            ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_API)
            ->setAutoCapture(false)
            ->setContext(CreateTransactionRequest::CONTEXT_ONLINE)
            ->setMerchantReference($this->oldMerchantReference)
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
            ->setUserData($this->getUser())
            ->setBillingAddress($this->getAddress())
            ->setAmount($this->getAmount())
            ->addBasketItem($this->getBasketItem())
            ->setLocale(Codes::LOCALE_EN);

        $apiEndPoint = new CreateTransaction($this->config, $request);

        $createTransactionResult = $apiEndPoint->sendRequest();

        $this->assertEquals(0, $createTransactionResult->getData('resultCode'));

        $allowedPayments = $createTransactionResult->getData('allowedPaymentMethods');

        $this->assertTrue(in_array('BILL', $createTransactionResult->getData('allowedPaymentMethods')));

        if (!in_array('BILL', $allowedPayments)) {
            $this->fail("Allowed payment from CreateTransaction lacks BILL can not continue");
        }

        $reserveRequest = new ReserveRequest($this->config);

        $reserveRequest->setOrderID($orderId)
            ->setPaymentMethod(Methods::PAYMENT_METHOD_TYPE_BILL);

        $reserveApi = new ReserveApi($this->config, $reserveRequest);
        try {
            $reserveApi->sendRequest();
        } catch (ApiError $ae) {
        }

        $updateTransactionDataRequest = new UpdateTransactionDataRequest($this->config);
        $updateTransactionDataRequest->setOrderID($orderId)
            ->setMerchantReference($this->newMerchantReference);

        // Now test the updateTransactionData call
        $updateTransactionDataApi = new UpdateTransactionDataApi($this->config, $updateTransactionDataRequest);
        $result = $updateTransactionDataApi->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('salt'));
        $this->assertEquals($this->getUpdatedData(), $result->getData('updatedData'));
    }
}
