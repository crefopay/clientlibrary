<?php

namespace CrefoPay\Library\Tests\Endtoend\Integration;

use DateTime;
use CrefoPay\Library\Api\GetClearingFileList as GetClearingFileListApi;
use CrefoPay\Library\Request\GetClearingFileList as GetClearingFileListRequest;

/**
 * @group Endtoend
 */
class GetClearingFileListTest extends AbstractEndtoendTest
{
    /**
     * Make an successful call
     */
    public function testSuccessfulApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $clearingFileListRequest = new GetClearingFileListRequest($this->config);
        $clearingFileListRequest->setFrom(new DateTime('2017-01-01'))
            ->setTo(new DateTime('2017-02-01'));

        $clearingFileList = new GetClearingFileListApi($this->config, $clearingFileListRequest);
        $result = $clearingFileList->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertNotEmpty($result->getData('salt'));
    }

    /**
     * Make a call which receives no clearing file information
     */
    public function testEmptyApiCall()
    {
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $clearingFileListRequest = new GetClearingFileListRequest($this->config);
        $clearingFileListRequest->setFrom(new DateTime('2015-01-01'))
            ->setTo(new DateTime('2015-02-01'));

        $clearingFileList = new GetClearingFileListApi($this->config, $clearingFileListRequest);
        $result = $clearingFileList->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertEmpty($result->getData('message'));
        $this->assertEmpty($result->getData('clearingFiles'));
        $this->assertNotEmpty($result->getData('salt'));
    }
}