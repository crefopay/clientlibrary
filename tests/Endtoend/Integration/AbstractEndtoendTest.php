<?php


namespace CrefoPay\Library\Tests\Endtoend\Integration;


use Faker\Factory;
use Faker\Generator;
use PHPUnit\Framework\TestCase;
use CrefoPay\Library\Config;

/**
 * @group Endtoend
 */
class AbstractEndtoendTest extends TestCase
{
    /**
     * @var Generator
     */
    public $faker;

    /**
     * Config object for tests
     *
     * @var Config
     */
    public $config;

    /**
     * @var string
     */
    public $email;

    public function setUp() : void
    {
        date_default_timezone_set('Europe/Berlin');

        $faker = Factory::create();

        $this->faker = $faker;

        $this->email = getenv('CREFOPAY_TEST_EMAIL') ? trim(getenv('CREFOPAY_TEST_EMAIL')) : 'test@example.com';

        $merchantPassword = trim(getenv('CREFOPAY_TEST_MERCHANT_PASSWORD'));
        $merchantID = trim(getenv('CREFOPAY_TEST_MERCHANT_ID'));
        $storeID = trim(getenv('CREFOPAY_TEST_STORE_ID'));
        $baseURL = trim(getenv('CREFOPAY_TEST_BASE_URL'));
        $logEnabled = boolval(trim(getenv('CREFOPAY_TEST_LOG_ENABLED')));
        $logLocation = trim(getenv('CREFOPAY_TEST_LOG_LOCATION'));

        if (!empty($merchantPassword) && !empty($merchantID) && !empty($storeID) && !empty($baseURL)) {
            $configData = array(
                'merchantPassword' => $merchantPassword,
                'merchantID' => $merchantID,
                'storeID' => $storeID,
                'logEnabled' => $logEnabled,
                'sendRequestsWithSalt' => true,
                'baseUrl' => $baseURL,
            );
            if ($logEnabled) {
                $configData['logLevel'] = Config::LOG_LEVEL_DEBUG;
                $configData['logLocationMain'] = $logLocation;
                $configData['logLocationRequest'] = $logLocation;
            }
            $this->config = new Config($configData);
        } else {
            $this->config = null;
        }
    }

    public function tearDown() : void
    {
        unset($this->faker);
        unset($this->config);
    }

    public function testConfig()
    {
        $this->assertNotNull($this->config);
    }
}