<?php

namespace CrefoPay\Library\Tests\Endtoend\Integration;

use CrefoPay\Library\Api\GetSubscriptionPlans as GetSubscriptionPlansApi;
use CrefoPay\Library\Request\GetSubscriptionPlans as GetSubscriptionPlansRequest;
use CrefoPay\Library\Request\Objects\Amount;
use CrefoPay\Library\Request\Objects\AmountRange;

/**
 * @group Endtoend
 */
class GetSubscriptionPlansTest extends AbstractEndtoendTest
{
    private function getAmountRange()
    {
        return new AmountRange(new Amount(1), new Amount(1000));
    }

    /**
     * Make an successful call
     * Create a transaction then do the reserve call
     */
    public function testSuccessfulApiCall()
    {
        $pageNumber = 1;
        $pageSize = 25;
        if (is_null($this->config)) {
            $this->markTestSkipped('Config is not set, please set up the required environment variables');

            return false;
        }

        $getSubscriptionPlansRequest = new GetSubscriptionPlansRequest($this->config);
        $getSubscriptionPlansRequest->setAmount($this->getAmountRange())
            ->setPageNumber($pageNumber)
            ->setPageSize($pageSize);

        /**
         * Now test the getSubscriptionPlans call
         */
        $getSubscriptionPlansApi = new GetSubscriptionPlansApi($this->config, $getSubscriptionPlansRequest);
        $result = $getSubscriptionPlansApi->sendRequest();

        $this->assertEquals(0, $result->getData('resultCode'));
        $this->assertNotEmpty($result->getData('salt'));
        $this->assertNotEmpty($result->getData('subscriptionPlans'));
        $this->assertNotEmpty($result->getData('totalEntries'));
        $this->assertEquals($pageSize, $result->getData('pageSize'));
        $this->assertEquals($pageNumber, $result->getData('pageNumber'));
    }
}
