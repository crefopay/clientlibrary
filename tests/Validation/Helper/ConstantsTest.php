<?php

namespace CrefoPay\Library\Tests\Validation\Helper;


use PHPUnit\Framework\TestCase;
use CrefoPay\Library\Tests\Mock\Request\CustomValidationRequest;
use CrefoPay\Library\Validation\Helper\Constants;

class ConstantsTest extends TestCase
{

    public function testValidateConstant()
    {
        $request = new CustomValidationRequest();
        $className = get_class($request);
        $className = "\\" . $className;
        /**
         * Test successful validation
         */
        $result = Constants::validateConstant($className, 1, 'CUSTOM_TEST');
        $this->assertTrue($result, "Constant validation did not work");

        /**
         * Test unsuccessful validation
         */
        $result = Constants::validateConstant($className, 10, 'CUSTOM_TEST');
        $this->assertFalse($result, "Constant validation did not work");

        /**
         * Test unsuccessful validation
         */
        $result = Constants::validateConstant($className, "10", 'CUSTOM_TEST');
        $this->assertFalse($result, "Constant validation did not work");


    }

}
