<?php

namespace CrefoPay\Library\Tests\Request;

use Faker\Factory;
use Faker\Generator;
use CrefoPay\Library\Config;
use CrefoPay\Library\Request\AddPayment;
use CrefoPay\Library\Request\Objects\Amount;
use CrefoPay\Library\Validation\Validation;

class AddPaymentTest extends AbstractRequestTest
{
    /**
     * @var string A very long string
     */
    private $veryLongString;

    /**
     * @var Generator
     */
    private $faker;

    /**
     * Config object for tests
     * @var Config
     */
    private $config;

    public function setUp() : void
    {
        $faker = Factory::create();

        $this->veryLongString = preg_replace("/[^A-Za-z0-9]/", '', $faker->sentence(90));
        $this->faker = $faker;

        $this->config = new Config(array(
            'merchantPassword' => '8A!v#6qPc3?+G1on',
            'merchantID' => '123',
            'storeID' => 'test Store',
            'sendRequestsWithSalt' => true,
        ));
    }

    public function tearDown() : void
    {
        unset($this->faker);
        unset($this->config);
    }

    private function getAmount()
    {
        return new Amount(100, 0, 0);
    }

    public function testAddPaymentValidationSuccess()
    {
        $request = new AddPayment($this->config);
        $request->setOrderID(1)
            ->setCaptureID(1)
            ->setAmount($this->getAmount()->getAmount())
            ->setValutaDate(new \DateTimeImmutable())
            ->setIban('IBAN')
            ->setAccountHolder('HOLDER')
            ->setMessage('PAYMENT');

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testAddPaymentValidationOrderID()
    {
        $request = new AddPayment($this->config);
        $request->setCaptureID(1)
            ->setAmount($this->getAmount()->getAmount())
            ->setValutaDate(new \DateTimeImmutable());

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        /**
         * Test the orderId required validation
         */
        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\AddPayment',
            'orderID',
            'orderID is required',
            $data,
            "orderID is required failed to trigger"
        );

        /**
         * Test length
         */
        $request->setOrderID($this->veryLongString);
        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\AddPayment',
            'orderID',
            'orderID must be between 1 and 30 characters',
            $data,
            "orderID must be between 1 and 30 characters failed to trigger"
        );
    }

    public function testAddPaymentValidationCaptureID()
    {
        $request = new AddPayment($this->config);
        $request->setOrderID(1)
            ->setAmount($this->getAmount()->getAmount())
            ->setValutaDate(new \DateTimeImmutable());

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        /**
         * Test the captureID required validation
         */
        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\AddPayment',
            'captureID',
            'captureID is required',
            $data,
            "captureID is required failed to trigger"
        );

        /**
         * Test length
         */
        $request->setCaptureID($this->veryLongString);
        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\AddPayment',
            'captureID',
            'captureID must be between 1 and 30 characters',
            $data,
            "captureID must be between 1 and 30 characters failed to trigger"
        );
    }

    public function testAddPaymentValidationAmount()
    {
        $request = new AddPayment($this->config);
        $request->setOrderID(1)
            ->setCaptureID(1)
            ->setValutaDate(new \DateTimeImmutable());

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\AddPayment',
            'amount',
            'amount is required',
            $data,
            "amount is required failed to trigger"
        );

        /**
         * Test length
         */
        $request->setAmount(10000000000000000);
        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\AddPayment',
            'amount',
            'amount must be at maximum 16 digits',
            $data,
            "amount length validation failed to trigger"
        );
    }

    public function testAddPaymentValidationValutaDate()
    {
        $request = new AddPayment($this->config);
        $request->setOrderID(1)
            ->setCaptureID(1)
            ->setAmount($this->getAmount()->getAmount());

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\AddPayment',
            'valutaDate',
            'valutaDate is required',
            $data,
            "valutaDate is required failed to trigger"
        );
    }

    public function testAddPaymentValidationIban()
    {
        $request = new AddPayment($this->config);
        $request->setOrderID(1)
            ->setCaptureID(1)
            ->setAmount($this->getAmount()->getAmount())
            ->setValutaDate(new \DateTimeImmutable())
            ->setIban($this->veryLongString);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\AddPayment',
            'iban',
            'iban must be at maximum 34 characters',
            $data,
            "iban max length validation failed to trigger"
        );
    }

    public function testAddPaymentValidationAccountHolder()
    {
        $request = new AddPayment($this->config);
        $request->setOrderID(1)
            ->setCaptureID(1)
            ->setAmount($this->getAmount()->getAmount())
            ->setValutaDate(new \DateTimeImmutable())
            ->setAccountHolder($this->veryLongString);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\AddPayment',
            'accountHolder',
            'accountHolder must be at maximum 50 characters',
            $data,
            "accountHolder max length validation failed to trigger"
        );
    }

    public function testAddPaymentValidationMessage()
    {
        $request = new AddPayment($this->config);
        $request->setOrderID(1)
            ->setCaptureID(1)
            ->setAmount($this->getAmount()->getAmount())
            ->setValutaDate(new \DateTimeImmutable())
            ->setMessage($this->veryLongString);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\AddPayment',
            'message',
            'message must be at maximum 255 characters',
            $data,
            "message max length validation failed to trigger"
        );
    }
}
