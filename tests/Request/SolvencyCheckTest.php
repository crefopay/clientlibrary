<?php

namespace CrefoPay\Library\Tests\Request;

use DateTime;
use Faker\Factory;
use Faker\Generator;
use CrefoPay\Library\Config;
use CrefoPay\Library\Request\Objects\Address;
use CrefoPay\Library\Request\Objects\Person;
use CrefoPay\Library\Request\SolvencyCheck;
use CrefoPay\Library\User\Type;
use CrefoPay\Library\Validation\Validation;

class SolvencyCheckTest extends AbstractRequestTest
{
    /**
     * @var string A very long string
     */
    private $veryLongString;

    /**
     * @var Generator
     */
    private $faker;

    /**
     * Config object for tests
     * @var Config
     */
    private $config;

    public function setUp() : void
    {
        date_default_timezone_set('Europe/Berlin');

        $faker = Factory::create();

        $this->veryLongString = preg_replace("/[^A-Za-z0-9]/", '', $faker->sentence(90));
        $this->faker = $faker;

        $this->config = new Config(array(
            'merchantPassword' => '8A!v#6qPc3?+G1on',
            'merchantID' => '123',
            'storeID' => 'test Store',
            'sendRequestsWithSalt' => true,
        ));
    }

    public function tearDown() : void
    {
        unset($this->faker);
        unset($this->config);
    }

    /**
     * Get the person
     * @return Person
     */
    private function getUser()
    {
        $user = new Person();
        $user->setSalutation(PERSON::SALUTATIONMALE)
            ->setName($this->faker->name)
            ->setSurname($this->faker->name)
            ->setDateOfBirth(new DateTime())
            ->setEmail($this->faker->email)
            ->setPhoneNumber('066555666')
            ->setFaxNumber('066555454');

        return $user;
    }

    private function getAddress()
    {
        $address = new Address();
        $address->setStreet("Test")
            ->setNo(45)
            ->setZip("LS12 4TN")
            ->setCity("City")
            ->setState("State")
            ->setCountry("GB");

        return $address;
    }

    public function testRegisterUserValidationSuccess()
    {
        $request = new SolvencyCheck($this->config);
        $request->setUserId(11)
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserData($this->getUser())
            ->setBillingRecipient($this->faker->name)
            ->setBillingAddress($this->getAddress());

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testSolvencyCheckValidationUserID()
    {
        $request = new SolvencyCheck($this->config);
        $request->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserData($this->getUser())
            ->setBillingRecipient($this->faker->name)
            ->setBillingAddress($this->getAddress());

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        /**
         * Test Required
         */
        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\SolvencyCheck',
            'userID',
            'userID is required',
            $data,
            "userID is required validation failed"
        );

        /**
         * Test length validation
         */
        $request->setUserID($this->veryLongString);
        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\SolvencyCheck',
            'userID',
            'userID must be between 1 and 50 characters',
            $data,
            "userID must be between 1 and 50 characters failed"
        );
    }

    public function testSolvencyCheckValidationUserType()
    {
        $request = new SolvencyCheck($this->config);
        $request->setUserID(1)
            ->setUserData($this->getUser())
            ->setBillingRecipient($this->faker->name)
            ->setBillingAddress($this->getAddress());

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        /**
         * Test Required
         */
        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\SolvencyCheck',
            'userType',
            'userType is required',
            $data,
            "userType is required validation failed"
        );

        /**
         * Test length validation
         */
        $request->setUserType($this->veryLongString);
        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\SolvencyCheck',
            'userType',
            'userType must be certain values',
            $data,
            "userType must be certain values failed"
        );
    }

    public function testSolvencyCheckValidationBillingRecipient()
    {
        $request = new SolvencyCheck($this->config);
        $request->setUserId(11)
            ->setUserType(Type::USER_TYPE_PRIVATE)
            ->setUserData($this->getUser())
            ->setBillingRecipient($this->veryLongString)
            ->setBillingAddress($this->getAddress());

        $validation = new Validation();
        $validation->getValidator($request);
        $data = $validation->performValidation();

        /**
         * Test length validation
         */
        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\SolvencyCheck',
            'billingRecipient',
            'billingRecipient must be between 1 and 80 characters',
            $data,
            "billingRecipient must be between 1 and 80 characters failed"
        );
    }
}
