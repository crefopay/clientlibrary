<?php

namespace CrefoPay\Library\Tests\Request;

use Faker\Factory;
use Faker\Generator;
use CrefoPay\Library\Config;
use CrefoPay\Library\Request\UpdateTransactionData;
use CrefoPay\Library\Validation\Validation;

class UpdateTransactionDataTest extends AbstractRequestTest
{
    /**
     * @var string A very long string
     */
    private $veryLongString;

    /**
     * @var Generator
     */
    private $faker;

    /**
     * Config object for tests
     *
     * @var Config
     */
    private $config;

    public function setUp() : void
    {
        $faker = Factory::create();

        $this->veryLongString = preg_replace("/[^A-Za-z0-9]/", '', $faker->sentence(90));
        $this->faker = $faker;

        $this->config = new Config(array(
            'merchantPassword' => '8A!v#6qPc3?+G1on',
            'merchantID' => '123',
            'storeID' => 'test Store',
            'sendRequestsWithSalt' => true,
        ));
    }

    public function tearDown() : void
    {
        unset($this->faker);
        unset($this->config);
    }

    public function testUpdateTransactionDataValidationSuccess()
    {
        $request = new UpdateTransactionData($this->config);
        $request->setOrderID(1);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testUpdateTransactionDataValidationOrderID()
    {
        $request = new UpdateTransactionData($this->config);

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\UpdateTransactionData',
            'orderID',
            'orderID is required',
            $data,
            "orderID is required failed to trigger"
        );
    }

    public function testUpdateTransactionDataMerchantReferenceSuccess()
    {
        $request = new UpdateTransactionData($this->config);
        $request->setOrderID(1)
            ->setMerchantReference("test reference");

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testUpdateTransactionDataValidationMerchantReference()
    {
        $request = new UpdateTransactionData($this->config);
        $request->setOrderID(1)
            ->setMerchantReference('aaaaabbbbbcccccdddddeeeeefffffg');

        $validation = new Validation();

        $validation->getValidator($request);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\UpdateTransactionData',
            'merchantReference',
            'merchantReference must be between 1 and 30 characters',
            $data,
            "merchantReference validation failed to trigger"
        );
    }
}