<?php

namespace CrefoPay\Library\Request\Helper;

use PHPUnit\Framework\TestCase;

class PhoneNumberTest extends TestCase
{

    public function testValidatePhoneNumber()
    {
        $path = realpath(dirname(__FILE__));
        $json = file_get_contents($path . '/phonenumbers.json');
        $phoneNumbers = json_decode($json);
        foreach ($phoneNumbers as $number => $expected) {
            $this->assertEquals($expected, PhoneNumber::validatePhoneNumber($number),
                "Validation of $number did not return the expected value");
        }
    }

    public function testValidateFaxNumber()
    {
        $path = realpath(dirname(__FILE__));
        $json = file_get_contents($path . '/faxnumbers.json');
        $phoneNumbers = json_decode($json);
        foreach ($phoneNumbers as $number => $expected) {
            $this->assertEquals($expected, PhoneNumber::validateFaxNumber($number),
                "Validation of $number did not return the expected value");
        }
    }
}
