<?php

namespace CrefoPay\Library\Tests\Request\Objects;

use DateTime;
use Faker\Factory as Factory;
use Faker\Generator;
use CrefoPay\Library\Request\Objects\Crediconnect;
use CrefoPay\Library\Tests\Request\AbstractRequestTest;
use CrefoPay\Library\Validation\Validation;

class CrediconnectTest extends AbstractRequestTest
{
    /**
     * @var string A very long string
     */
    private $veryLongString;

    /**
     * @var Generator
     */
    private $faker;

    public function setUp() : void
    {
        $faker = Factory::create();


        $this->veryLongString = preg_replace("/[^A-Za-z0-9]/", '', $faker->sentence(90));
        $this->faker = $faker;
    }

    public function tearDown() : void
    {
        unset($this->faker);
    }

    public function testRequestDateFailure()
    {
        $crediconnect = new Crediconnect();
        $crediconnect->setTrafficLightResult(Crediconnect::TRAFFIC_LIGHT_YELLOW);

        $validation = new Validation();
        $validation->getValidator($crediconnect);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Crediconnect',
            'requestDate',
            'RequestDate is required',
            $data,
            "RequestDate requirement did not trigger"
        );
    }

    public function testTrafficLightResultFailure()
    {
        $crediconnect = new Crediconnect();
        $crediconnect->setRequestDate(new DateTime());

        $validation = new Validation();
        $validation->getValidator($crediconnect);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Crediconnect',
            'trafficLightResult',
            'TrafficLightResult is required',
            $data,
            "TrafficLightResult requirement did not trigger"
        );

        $crediconnect->setTrafficLightResult("BONIVERSUM");
        $validation->getValidator($crediconnect);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Crediconnect',
            'trafficLightResult',
            'TrafficLightResult must have a valid value',
            $data,
            "TrafficLightResult constant validation did not trigger"
        );
    }

    public function testCompleteObject()
    {
        $crediconnect = new Crediconnect();
        $crediconnect->setRequestDate(new DateTime())
            ->setTrafficLightResult(Crediconnect::TRAFFIC_LIGHT_YELLOW);

        $validation = new Validation();
        $validation->getValidator($crediconnect);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }
}
