<?php

namespace CrefoPay\Library\Tests\Request\Objects;

use DateTime;
use Faker\Factory as Factory;
use Faker\Generator;
use CrefoPay\Library\Request\Objects\Creditreform;
use CrefoPay\Library\Tests\Request\AbstractRequestTest;
use CrefoPay\Library\Validation\Validation;

class CreditreformTest extends AbstractRequestTest
{
    /**
     * @var string A very long string
     */
    private $veryLongString;

    /**
     * @var Generator
     */
    private $faker;

    public function setUp() : void
    {
        $faker = Factory::create();


        $this->veryLongString = preg_replace("/[^A-Za-z0-9]/", '', $faker->sentence(90));
        $this->faker = $faker;
    }

    public function tearDown() : void
    {
        unset($this->faker);
    }

    public function testRequestDate()
    {
        $crefoResult = new Creditreform();
        $crefoResult->setCrefoProductName(Creditreform::PRODUCT_E_CREFO)
            ->setBlackWhiteResult(true)
            ->setTrafficLightResult(Creditreform::TRAFFIC_LIGHT_YELLOW);

        $validation = new Validation();
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Creditreform',
            'requestDate',
            'RequestDate is required',
            $data,
            "RequestDate requirement did not trigger"
        );
    }

    public function testCrefoProductName()
    {
        $crefoResult = new Creditreform();
        $crefoResult->setRequestDate(new DateTime())
            ->setBlackWhiteResult(true)
            ->setTrafficLightResult(Creditreform::TRAFFIC_LIGHT_NONE);

        $validation = new Validation();
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Creditreform',
            'crefoProductName',
            'CrefoProductName is required',
            $data,
            "CrefoProductName requirement did not trigger"
        );

        $crefoResult->setCrefoProductName("BONIVERSUM");
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Creditreform',
            'crefoProductName',
            'CrefoProductName must have a valid value',
            $data,
            "CrefoProductName constant validation did not trigger"
        );
    }

    public function testBlackWhiteResult()
    {
        $crefoResult = new Creditreform();
        $crefoResult->setRequestDate(new DateTime())
            ->setCrefoProductName(Creditreform::PRODUCT_RISK_CHECK)
            ->setBlackWhiteResult("TEST123")
            ->setTrafficLightResult(Creditreform::TRAFFIC_LIGHT_RED);

        $validation = new Validation();
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Creditreform',
            'blackWhiteResult',
            'BlackWhiteResult is not a valid value',
            $data,
            "BlackWhiteResult boolean check did not trigger"
        );
    }

    public function testTrafficLightResult()
    {
        $crefoResult = new Creditreform();
        $crefoResult->setRequestDate(new DateTime())
            ->setCrefoProductName(Creditreform::PRODUCT_BRIEF_REPORT)
            ->setBlackWhiteResult(true);

        $validation = new Validation();
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Creditreform',
            'trafficLightResult',
            'TrafficLightResult is required',
            $data,
            "TrafficLightResult requirement did not trigger"
        );

        $crefoResult->setTrafficLightResult("BONIVERSUM");
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Creditreform',
            'trafficLightResult',
            'TrafficLightResult must have a valid value',
            $data,
            "TrafficLightResult constant validation did not trigger"
        );
    }

    public function testCompactScore()
    {
        $crefoResult = new Creditreform();
        $crefoResult->setRequestDate(new DateTime())
            ->setCrefoProductName(Creditreform::PRODUCT_COMPACT_REPORT);

        $validation = new Validation();
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Creditreform',
            'compactScore',
            'CompactScore is required',
            $data,
            "CompactScore requirement did not trigger"
        );

        $crefoResult->setCompactScore('diner');
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Creditreform',
            'compactScore',
            'CompactScore must be an integer',
            $data,
            "CompactScore integer check did not trigger"
        );

        $crefoResult->setCompactScore(100000);
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Creditreform',
            'compactScore',
            'CompactScore must be between 1 and 600',
            $data,
            "CompactScore between check did not trigger"
        );

        $score = 150;
        $crefoResult->setCompactScore($score);
        $this->assertEquals($score, $crefoResult->getCompactScore());
    }

    public function testDateOfFoundation()
    {
        $futureDate=new DateTime();
        $futureDate->add(new \DateInterval('P2D')); // two days in the future

        $crefoResult = new Creditreform();
        $crefoResult->setRequestDate(new DateTime())
            ->setCrefoProductName(Creditreform::PRODUCT_COMPACT_REPORT)
            ->setCompactScore(300)
            ->setDateOfFoundation($futureDate);

        $validation = new Validation();
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Creditreform',
            'dateOfFoundation',
            'DateOfFoundation must be a date in the past',
            $data,
            "'DateOfFoundation must be a date in the past' did not trigger"
        );

        $foundationDate = new DateTime('2010-01-01');
        $crefoResult->setDateOfFoundation($foundationDate);
        $this->assertEquals($foundationDate, $crefoResult->getDateOfFoundation());
    }

    public function testLegalForm()
    {
        $legalForm = "LEFO-LU-6";
        $crefoResult = new Creditreform();
        $crefoResult->setRequestDate(new DateTime())
            ->setCrefoProductName(Creditreform::PRODUCT_COMPACT_REPORT)
            ->setCompactScore(300)
            ->setLegalForm($legalForm);
        $this->assertEquals($legalForm, $crefoResult->getLegalForm());
    }

    public function testLineOfBusiness()
    {
        $wz2008Code = "08.92.0";
        $crefoResult = new Creditreform();
        $crefoResult->setRequestDate(new DateTime())
            ->setCrefoProductName(Creditreform::PRODUCT_COMPACT_REPORT)
            ->setCompactScore(300)
            ->setLineOfBusiness($wz2008Code);
        $this->assertEquals($wz2008Code, $crefoResult->getLineOfBusiness());
    }

    public function testCompanyEmail()
    {
        $crefoResult = new Creditreform();
        $crefoResult->setRequestDate(new DateTime())
            ->setCrefoProductName(Creditreform::PRODUCT_COMPACT_REPORT)
            ->setCompactScore(300)
            ->setCompanyEmail('info@');

        $validation = new Validation();
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Creditreform',
            'companyEmail',
            'CompanyEmail must be a valid email',
            $data,
            "CompanyEmail validation did not trigger"
        );

        $companyEmail = 'info@example.com';
        $crefoResult->setCompanyEmail($companyEmail);
        $this->assertEquals($companyEmail, $crefoResult->getCompanyEmail());
    }

    public function testCompleteObject()
    {
        $crefoResult = new Creditreform();
        $crefoResult->setRequestDate(new DateTime())
            ->setCrefoProductName(Creditreform::PRODUCT_IDENTIFICATION)
            ->setBlackWhiteResult(true)
            ->setTrafficLightResult(Creditreform::TRAFFIC_LIGHT_YELLOW);

        $validation = new Validation();
        $validation->getValidator($crefoResult);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }
}
