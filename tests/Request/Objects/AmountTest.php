<?php

namespace CrefoPay\Library\Tests\Request\Objects;

use PHPUnit\Framework\TestCase;
use CrefoPay\Library\Request\Objects\Amount as Amount;
use CrefoPay\Library\Validation\Validation;

class AmountTest extends TestCase
{

    public function testAmountTestValidationSuccess()
    {
        $amount = new Amount();
        $amount->setAmount(9200)->setNetAmount(1840);

        $validation = new Validation();
        $validation->getValidator($amount);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testAmountTestValidationAmountFailure()
    {
        $amount = new Amount();

        $validation = new Validation();

        /** test the invalid amount logic */
        $amount->setAmount(99999999999999999);
        $validation->getValidator($amount);
        $data = $validation->performValidation();

        $expected = array(
            'CrefoPay\\Library\\Request\\Objects\\Amount' =>
                array(
                    'amount' =>
                        array(
                            0 => 'Amount must be between 1 and 16 digits',
                        ),
                ),
        );

        $this->assertEquals($expected, $data, 'Validation not triggered the amount invalid when more than 16 digits');

        /** Test the amount to a float */
        $amount->setAmount(99.99);
        $validation->getValidator($amount);
        $data = $validation->performValidation();

        $expected = array(
            'CrefoPay\\Library\\Request\\Objects\\Amount' =>
                array(
                    'amount' =>
                        array(
                            0 => 'Amount must be an integer',
                        ),
                ),
        );

        $this->assertEquals($expected, $data, 'Validation not triggered the amount invalid when a float');

    }

    public function testAmountTestValidationNetAmountFailure()
    {
        /** Test length validation */
        $amount = new Amount();
        $amount->setAmount(99);
        $amount->setNetAmount(99999999999999999);

        $validation = new Validation();
        $validation->getValidator($amount);
        $data = $validation->performValidation();

        $expected = array(
            'CrefoPay\\Library\\Request\\Objects\\Amount' =>
                array(
                    'netAmount' =>
                        array(
                            0 => 'NetAmount must be between 1 and 16 digits',
                        ),
                ),
        );

        $this->assertEquals(
            $expected,
            $data,
            'Validation not triggered the netAmount invalid when more than 16 digits'
        );

        $amount->setNetAmount(99.99);
        $validation->getValidator($amount);
        $data = $validation->performValidation();

        $expected = array(
            'CrefoPay\\Library\\Request\\Objects\\Amount' =>
                array(
                    'netAmount' =>
                        array(
                            0 => 'NetAmount must be an integer',
                            1 => 'NetAmount must be between 1 and 16 digits',
                        ),
                ),
        );

        $this->assertEquals($expected, $data, 'Integer validation not triggered the netAmount');

    }
}
