<?php

namespace CrefoPay\Library\Tests\Request\Objects;

use Faker\Factory;
use Faker\Generator;
use CrefoPay\Library\Request\Objects\AdditionalPaymentOptions;
use CrefoPay\Library\Tests\Request\AbstractRequestTest;
use CrefoPay\Library\Validation\Validation;

class AdditionalPaymentOptionsTest extends AbstractRequestTest
{
    /**
     * @var string A very long string
     */
    private $veryLongString;

    /**
     * @var Generator
     */
    private $faker;

    public function setUp() : void
    {
        $faker = Factory::create();

        $this->veryLongString = preg_replace("/[^A-Za-z0-9]/", '', $faker->sentence(90));
        $this->faker = $faker;
    }

    public function tearDown() : void
    {
        unset($this->faker);
    }

    public function testAdditionalPaymentOptionsValidationSuccess()
    {
        /**
         * Test Addition Payment Options success
         */
        $additionalPaymentOptions = new AdditionalPaymentOptions();
        $additionalPaymentOptions->setRecurringCustomer(true)
            ->setRecurringTransactionType(AdditionalPaymentOptions::RECURRING_TYPE_FIRST);

        $validation = new Validation();
        $validation->getValidator($additionalPaymentOptions);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testAdditionalPaymentOptionsTransactionTypeValidation()
    {
        /**
         * Test Addition Payment Options failure
         */
        $additionalPaymentOptions = new AdditionalPaymentOptions();
        $additionalPaymentOptions->setRecurringCustomer(true)
            ->setRecurringTransactionType('UNKNOWN');

        $validation = new Validation();
        $validation->getValidator($additionalPaymentOptions);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\AdditionalPaymentOptions',
            'recurringTransactionType',
            'recurringTransactionType must be one of the defined constants',
            $data,
            "recurringTransactionType constant validation did not trigger"
        );
    }

    public function testGetValidationData()
    {
        $additionalPaymentOptions = new AdditionalPaymentOptions();
        $additionalPaymentOptions->setRecurringCustomer(true)
            ->setRecurringTransactionType(AdditionalPaymentOptions::RECURRING_TYPE_FIRST);
        $expectedValidationData = array(
            "recurringTransactionType" => array(
                array(
                    'name' => 'Callback',
                    'value' => 'CrefoPay\Library\Request\Objects\AdditionalPaymentOptions::validateRecurringTransactionType',
                    'message' => "recurringTransactionType must be one of the defined constants",
                ),
            ),
        );
        $validationData = $additionalPaymentOptions->getValidationData();
        $this->assertEquals($expectedValidationData, $validationData);
    }

    public function testToArray()
    {
        $additionalPaymentOptions = new AdditionalPaymentOptions();
        $arrayRepresentation = $additionalPaymentOptions->toArray();
        $this->assertEquals(array(), $arrayRepresentation);

        $additionalPaymentOptions->setRecurringCustomer(true);
        $arrayRepresentation = $additionalPaymentOptions->toArray();
        $this->assertEquals(array('recurringCustomer' => true), $arrayRepresentation);

        $additionalPaymentOptions->setRecurringCustomer(true)
            ->setRecurringTransactionType(AdditionalPaymentOptions::RECURRING_TYPE_FIRST);
        $arrayRepresentation = $additionalPaymentOptions->toArray();
        $this->assertEquals(
            array(
                'recurringCustomer' => true,
                'recurringTransactionType' => AdditionalPaymentOptions::RECURRING_TYPE_FIRST,
            ), $arrayRepresentation
        );
    }

    public function testValidateRecurringTransactionType()
    {
        $checkRecurringTypeFirst = AdditionalPaymentOptions::validateRecurringTransactionType(
            AdditionalPaymentOptions::RECURRING_TYPE_FIRST
        );
        $this->assertTrue($checkRecurringTypeFirst);
        $checkRecurringTypeRecurring = AdditionalPaymentOptions::validateRecurringTransactionType(
            AdditionalPaymentOptions::RECURRING_TYPE_RECURRING
        );
        $this->assertTrue($checkRecurringTypeRecurring);
        $checkRecurringTypeUnknown = AdditionalPaymentOptions::validateRecurringTransactionType('UNKNOWN');
        $this->assertFalse($checkRecurringTypeUnknown);
    }
}
