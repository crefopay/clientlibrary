<?php

namespace CrefoPay\Library\Tests\Request\Objects;

use DateTime;
use Faker\Factory as Factory;
use Faker\Generator;
use CrefoPay\Library\Request\Objects\Person as Person;
use CrefoPay\Library\Tests\Request\AbstractRequestTest;
use CrefoPay\Library\Validation\Validation;

class PersonTest extends AbstractRequestTest
{
    /**
     * @var string A very long string
     */
    private $veryLongString;

    /**
     * @var Generator
     */
    private $faker;

    public function setUp() : void
    {
        date_default_timezone_set('Europe/Berlin');

        $faker = Factory::create();

        $this->veryLongString = preg_replace("/[^A-Za-z0-9]/", '', $faker->sentence(90));
        $this->faker = $faker;
    }

    public function tearDown() : void
    {
        unset($this->faker);
    }

    /**
     * Test successful validation
     */
    public function testPersonTestValidationSuccess()
    {
        $person = new Person();
        $person->setSalutation(PERSON::SALUTATIONMALE)
            ->setName($this->faker->name)
            ->setSurname($this->faker->name)
            ->setDateOfBirth(new DateTime())
            ->setEmail($this->faker->email)
            ->setPhoneNumber('032555666')
            ->setFaxNumber('032555454');

        $validation = new Validation();
        $validation->getValidator($person);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }

    public function testPersonTestValidationSalutationFailure()
    {
        /**
         * Test required
         */
        $person = new Person();
        $person->setName($this->faker->name)
            ->setSurname($this->faker->name)
            ->setDateOfBirth(new DateTime())
            ->setEmail($this->faker->email)
            ->setPhoneNumber('032555666')
            ->setFaxNumber('032555454');

        $validation = new Validation();

        /**
         * Test the call back
         */
        $person->setSalutation("a");
        $validation->getValidator($person);
        $data = $validation->performValidation();
        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Person',
            'salutation',
            'Salutation must be M, F or V',
            $data,
            "Salutation must be M, F or V validation failed"
        );

    }

    public function testPersonTestValidationNameFailure()
    {
        $validation = new Validation();

        /**
         * Test required
         */
        $person = new Person();
        $person->setSalutation(PERSON::SALUTATIONFEMALE)
            ->setSurname($this->faker->name)
            ->setDateOfBirth(new DateTime())
            ->setEmail($this->faker->email)
            ->setPhoneNumber('032555666')
            ->setFaxNumber('032555454');

        $validation->getValidator($person);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Person',
            'name',
            'Name is required',
            $data,
            "Name is required validation failed"
        );

        /** Test length */
        $person->setName($this->veryLongString);
        $validation->getValidator($person);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Person',
            'name',
            'Name must be less than 50 characters',
            $data,
            "Name must be less than 50 characters validation failed"
        );

    }

    public function testPersonTestValidationSurnameFailure()
    {
        $validation = new Validation();

        /**
         * Test required
         */
        $person = new Person();
        $person->setSalutation(PERSON::SALUTATIONFEMALE)
            ->setName($this->faker->name)
            ->setDateOfBirth(new DateTime())
            ->setEmail($this->faker->email)
            ->setPhoneNumber('032555666')
            ->setFaxNumber('032555454');

        $validation->getValidator($person);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Person',
            'surname',
            'Surname is required',
            $data,
            "Surname is required validation failed"
        );

        /** Test length */
        $person->setSurname($this->veryLongString);
        $validation->getValidator($person);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Person',
            'surname',
            'Surname must be less than 50 characters',
            $data,
            "Surname must be less than 50 characters validation failed"
        );
    }

    public function testPersonTestValidationEmailFailure()
    {
        $validation = new Validation();

        /**
         * Test required
         */
        $person = new Person();
        $person->setSalutation(PERSON::SALUTATIONFEMALE)
            ->setName($this->faker->name)
            ->setSurname($this->faker->name)
            ->setDateOfBirth(new DateTime())
            ->setPhoneNumber('032555666')
            ->setFaxNumber('032555454');

        $validation->getValidator($person);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Person',
            'email',
            'Email is required',
            $data,
            "Email is required validation failed"
        );

        /** Test length */
        $person->setEmail($this->veryLongString);
        $validation->getValidator($person);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Person',
            'email',
            'Email must be less than 254 characters',
            $data,
            "Email must be less than 254 characters validation failed"
        );
    }

    public function testPersonTestValidationPhoneNumberFailure()
    {
        $validation = new Validation();

        /**
         * Test length
         */
        $person = new Person();
        $person->setSalutation(PERSON::SALUTATIONFEMALE)
            ->setName($this->faker->name)
            ->setSurname($this->faker->name)
            ->setEmail($this->faker->email)
            ->setDateOfBirth(new DateTime())
            ->setPhoneNumber("123456789")
            ->setFaxNumber('0123456789');

        $validation->getValidator($person);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Person',
            'phoneNumber',
            'Phone number must be valid for the CrefoPay API',
            $data,
            "Phone number must be valid for the CrefoPay API failed"
        );
    }

    public function testPersonTestValidationFaxNumberFailure()
    {
        $validation = new Validation();

        /**
         * Test length
         */
        $person = new Person();
        $person->setSalutation(PERSON::SALUTATIONFEMALE)
            ->setName($this->faker->name)
            ->setSurname($this->faker->name)
            ->setEmail($this->faker->email)
            ->setDateOfBirth(new DateTime())
            ->setPhoneNumber("0123456789")
            ->setFaxNumber('123456789');

        $validation->getValidator($person);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Person',
            'faxNumber',
            'Fax number must be valid for the CrefoPay API',
            $data,
            "Fax number must be valid for the CrefoPay API failed"
        );
    }
}
