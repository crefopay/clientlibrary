<?php

namespace CrefoPay\Library\Tests\Request\Objects;

use DateTime;
use Faker\Factory as Factory;
use Faker\Generator;
use CrefoPay\Library\Request\Objects\Boniversum;
use CrefoPay\Library\Tests\Request\AbstractRequestTest;
use CrefoPay\Library\Validation\Validation;

class BoniversumTest extends AbstractRequestTest
{
    /**
     * @var string A very long string
     */
    private $veryLongString;

    /**
     * @var Generator
     */
    private $faker;

    public function setUp() : void
    {
        $faker = Factory::create();


        $this->veryLongString = preg_replace("/[^A-Za-z0-9]/", '', $faker->sentence(90));
        $this->faker = $faker;
    }

    public function tearDown() : void
    {
        unset($this->faker);
    }

    public function testRequestDate()
    {
        $boniversum = new Boniversum();
        $boniversum->setIdentification(Boniversum::IDENTIFICATION_PERSON_HOUSEHOLD_IDENTIFIED)
            ->setAddressValidationStatus(Boniversum::ADDRESS_SUCCESSFULLY_VALIDATED)
            ->setTrafficLightColor(Boniversum::TRAFFIC_LIGHT_YELLOW)
            ->setScore(3.1);

        $validation = new Validation();
        $validation->getValidator($boniversum);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Boniversum',
            'requestDate',
            'RequestDate is required',
            $data,
            "RequestDate requirement did not trigger"
        );
    }

    public function testIdentificationFailure()
    {
        $boniversum = new Boniversum();
        $boniversum->setRequestDate(new DateTime())
            ->setAddressValidationStatus(Boniversum::ADDRESS_SUCCESSFULLY_VALIDATED)
            ->setTrafficLightColor(Boniversum::TRAFFIC_LIGHT_YELLOW)
            ->setScore(3.1);

        $validation = new Validation();
        $validation->getValidator($boniversum);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Boniversum',
            'identification',
            'Identification is required',
            $data,
            "Identification requirement did not trigger"
        );

        $boniversum->setIdentification("BONIVERSUM");
        $validation->getValidator($boniversum);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Boniversum',
            'identification',
            'Identification must have a valid value',
            $data,
            "Identification constant validation did not trigger"
        );
    }

    public function testAddressValidationStatusFailure()
    {
        $boniversum = new Boniversum();
        $boniversum->setRequestDate(new DateTime())
            ->setIdentification(Boniversum::IDENTIFICATION_PERSON_IDENTIFIED)
            ->setTrafficLightColor(Boniversum::TRAFFIC_LIGHT_YELLOW)
            ->setScore(3.1);

        $validation = new Validation();
        $validation->getValidator($boniversum);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Boniversum',
            'addressValidationStatus',
            'AddressValidationStatus is required',
            $data,
            "AddressValidationStatus requirement did not trigger"
        );

        $boniversum->setAddressValidationStatus("BONIVERSUM");
        $validation->getValidator($boniversum);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Boniversum',
            'addressValidationStatus',
            'AddressValidationStatus must have a valid value',
            $data,
            "AddressValidationStatus constant validation did not trigger"
        );
    }

    public function testTrafficLightColorFailure()
    {
        $boniversum = new Boniversum();
        $boniversum->setRequestDate(new DateTime())
            ->setAddressValidationStatus(Boniversum::ADDRESS_SUCCESSFULLY_VALIDATED)
            ->setIdentification(Boniversum::IDENTIFICATION_PERSON_IDENTIFIED)
            ->setScore(3.1);

        $validation = new Validation();
        $validation->getValidator($boniversum);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Boniversum',
            'trafficLightColor',
            'TrafficLightColor is required',
            $data,
            "TrafficLightColor requirement did not trigger"
        );

        $boniversum->setTrafficLightColor("BONIVERSUM");
        $validation->getValidator($boniversum);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Boniversum',
            'trafficLightColor',
            'TrafficLightColor must have a valid value',
            $data,
            "TrafficLightColor constant validation did not trigger"
        );
    }

    public function testScoreFailure()
    {
        $boniversum = new Boniversum();
        $boniversum->setRequestDate(new DateTime())
            ->setAddressValidationStatus(Boniversum::ADDRESS_SUCCESSFULLY_VALIDATED)
            ->setIdentification(Boniversum::IDENTIFICATION_PERSON_IDENTIFIED)
            ->setTrafficLightColor(Boniversum::TRAFFIC_LIGHT_YELLOW);

        $validation = new Validation();
        $validation->getValidator($boniversum);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Boniversum',
            'score',
            'Score is required',
            $data,
            "Score requirement did not trigger"
        );

        $boniversum->setScore("BONIVERSUM");
        $validation->getValidator($boniversum);
        $data = $validation->performValidation();

        $this->assertValidationReturned(
            'CrefoPay\\Library\\Request\\Objects\\Boniversum',
            'score',
            'Score must be a float value',
            $data,
            "Score float check did not trigger"
        );
    }

    public function testCompleteObject()
    {
        $boniversum = new Boniversum();
        $boniversum->setRequestDate(new DateTime())
            ->setAddressValidationStatus(Boniversum::ADDRESS_SUCCESSFULLY_VALIDATED)
            ->setIdentification(Boniversum::IDENTIFICATION_PERSON_IDENTIFIED)
            ->setTrafficLightColor(Boniversum::TRAFFIC_LIGHT_YELLOW)
            ->setScore(3.1);

        $validation = new Validation();
        $validation->getValidator($boniversum);
        $data = $validation->performValidation();

        $this->assertEmpty($data, "Validation found an issue when there should be none");
    }
}
