<?php

namespace CrefoPay\Library\Tests\Api;

use PHPUnit\Framework\TestCase;
use CrefoPay\Library\Api\GetUserPaymentInstrument as GetUserPaymentInstrumentApi;
use CrefoPay\Library\Config;
use CrefoPay\Library\Request\GetUserPaymentInstrument;

class GetUserPaymentInstrumentTest extends TestCase
{
    /**
     * Config object for tests
     * @var Config
     */
    private $config;

    public function setUp() : void
    {
        $this->config = new Config(array(
            'merchantPassword' => '8A!v#6qPc3?+G1on',
            'merchantID' => '123',
            'storeID' => 'test Store',
            'sendRequestsWithSalt' => true,
            'baseUrl' => "http://www.something.com/",
        ));
    }

    public function tearDown() : void
    {
        unset($this->config);
    }

    public function testGetUrl()
    {
        $request = new GetUserPaymentInstrument($this->config);

        $api = new GetUserPaymentInstrumentApi($this->config, $request);

        $this->assertEquals('http://www.something.com/getUserPaymentInstrument', $api->getUrl());
    }
}
