<?php

namespace CrefoPay\Library\Tests\Api;

use PHPUnit\Framework\TestCase;
use CrefoPay\Library\Api\GetSubscriptionPlans as GetSubscriptionPlansApi;
use CrefoPay\Library\Config;
use CrefoPay\Library\Request\GetSubscriptionPlans;
use CrefoPay\Library\Request\Objects\Amount;
use CrefoPay\Library\Request\Objects\AmountRange;
use CrefoPay\Library\Request\Objects\SubscriptionPlan;

class GetSubscriptionPlansTest extends TestCase
{
    /**
     * Config object for tests
     * @var Config
     */
    private $config;

    public function setUp() : void
    {
        $this->config = new Config(array(
            'merchantPassword' => '8A!v#6qPc3?+G1on',
            'merchantID' => '123',
            'storeID' => 'test Store',
            'sendRequestsWithSalt' => true,
            'baseUrl' => "http://www.something.com/",
        ));
    }

    public function tearDown() : void
    {
        unset($this->config);
    }

    public function testGetUrl()
    {
        $request = new GetSubscriptionPlans($this->config);

        $api = new GetSubscriptionPlansApi($this->config, $request);

        $this->assertEquals('http://www.something.com/getSubscriptionPlans', $api->getUrl());
    }

    /**
     * Test the api
     */
    public function testSuccessfulMockRequest()
    {
        $header = "HTTP/1.1 200 OK
            \r\nDate: Wed, 18 Nov 2015 14:23:48 GMT
            \r\nServer: Jetty(8.1.15.v20140411)
            \r\nContent-Type: application/json;charset=UTF-8
            \r\nAccess-Control-Allow-Origin: *
            \r\nAccess-Control-Allow-Methods: POST
            \r\nAccess-Control-Expose-Headers: X-Payco-TOKEN, X-Payco-HMAC
            \r\nX-Payco-HMAC: f0448f92b4f66c823c56dd7ab6d5e01ec35843cb
            \r\nVia: 1.1 sandbox.crefopay.de
            \r\nConnection: close
            \r\nTransfer-Encoding: chunked";

        $rawResponse = '{
          "resultCode": 0,
          "pageNumber": 1,
          "pageSize": 25,
          "totalEntries": "2",
          "subscriptionPlans": [{
            "planReference": "D3MO",
            "name": "Demoabo",
            "description": "Ein Testabonnement",
            "amount": {
                "amount": 1000
            },
            "interval": "MONTHLY",
            "trialPeriod": 30,
            "basicPaymentsCount": 48,
            "contactDetails": "Feurigstr. 59, 12103 Berlin, Deutschland",
            "hasSubscribers": true
          }, {
            "planReference": "ORIGIN",
            "name": "Origin - Abo",
            "description": "Ein Origin Abonnement",
            "amount": {
                "amount": 1500
            },
            "interval": "MONTHLY",
            "basicPaymentsCount": 12,
            "contactDetails": "Feurigstr. 59, 12103 Berlin, Deutschland",
            "hasSubscribers": true
          }],
          "salt": "nMp9eFTqrURBqquBb3P9hRX8g7RDzE8DCvu3nKwYJLvwha8F"
        }';

        $request = new GetSubscriptionPlans($this->config);
        $request->setInterval(GetSubscriptionPlans::INTERVAL_MONTHLY)
            ->setPageNumber(1)
            ->setPageSize(25)
            ->setAmount(new AmountRange(new Amount(1), new Amount(1000)));

        $api = new GetSubscriptionPlansApi($this->config, $request);

        $api->setResponseRaw($rawResponse, 200, $header);

        $response = $api->sendRequest();

        $this->assertEquals(0, $response->getData('resultCode'));
        $this->assertEquals($request->getPageNumber(), $response->getData('pageNumber'));
        $this->assertEquals($request->getPageSize(), $response->getData('pageSize'));

        $subscriptionPlans = $response->getData('subscriptionPlans');
        /** @var $plan SubscriptionPlan */
        $plan = $subscriptionPlans[0];

        $this->assertEquals(count($subscriptionPlans), $response->getData('totalEntries'));

        $this->assertInstanceOf('CrefoPay\Library\Request\Objects\SubscriptionPlan', $plan);
        $this->assertEquals(
            $plan->getInterval(),
            SubscriptionPlan::INTERVAL_MONTHLY
        );
        $this->assertEquals(
            $plan->getAmount(),
            new Amount(1000)
        );
    }
}