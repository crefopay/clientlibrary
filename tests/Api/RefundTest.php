<?php

namespace CrefoPay\Library\Tests\Api;

use PHPUnit\Framework\TestCase;
use CrefoPay\Library\Api\Refund as RefundApi;
use CrefoPay\Library\Config;
use CrefoPay\Library\Request\Refund;

class RefundTest extends TestCase
{
    /**
     * Config object for tests
     * @var Config
     */
    private $config;

    public function setUp() : void
    {
        $this->config = new Config(array(
            'merchantPassword' => '8A!v#6qPc3?+G1on',
            'merchantID' => '123',
            'storeID' => 'test Store',
            'sendRequestsWithSalt' => true,
            'baseUrl' => "http://www.something.com",
        ));
    }

    public function tearDown() : void
    {
        unset($this->config);
    }

    public function testGetUrl()
    {
        $request = new Refund($this->config);

        $api = new RefundApi($this->config, $request);

        $this->assertEquals('http://www.something.com/refund', $api->getUrl());
    }
}
