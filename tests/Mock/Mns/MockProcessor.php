<?php

namespace CrefoPay\Library\Tests\Mock\Mns;

use CrefoPay\Library\Mns\ProcessorInterface;

class MockProcessor implements ProcessorInterface
{
    public $data = array();

    /**
     * @param integer $merchantID          This is the merchantID assigned by CrefoPay.
     * @param string  $storeID             This is the store ID of a merchant assigned by CrefoPay as a merchant can have
     *                                     more than one store.
     * @param string  $orderID             This is the order number tyhat the shop has assigned
     * @param string  $captureID           The confirmation ID of the capture. Only sent for Notifications that belong to
     *                                     captures
     * @param string  $merchantReference   Reference that can be set by the merchant during the createTransaction call.
     * @param string  $paymentReference    The reference number of the
     * @param string  $userID              The unique user id of the customer.
     * @param integer $amount              This is either the amount of an incoming payment or “0” in case of some
     *                                     status changes
     * @param string  $currency            Currency code according to ISO4217.
     * @param string  $transactionStatus   Current status of the transaction. Same values as resultCode
     * @param string  $orderStatus         Possible values: PAID PAYPENDING PAYMENTFAILED CHARGEBACK CLEARED. Status of
     *                                     order
     * @param string  $additionalData      Json string with additional data
     * @param integer $transactionBalance  The sum of all received payments minus the sum of all captured amounts plus all 
     *                                     reductions. This value is negative when still waiting for payments,
     *                                     positive in case a transaction was overpaid, and zero when the debt is paid 
     *                                     exactly or no capture was performed yet.
     * @param string  $timestamp           Unix timestamp, Notification timestamp
     * @param string  $version             notification version (currently 2.3)
     *
     * @link https://docs.crefopay.de/api/#notification-call
     */
    public function sendData(
        $merchantID,
        $storeID,
        $orderID,
        $captureID,
        $merchantReference,
        $paymentReference,
        $userID,
        $amount,
        $currency,
        $transactionStatus,
        $orderStatus,
        $additionalData,
        $transactionBalance,
        $timestamp,
        $version
    ) {
        $this->data = array(
            'merchantID' => $merchantID,
            'storeID' => $storeID,
            'orderID' => $orderID,
            'captureID' => $captureID,
            'merchantReference' => $merchantReference,
            'paymentReference' => $paymentReference,
            'userID' => $userID,
            'amount' => $amount,
            'currency' => $currency,
            'transactionStatus' => $transactionStatus,
            'orderStatus' => $orderStatus,
            'additionalData' => $additionalData,
            'transactionBalance' => $transactionBalance,
            'timestamp' => $timestamp,
            'version' => $version,
        );
    }

    /**
     * The run method used by the processor to run successfully validated MNS notifications.
     * This should not return anything
     */
    public function run()
    {
        return null;
    }
}