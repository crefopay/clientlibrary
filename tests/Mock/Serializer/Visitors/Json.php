<?php

namespace CrefoPay\Library\Tests\Mock\Serializer\Visitors;

use CrefoPay\Library\Request\RequestInterface as RequestInterface;
use CrefoPay\Library\Serializer\Serializer as Serializer;
use CrefoPay\Library\Serializer\Visitors\VisitorInterface as VisitorInterface;

class Json implements VisitorInterface
{
    /**
     * The method by which the object is visited and is serialized
     * For the mock object I will simply return an preformatted json string
     *
     * @param RequestInterface $object
     * @param Serializer       $serializer
     *
     * @return string Returns a formatted string such as json, post data from the object
     */
    public function visit(RequestInterface $object, Serializer $serializer)
    {
        return '{"testa": 1}';
    }

    /**
     * Returns the datatype the visitor outputs such as xml,json or post form
     * @return string
     */
    public function getType()
    {
        return 'json';
    }
}
