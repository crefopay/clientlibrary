<?php


namespace CrefoPay\Library\Tests\Mock\Request;

use CrefoPay\Library\Request\AbstractRequest;
use CrefoPay\Library\Request\RequestInterface;

class ExcludedFieldsRequest extends AbstractRequest
{
    public $data = array(
        'test1' => 'foo',
        'test2' => 'boo',
        'test3' => 22,
    );

    public function getPreSerializerData()
    {
        return $this->data;
    }

    public function getClassValidationData(RequestInterface $parent = null)
    {
        return array();
    }

    public function getExcludedMacFields()
    {
        return array('excluded');
    }
}