<?php

namespace CrefoPay\Library\Tests\Mock\Request;

use CrefoPay\Library\Request\RequestInterface;

class MultipartRequest implements RequestInterface
{
    public $data = array();

    public function getSerialiseType()
    {
        return 'multipart';
    }

    public function getSerializerData()
    {
        return $this->data;
    }

    public function toArray()
    {
        return $this->getSerializerData();
    }

    public function getValidationData(RequestInterface $parent = null)
    {
        return array();
    }

    public function customValidation()
    {
        return array();
    }
}
