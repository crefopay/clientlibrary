<?php

namespace CrefoPay\Library\Tests\Mock\Request;

use CrefoPay\Library\Request\Attributes\ObjectArray;
use CrefoPay\Library\Request\RequestInterface;

class UrlRequestWithArray implements RequestInterface
{
    /**
     * String with the visitor code that should handle serialization etc json,post etc
     * @return string
     */
    public function getSerialiseType()
    {
        return 'urlencode';
    }

    /**
     * Return array with the data to be serialized
     * @return array
     */
    public function getSerializerData()
    {
        $objectArray = new ObjectArray();

        $objectArray->append(new NonRecursiveJsonRequest());
        $objectArray->append(new NonRecursiveJsonRequest());

        return array(
            'test' => 1,
            'test2' => 2,
            'arrayValue' => $objectArray,
        );
    }

    public function toArray()
    {
        return $this->getSerializerData();
    }

    public function getValidationData(RequestInterface $parent = null)
    {
        return array();
    }

    public function customValidation()
    {
        return array();
    }
}