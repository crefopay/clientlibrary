<?php

namespace CrefoPay\Library\Tests\Mock\Response\Unserializer\Handler;


use CrefoPay\Library\Request\RequestInterface;
use CrefoPay\Library\Response\Unserializer\Handler\UnserializerInterface;
use CrefoPay\Library\Response\Unserializer\Processor;
use CrefoPay\Library\Tests\Mock\Request\TopLevelRequest;

class MockHandleRecursive implements UnserializerInterface
{
    /**
     * Return the string of the property that the unserializer will handle
     * @return array
     */
    public function getAttributeNameHandler()
    {
        return array('testobj');
    }

    /**
     * @param           $value
     * @param Processor $processor
     *
     * @return RequestInterface
     */
    public function unserializeProperty(Processor $processor, $value)
    {
        $request = new TopLevelRequest();
        $request->data = $value;

        return $request;
    }
}
