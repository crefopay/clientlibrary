<?php

namespace CrefoPay\Library\Tests\Response\Unserializer\Handler;

use PHPUnit\Framework\TestCase;
use CrefoPay\Library\Request\Objects\Amount;
use CrefoPay\Library\Response\Unserializer\Handler\Amount as AmountUnserializer;
use CrefoPay\Library\Response\Unserializer\Processor;

class AmountTest extends TestCase
{
    /**
     * Test if array of PaymentInstrument is returned
     */
    public function testSerialization()
    {
        $jsonString = '{"amount":15,"netAmount":10}';

        $jsonObj = json_decode($jsonString, true);

        $amountProcessor = new AmountUnserializer();

        $amount = $amountProcessor->unserializeProperty(new Processor(), $jsonObj);

        /**
         * @var Amount $amount
         */
        $this->assertEquals(15, $amount->getAmount());
        $this->assertEquals(10, $amount->getNetAmount());

    }
}
