<?php

namespace CrefoPay\Library\Tests\Response\Unserializer\Handler;

use PHPUnit\Framework\TestCase;
use CrefoPay\Library\Request\Objects\Company;
use CrefoPay\Library\Response\Unserializer\Handler\Company as CompanyUnserializer;
use CrefoPay\Library\Response\Unserializer\Processor;

class CompanyTest extends TestCase
{
    /**
     * Test if array of PaymentInstrument is returned
     */
    public function testSerialization()
    {
        $path = realpath(dirname(__FILE__));

        $json = file_get_contents("$path/CompanyTest.json");

        $value = json_decode($json, true);

        $serializer = new CompanyUnserializer();

        /**
         * @var Company $company
         */
        $company = $serializer->unserializeProperty(new Processor(), $value);

        $this->assertEquals("Test", $company->getCompanyName());
        $this->assertEquals(1, $company->getCompanyRegistrationID());
        $this->assertEquals(1111, $company->getCompanyVatID());
        $this->assertEquals(2222, $company->getCompanyTaxID());
        $this->assertEquals('FN', $company->getCompanyRegisterType());

    }
}
