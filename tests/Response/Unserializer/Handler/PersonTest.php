<?php

namespace CrefoPay\Library\Tests\Response\Unserializer\Handler;

use PHPUnit\Framework\TestCase;
use CrefoPay\Library\Response\Unserializer\Handler\Person as PersonUnserializer;
use CrefoPay\Library\Response\Unserializer\Processor;

class PersonTest extends TestCase
{
    protected $testCases = 3;

    /**
     * Test if array of PaymentInstrument is returned
     */
    public function testSerialization()
    {
        $path = realpath(dirname(__FILE__));

        $json = file_get_contents("$path/PersonTest.json");

        $data = json_decode($json, true);

        $this->assertEquals(count($data), $this->testCases, "Array does not contain " . $this->testCases . " elements");

        $serializer = new PersonUnserializer();

        $objValidationRan = 0;

        foreach ($data as $json) {
            /**
             * @var array $json
             */
            $person = $serializer->unserializeProperty(new Processor(), $json);

            if ($objValidationRan === 0) {
                $this->assertEquals("M", $person->getSalutation());
                $this->assertEquals("Keyshawn", $person->getName());
                $this->assertEquals("Sawayn", $person->getSurname());
                $this->assertEquals("1986-11-11", $person->getDateOfBirth()->format("Y-m-d"));
                $this->assertEquals('test@test.com', $person->getEmail());
                $this->assertEquals('222555', $person->getPhoneNumber());
                $this->assertEquals('333444', $person->getFaxNumber());
                $objValidationRan++;
                continue;
            }
            if ($objValidationRan === 1) {
                $this->assertNull($person->getSalutation());
                $this->assertEquals("Robert", $person->getName());
                $this->assertEquals("Reighley", $person->getSurname());
                $this->assertNull($person->getDateOfBirth());
                $this->assertEquals('test@example.com', $person->getEmail());
                $this->assertNull($person->getPhoneNumber());
                $this->assertNull($person->getFaxNumber());
                $objValidationRan++;
                continue;
            }
            if ($objValidationRan === 2) {
                $this->assertEmpty($person->getSalutation());
                $this->assertEquals("Dana", $person->getName());
                $this->assertEquals("Druxley", $person->getSurname());
                $this->assertNull($person->getDateOfBirth());
                $this->assertEquals('test2@example.com', $person->getEmail());
                $this->assertEquals('123456789', $person->getPhoneNumber());
                $this->assertEmpty($person->getFaxNumber());
                $objValidationRan++;
                break;
            }
        }

        $this->assertEquals($this->testCases, $objValidationRan, "Not all object validation has ran in this test");
    }
}