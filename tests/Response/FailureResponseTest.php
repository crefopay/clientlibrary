<?php

namespace CrefoPay\Library\Tests\Serializer;

use PHPUnit\Framework\TestCase;
use CrefoPay\Library\Config;
use CrefoPay\Library\Response\FailureResponse;

class FailureResponseTest extends TestCase
{
    /**
     * Config object for tests
     * @var Config
     */
    private $config;

    public function setUp() : void
    {
        $this->config = new Config(array(
            'merchantPassword' => '8A!v#6qPc3?+G1on',
            'merchantID' => '123',
            'storeID' => 'test Store',
            'sendRequestsWithSalt' => true,
        ));
    }

    public function tearDown() : void
    {
        unset($this->config);
    }

    /**
     * Test that if a error status method will get set
     */
    public function testGetErrorStatusMessage()
    {
        $failureResponse = new FailureResponse(
            $this->config,
            array(
                'resultCode' => 2014,
            )
        );

        $expected = 'The user already exists.';

        $this->assertEquals($expected, $failureResponse->getErrorStatusMessage());
    }
}
