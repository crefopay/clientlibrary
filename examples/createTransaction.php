<?php

require_once 'vendor/autoload.php';
require_once 'helperAddress.php';
require_once 'helperPerson.php';

use Address as DemoAddress;
use Person as DemoPerson;
use CrefoPay\Library\Api\CreateTransaction as CreateTxApi;
use CrefoPay\Library\Api\Exception\ApiError;
use CrefoPay\Library\Api\Exception\CurlError;
use CrefoPay\Library\Config;
use CrefoPay\Library\Integration\Type as IntegrationType;
use CrefoPay\Library\Request\CreateTransaction as CreateTxRequest;
use CrefoPay\Library\Request\Objects\Address;
use CrefoPay\Library\Request\Objects\Amount;
use CrefoPay\Library\Request\Objects\BasketItem;
use CrefoPay\Library\Request\Objects\Person;
use CrefoPay\Library\Risk\RiskClass;
use CrefoPay\Library\User\Type as UserType;

class CreateTx
{
    /**
     * The createTransaction API
     *
     * @var CreateTxApi
     */
    private $api;

    /**
     * Config of the library
     *
     * @var Config
     */
    private $config;

    /**
     * The createTransaction request
     *
     * @var CreateTxRequest
     */
    private $request;

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->config = new Config([
            'merchantPassword' => '12345678ABCDEFGH',
            'merchantID' => '100',
            'storeID' => 'snippetsEUR',
            'logEnabled' => true,
            'logLevel' => 100,
            'logLocationMain' => './log/main',
            'logLocationRequest' => './log/requests',
            'logLocationMNS' => './log/mns',
            'logLocationCallbacks' => './log/callbacks',
            'defaultRiskClass' => '1',
            'defaultLocale' => 'DE',
            'sendRequestsWithSalt' => false,
            'baseUrl' => 'https://sandbox.crefopay.de/2.0/',
        ]);
        $this->request = new CreateTxRequest($this->config);
    }

    public function create(CreateTxRequest $request = null)
    {
        if ($request === null) {
            # if no request provided we use dummy data

            $ha = new DemoAddress();
            $hp = new DemoPerson();

            $txAddress = new Address();
            $txAddress
                ->setStreet($ha->getStreet())
                ->setNo($ha->getNo())
                ->setZip($ha->getZip())
                ->setCity($ha->getCity())
                ->setCountry($ha->getCountry());
            $txAmount = new Amount(random_int(1, 1000000));
            $txBasket = new BasketItem();
            $txBasket
                ->setBasketItemAmount($txAmount)
                ->setBasketItemCount(1)
                ->setBasketItemText('default basket item');
            $txUser = new Person();
            $txUser
                ->setName($hp->getName())
                ->setSurname($hp->getSurname())
                ->setEmail($hp->getEmail());

            $this->request
                ->setAmount($txAmount)
                ->addBasketItem($txBasket)
                ->setBillingRecipient($hp->getRecipientName())
                ->setContext('ONLINE')
                ->setIntegrationType(IntegrationType::INTEGRATION_TYPE_API)
                ->setBillingAddress($txAddress)
                ->setUserData($txUser)
                ->setUserID(uniqid())
                ->setUserRiskClass(RiskClass::RISK_CLASS_DEFAULT)
                ->setUserType(UserType::USER_TYPE_PRIVATE)
            ;

            $this->api = new CreateTxApi($this->config, $this->request);
        } else {
            $this->api = new CreateTxApi($this->config, $request);
        }

        try {
            $response = $this->api->sendRequest();
        } catch (ApiError $a) {
            # do some error handling
            echo("Got an API Exception: " . $a->getMessage() . "\n");
            echo("Details: \n" . print_r($a, true));
        } catch (CurlError $c) {
            # do some error handling
            echo("Got a Curl Exception: " . $c->getMessage() . "\n");
            echo("Details: \n" . print_r($c, true));
        } catch (\Exception $e) {
            # do some error handling
            echo("Got an Exception: \n" . $e->getMessage() . "\n");
            echo("Details: \n" . print_r($e, true));
        }
    }
}

$c = new CreateTx();
$c->create();

?>