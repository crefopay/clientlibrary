<?php

/**
 * PHP Address object for CrefoPay API 2.x 
 */
class Address 
{
    /**
     * street
     *
     * @var string
     */
    private $street;

    /**
     * house number
     *
     * @var string
     */
    private $no;

    /**
     * additional information
     *
     * @var string
     */
    private $additional;

    /**
     * zip
     *
     * @var string
     */
    private $zip;

    /**
     * city
     *
     * @var string
     */
    private $city;

    /**
     * state, e.g. NRW
     *
     * @var string
     */
    private $state;

    /**
     * country
     *
     * @var 2 letter country code according ISO 3166
     */
    private $country;


    /**
     * constructor
     */
    public function __construct()
    {
        $this->setStreet("Hellersbergstr.");
        $this->setNo("14");
        $this->setZip("41460");
        $this->setCity("Neuss");
        $this->setCountry("DE");
    }

    /**
     * Return the company address for simulated risk checks
     *
     * @param string $result
     * @return  self
     */
    public function getCompanyAddress(string $result)
    {
        switch ($result) {
            # identification failed
            case 'FAILED':
                $this->setZip("1234");
                $this->setCity("Rue de Nivoil");
                $this->setNo("38");
                $this->setStreet("Neugasse");
                $this->setCountry("LU");
            break;

            case 'NONE':
                $this->setZip("99423");
                $this->setCity("Weimar");
                $this->setNo("17");
                $this->setStreet("Bahnhofstrasse");
                $this->setCountry("DE");
            break;

            # red result
            case 'RED':
                $this->setZip("99423");
                $this->setCity("Weimar");
                $this->setNo("89");
                $this->setStreet("Eckenerstrasse");
                $this->setCountry("DE");
            break;
            # yellow result
            case 'YELLOW':
                $this->setZip("99423");
                $this->setCity("Weimar");
                $this->setNo("172");
                $this->setStreet("Hermann-Brill Platz");
                $this->setCountry("DE");
            break;

            # green result
            case 'GREEN':
                $this->setZip("99423");
                $this->setCity("Weimar");
                $this->setNo("145");
                $this->setStreet("Lisztstrasse");
                $this->setCountry("DE");
            break;

            # fallback
            default:
                $this->setZip("2560");
                $this->setCity("Berndorf");
                $this->setNo("1");
                $this->setStreet("Neugasse");
                $this->setCountry("AT");
            break;
        }

        return $this;
    }

    /**
     * Get the value of street
     */ 
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set the value of street
     *
     * @return  self
     */ 
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get the value of no
     */ 
    public function getNo()
    {
        return $this->no;
    }

    /**
     * Set the value of no
     *
     * @return  self
     */ 
    public function setNo($no)
    {
        $this->no = $no;

        return $this;
    }

    /**
     * Get the value of additional
     */ 
    public function getAdditional()
    {
        return $this->additional;
    }

    /**
     * Set the value of additional
     *
     * @return  self
     */ 
    public function setAdditional($additional)
    {
        $this->additional = $additional;

        return $this;
    }

    /**
     * Get the value of zip
     */ 
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set the value of zip
     *
     * @return  self
     */ 
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get the value of city
     */ 
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of city
     *
     * @return  self
     */ 
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get the value of state
     */ 
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set the value of state
     *
     * @return  self
     */ 
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get the value of country
     */ 
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set the value of country
     *
     * @return  self
     */ 
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }
}
