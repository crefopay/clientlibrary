<?php

/**
 * PHP Person object for CrefoPay API 2.x 
 */
class Person 
{
    /**
     * The field 'salutation' is not mandatory, but it is required to perform 
     * most solvency checks for the payment methods direct debit and bill 
     * payment. If no salutation is provided the available payment methods may
     * be restricted.
     * 
     * possible Values: M | F | V
     */
    private $salutation;

    /**
     * 	Person's name
     */
    private $name;

    /**
     * 	Person's surname
     */
    private $surname;

    /**
     * Date of birth. The field 'dateOfBirth' is not mandatory, but it is 
     * required to perform most solvency checks for the payment methods direct
     * debit and bill payment. If no date of birth is provided the available
     * payment methods may be restricted.
     */
    private $dateOfBirth;

    /**
     * 	Person's e-mail address
     */
    private $email;

    /**
     * Person's phone number with leading zero
     */
    private $phoneNumber;

    /**
     * Person's fax number with leading zero
     */
    private $faxNumber;


    /**
     * Constructor
     * 
     * @param int $score
     */
    public function __construct(int $score = 50)
    {
        switch (true) {
            # successfully validated: Score 63 S8 
            case ($score <= 63):
                $this->setName("Uno");
                $this->setSurname("Eins");
                $this->setSalutation("M");
                $this->setEmail("service@crefopay.de");
                $this->setDateOfBirth("1960-07-07");
                $this->setPhoneNumber("+493011111"); 
            break;

            # successfully validated: Score 71 S8
            case ($score <= 71):
                $this->setName("Uno");
                $this->setSurname("Sechzig");
                $this->setSalutation("M");
                $this->setEmail("service@crefopay.de");
                $this->setDateOfBirth("1966-06-06");
                $this->setPhoneNumber("+493011111"); 
            break;

            case ($score <= 94):
                $this->setName("Uno");
                $this->setSurname("Neununddreißig");
                $this->setSalutation("M");
                $this->setEmail("service@crefopay.de");
                $this->setDateOfBirth("1996-01-11");
                $this->setPhoneNumber("+493011111"); 
            break;

            case ($score <= 97):
                $this->setName("Uno");
                $this->setSurname("Einhundert");
                $this->setSalutation("M");
                $this->setEmail("service@crefopay.de");
                $this->setDateOfBirth("1910-10-10");
                $this->setPhoneNumber("+493011111"); 
            break;

            case ($score <= 5200):
                $this->setName("Uno");
                $this->setSurname("Siebzig");
                $this->setSalutation("M");
                $this->setEmail("service@crefopay.de");
                $this->setDateOfBirth("1989-05-05");
                $this->setPhoneNumber("+493011111"); 
            break;

            case ($score <= 5800):
                $this->setName("Uno");
                $this->setSurname("Sechs");
                $this->setSalutation("M");
                $this->setEmail("service@crefopay.de");
                $this->setDateOfBirth("1910-07-07");
                $this->setPhoneNumber("+493011111"); 
            break;

            case ($score > 5800):
                $this->setName("Uno");
                $this->setSurname("Zwei");
                $this->setSalutation("M");
                $this->setEmail("service@crefopay.de");
                $this->setDateOfBirth("1950-07-07");
                $this->setPhoneNumber("+493011111"); 
            break;

        }
    }

    /**
     * Get the recipient name
     *
     * @return string
     */
    public function getRecipientName()
    {
        return $this->getName() . ' ' . $this->getSurname();
    }

    /**
     * Get the field 'salutation' is not mandatory, but it is required to perform
     */ 
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * Set the field 'salutation' is not mandatory, but it is required to perform
     *
     * @return  self
     */ 
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;

        return $this;
    }

    /**
     * Get 	Person's name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set 	Person's name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get 	Person's surname
     */ 
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set 	Person's surname
     *
     * @return  self
     */ 
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get date of birth. The field 'dateOfBirth' is not mandatory, but it is
     */ 
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Set date of birth. The field 'dateOfBirth' is not mandatory, but it is
     *
     * @return  self
     */ 
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * Get 	Person's e-mail address
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set 	Person's e-mail address
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get person's phone number with leading zero
     */ 
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set person's phone number with leading zero
     *
     * @return  self
     */ 
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get person's fax number with leading zero
     */ 
    public function getFaxNumber()
    {
        return $this->faxNumber;
    }

    /**
     * Set person's fax number with leading zero
     *
     * @return  self
     */ 
    public function setFaxNumber($faxNumber)
    {
        $this->faxNumber = $faxNumber;

        return $this;
    }
}
