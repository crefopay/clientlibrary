# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

## [Planned]

- Apple-Pay  Support
- Google-Pay Support

## [2.1.0] - 2023-09-08

### Added in [2.1.0]

- Support for new payment method ZINIA_BNPL
- Support for new payment method ZINIA_INSTALLMENT

### Changed in [2.1.0]

- Introduced PHPStan to improve code style and code quality

## [2.0.2] - 2022-08-15

### Changed in [2.0.2]

- Extend max length for orderID, captureID and subscriptionID to 50 characters  
- Make captureID not longer mandatory for addPayment  

## [2.0.1] - 2022-06-24  

### Added in [2.0.1]

- Adds support to pass custom curl-options

## [2.0.0] - 2021-12-29

### Breaking Changes in [2.0.0]

- Switched the namespace of the library: Upg -> CrefoPay

### Added in [2.0.0]

- composer.json now contains the `version` tag of the library
- composer.json now contains the `homepage` tag of the library
- composer.json now contains the `readme` tag of the library
- Library now supports static constants for createTransaction context (`\CrefoPay\Library\Integration\Context`)

## [1.3.0] - 2021-12-23

### Added in [1.3.0]

- PHP-8 Support  
- MNS 2.4 and PNS support

## [1.2.4] - 2021-03-15

### Added in [1.2.4]

- Library now supports the startDate of createSubscription

### Changed in [1.2.4]

- BIC is not longer mandatory for direct debit payments
- Replaced vatAmount and vatRate with netAmount

## [1.2.3] - 2021-02-24

### Added in [1.2.3]

- Added NAME to Address request object

## [1.2.2] - 2020-11-15

### Added in [1.2.2]

- added SALUTATIONVARIOUS to Person request object

## [1.2.1] - 2020-10-02

### Added in [1.2.1]

- Support for new MNS param transactionBalance
- Suppott for new Reserve param referenceOrder

## [1.2.0] - 2020-06-17

### Added  in [1.2.0]

- Supporting multi shop backends

### Removed in [1.2.0]

- Monolog dependency

### Fixed in [1.2.0]

- Fixed an incompatibility with applications that depended on the latest monolog version

## [1.1.6] - 2019-12-10

### Added in [1.1.6]

- Added the API Call `addPayment` which allows you to add payments to open captures

### Changed in [1.1.6]

- Changed the logging system to reuse the logger on sequential API calls
- Changed the `UpdateInvoice` end-to-end test

### Deprecated in [1.1.6]

- Deprecated the `verifyCard` Api and Request

### Removed in [1.1.6]

- Removed the `verifyCard` end-to-end tests

### Fixed in [1.1.6]

- Fixed an incompatibility with applications that depended on the latest monolog version

## [1.1.5] - 2019-09-25

### Added in [1.1.5]

- Added a validation helper `\Upg\Library\Request\Helper\PhoneNumber` to validate the phone numbers before sending the request
- Added a new class `\Upg\Library\Integration\Type` which defines the `INTEGRATION_TYPE_*` constants
- Added the non-documented `integrationType` field to the `CreateSubscription` request
- Added the API Call `solvencyCheck` which allows you to check user or company data for solvency
- Added unit tests for the new API endpoint
- Added fields to send compact report data to CrefoPay
- Added new field `creditLimit` to `CreateTransaction` and `RegisterUser` to define a user specific credit limit
- Added new payment method `AMAZON_PAY`

### Changed in [1.1.5]

- Updated transaction tests to use the `\Upg\Library\Integration\Type` class
- Updated subscription tests to use the `\Upg\Library\User\Type` class
- Updated error messages

### Deprecated in [1.1.5]

- Deprecated the `INTEGRATION_TYPE_*` constants in the class `CreateTransaction`
- Deprecated old payment methods and locales

## [1.1.4] - 2019-09-24

### Fixed in [1.1.4]

- Fixed an incompatibility with applications that depended on the latest monolog version

## [1.1.3] - 2019-05-07

### Added in [1.1.3]

- Added a check which will prefill the `locale` and `userRiskClass` of requests that need it based on the `defaultLocale` and `defaultRiskClass` in the `Config` object
- Added the API Call `updateInvoice` which does the same as `updateTransaction` and will replace it in the future
- Added the API Call `updateTransactionData` which allows you to update specific fields of a transaction
- Added some unit tests for the new API endpoints

### Changed in [1.1.3]

- Updated the error codes to take recently added error codes into account 
- Changed some unit tests to accord for functional changes
- (Non-Functional) Changed the documentation links in our PHPDoc comments to the new documentation page

### Deprecated in [1.1.3]

- Deprecated the function `setCcv` in the `reserve` request class in favor of the new function `setCvv`

### Fixed in [1.1.3]

- Fixed an issue where edge cases broke the `userData` deserialization and resulted in a `Fatal Error` (see #2)

## [1.1.2] - 2019-02-25

### Added in [1.1.2]

- Added a flag to define a customer as known to CrefoPay, this will remove the requirement of some fields like the `userType`
- Added validation for `userData` and `companyData` fields based on the set `userType`

### Changed in [1.1.2]

- Changed the `DateTime` type hints to `DateTimeInterface` to allow `DateTimeImmutable` objects

## [1.1.1] - 2019-02-13

### Fixed in [1.1.1]
- Unserializer for the `userData` no longer throws an `Exception` if the `dateOfBirth` is not set in a API response 

## [1.1.0] - 2019-02-12

### Changed in [1.1.0]

- This library is now published as [crefopay/php-clientlibrary](https://packagist.org/packages/crefopay/php-clientlibrary), it supercedes the old abandoned package named `vmrose/php-clientlibrary`.

## [1.0.16] - 2019-01-28

### Fixed in [1.0.16]

- Fixed an issue with the MAC calculator that caused it to fail the validation of the API response on some setups

## [1.0.15] - 2018-11-20

### Changed in [1.0.15]

- Updated the character limit of the person's email according to the documentation

### Removed in [1.0.15]

- Removed the `getToken` API as was informed in earlier changelogs

### Fixed in [1.0.15]

- Library no longer removes any non-digit characters in the person's phone and fax number

## [1.0.14] - 2018-11-14

### Added in [1.0.14]

- Added support for the field `additionalPaymentOptions` in the API CreateTransaction together with the new field `recurringTransactionType`

### Changed in [1.0.14]

- Changed the validation functions to receive a copy of their parent to define their validation rules based on its set fields.

### Deprecated in [1.0.14]

- The constants USER_TYPE_PRIVATE and USER_TYPE_BUSINESS in the class ```\Upg\Library\Request\CreateTransaction``` were deprecated in favor of their own class ```\Upg\Library\User\Type``` which was already in use for ```\Upg\Library\Request\RegisterUser```.

### Removed in [1.0.14]

- Removed the validation for userIpAddress due to increasing complexity with IPv6, in case of an invalid IP you'll get an `ApiError` Exception.

### Fixed in [1.0.14]

- Fixed support for the PayByLink API, the library required more data than the API itself.

## [1.0.13] - 2018-09-20

### Fixed in 

- Added alias for class Address in the new solvency data unserializer

## [1.0.12] - 2018-09-10

### Added in [1.0.12]

- Added support for the new field returnRiskData in the APIs GetTransactionStatus and GetUser
- Added the object CrediConnect for solvencyCheckInformation in the createTransaction API

### Deprecated in [1.0.12]

- The getToken API is now deprecated and will be removed in version 1.0.15

## [1.0.11] - 2018-07-25

### Added in [1.0.11]

- Added support for the new additional field in the address

### Deprecated in [1.0.11]

- The field billingRecipientAddition in the createTransaction and createSubscription requests was deprecated in favor of the new field mentioned above
- The field shippingRecipientAddition in the createTransaction and createSubscription requests was deprecated in favor of the new field mentioned above

## [1.0.10] - 2018-06-18

### Fixed in [1.0.10]

- Fixed a critical error, that caused the library to fail in some cases due to class name confusion

## [1.0.9] - 2018-06-15

### Added in [1.0.9]

- Added support for the updateSubscription API
- Added support for the Pay by Link API

## [1.0.8] - 2018-05-11

### Added in [1.0.8]

- Added support for the createSubscription API
- Added support for the getSubscriptionPlans API
- Added the new integrationType "Secure Fields"
- Added the new field bonimaResult to the solvencyCheckInformation

### Deprecated in [1.0.8]

- The Boniversum result class was deprecated

## [1.0.7] - 2018-01-16

### Added in [1.0.7]

- Added the objects for solvencyCheckInformation in the createTransaction API
- Added the tests for solvencyCheckInformation and its objects

## [1.0.6] - 2017-12-14

### Added in [1.0.6]

- The company object now allows setting the new email field.

### Changed in [1.0.6]

- The company object now requires the companyName to be set.
- Added tests for the changes in the company object.

## [1.0.5] - 2017-11-23

### Fixed in [1.0.5]

- Fixed the persons' salutation to not be required as per API documentation

## [1.0.4] - 2017-11-20

### Added in [1.0.4]

- Added debug logging to the callback and MNS handler

### Changed in [1.0.4]

- Changed some tests
- Changed the documentation links in the files

## [1.0.3]

### Added in [1.0.3]

- Added support for the getClearingFiles API
- Added support for the getToken API

### Changed in [1.0.3]

- Updated README
- Changed tests
- Changed Config to have some default values

## [1.0.2]

### Added in [1.0.2]

- Added new function to get the paymentInstruments' unmasked IBAN
- Added support for the getTransactionPaymentInstruments API
- Added support for the getClearingFileList API
- Added support for the verifyCard API
- Added error codes

### Deprecated in [1.0.2]

- The function "getVailidationResults" is deprecated, use the function "getValidationResults"

### Changed in [1.0.2]

- The validation library version is now specified and not using the development branch
- Updated README
- Updated error codes
- Updated composer.json

[1.0.1] https://github.com/UPGcarts/clientlibrary/compare/1.0.0...1.0.1
[1.0.0] https://github.com/UPGcarts/clientlibrary/releases/tag/1.0.0
